<?php

/**
 * Representation of date and time.
 * 日付と時刻をあらわす。
 * 
 *   PHP5.2から追加されたPHP標準関数「DateTime」と従来のPHP標準関数「date」にとらわれず、日付を容易に行うために設けられたUtilクラス。
 *     DateTime=> http://php.net/manual/ja/class.datetime.php
 *     date=> http://php.net/manual/ja/function.date.php
 *   
 *   Datetimeは2038年問題に対処済。＜推奨＞
 *   dateは2038年問題に未対処。＜非推奨＞
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Date.class.php 75768 2017-01-30 10:11:51Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics,
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/**
 * PHP 5.2 or higher, use the DateTime class. If there is no DateTime class, use the "date" function to manipulate the "date".
 * PHP5.2以上ならDateTimeクラスを、DateTimeクラスがない場合「date」関数を使用し『日付』を操作する。
 * 
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class Repository_Components_Util_Date
{
    /**
     * Convert date string to specified format.
     * 日付文字列を指定したフォーマットに変換する。
     * 
     * ※注意事項：下記は空文字には変換されない。下記のとおり対応する年月日に変換される。
     *     (閏年ではない)年2月29日 -> 3月1日
     *     (閏年ではない)年2月30日 -> 3月2日
     *     (閏年ではない)年2月31日 -> 3月3日
     *     4月31日 -> 5月1日
     * 
     * @param string $format Only returnable formats are format formats that PHP standards support.
     *                       返却可能なフォーマットは、PHPが標準サポートしているフォーマット形式のみとする。
     *                       詳細はhttp://php.net/manual/ja/function.date.php⇒formatを確認すること。
     * @param string $strDate The accepted date format is only the date format standard supported by PHP. 
     *                        受け付ける日付形式は、PHPが標準サポートしている日付形式のみとする。
     *                        詳細はhttp://php.net/manual/ja/datetime.formats.date.php⇒各国ごとの表記法およびISO 8601を確認すること。
     * @return string 指定したフォーマットに変換した日付文字列。
     */
    public static function dateFormat($format, $strDate)
    {
        $formatDate = "";
        if(class_exists("DateTime"))
        {
            // DateTimeクラスを用いて日付のフォーマット変更を行う。
            try
            {
                $dateTime = new Datetime($strDate);
                $formatDate = $dateTime->format($format);
            }
            catch (Exception $ex)
            {
                // Datetimeはエラー時に例外が起きるため握りつぶす。
                // 例外は$strDateが存在しない日付などでも発生する。
                $formatDate = "";
            }
        }
        else
        {
            // date, strtotimeを用いて日付のフォーマット変更を行う。
            $timeStamp = strtotime($strDate);
            if($timeStamp === false)
            {
                $formatDate = "";
            }
            else
            {
                $formatDate = date($format, $timeStamp);
            }
        }
        
        // format()およびdate()のエラー判定
        if($formatDate === false)
        {
            $formatDate = "";
        }
        
        return $formatDate;
    }
    
    /**
     * Convert month.
     * 月変換を行う。
     * 
     *   PHPにおける月名変換は、実行した日付を保管した変換が実施される。
     *   このため、1月30日に「Feb」を変換すると「2月30日」として変換され「3月」が返却されてしまう。
     *   これを受けて、月変換では1日を補填した変換を行うメソッドを設ける。
     * 
     * @param string $format Only returnable formats are format formats that PHP standards support.
     *                       返却可能なフォーマットは、PHPが標準サポートしているフォーマット形式のみとする。
     *                       詳細はhttp://php.net/manual/ja/function.date.php⇒formatを確認すること。
     * @param string $strMonth The accepted date format is only the date format standard supported by PHP. 
     *                         受け付ける月は、PHPが標準サポートしている月表記のみとする。
     *                         詳細はhttp://php.net/manual/ja/datetime.formats.date.php⇒各国ごとの表記法およびISO 8601を確認すること。
     * @return string 指定したフォーマットに変換した月の文字列。

     */
    public static function dateFormatMonth($format, $strMonth)
    {
        // 月名変換の際は、1日になるように補填してから処理を呼び出す。
        return self::dateFormat($format, $strMonth."-01");
    }
}
?>
