<?php

/**
 * Repository Components Business Index Manager Class
 * インデックス情報処理ビジネスクラス
 *
 * @package     WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Indexmanager.class.php 84353 2018-12-15 09:57:29Z atsushi_suzuki $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics,
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Business logic abstract class
 * ビジネスロジック基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/FW/BusinessBase.class.php';

/**
 * Repository Components Business Index Manager Class
 * インデックス情報処理ビジネスクラス
 *
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Components_Business_Indexmanager extends BusinessBase
{
    /**
     * Index ID of root index
     * ルートインデックスのインデックスID
     * 
     * @var int
     */
    const ID_ROOT_INDEX = 0;
    
    /**
      * Check the public state for the specified index recursively
      * 指定したインデックスの公開状態を再帰的にチェックする
      *
      * @param int $indexId Index ID インデックスID
      * @return string "public" or "private" 「公開」か「非公開」
      */
    public function checkIndexStateRecursive($indexId) {
        $status = $this->checkIndexPublicState($indexId);
        if($status == "private")
        {
            return "private";
        }
        else
        {
            $result = $this->getIndexAllData($indexId);
            if($result[0]["parent_index_id"] == self::ID_ROOT_INDEX)
            {
                return "public";
            }
            else
            {
                return $this->checkIndexStateRecursive($result[0]["parent_index_id"]);
            }
        }
    }
    
    /**
      * Check the public state for the specified index
      * 指定したインデックスの公開状態をチェックする
      *
      * @param int $indexId Index ID インデックスID
      * @return string "public" or "private" 「公開」か「非公開」
      */
    private function checkIndexPublicState($indexId) {
        // インデックス自身の公開フラグのチェック
        $result = $this->getIndexAllData($indexId);
        if($result[0]["public_state"] == 1) {
            return "public";
        }
    
        return "private";
    }
    
    /**
      * Check the browsing authority for the specified index recursively
      * 指定したインデックスの閲覧権限を再帰的にチェックする
      *
      * @param int $indexId Index ID インデックスID
      * @return string "public" or "private" 「公開」か「非公開」
      */
    public function checkIndexBrowsingAuthorityRecursive($indexId) {
        $status = $this->checkIndexBrowsingAuthority($indexId);
        if($status == "private")
        {
            return "private";
        }
        else
        {
            $result = $this->getIndexAllData($indexId);
            if($result[0]["parent_index_id"] == self::ID_ROOT_INDEX)
            {
                return "public";
            }
            else
            {
                return $this->checkIndexBrowsingAuthorityRecursive($result[0]["parent_index_id"]);
            }
        }
    }
    
    /**
     * check index open
     * インデックスが完全公開かをチェックする
     *
     * @param $indexId int index ID インデックスID
     * @return string "public"|"private" "公開"|"非公開"
     */
    public function checkIndexOpen($indexId) {
        if($this->checkIndexStateRecursive($indexId) == "public" &&
            $this->checkIndexBrowsingAuthorityRecursive($indexId) == "public" &&
            $this->checkIndexHarvestStateRecursive($indexId) == "public") {
            return "public";
        } else {
            return "private";
        }
        
    }
    
    /**
      * Check the browsing authority for the specified index
      * 指定したインデックスの閲覧権限をチェックする
      *
      * @param int $indexId Index ID インデックスID
      * @return string "public" or "private" 「公開」か「非公開」
      */
    private function checkIndexBrowsingAuthority($indexId) {
        // インデックス閲覧権限のチェック
        $result = $this->getIndexBrowsingAuthority($indexId);
        // なんらかの権限情報が存在する場合
        if(count($result) == 0) {
            return "private";
        }
    
        if($result[0]["exclusive_acl_role_id"] > 0 ||  // 閲覧ベース権限に制限がかかっている
            $result[0]["exclusive_acl_room_auth"] > -1) // 閲覧ルーム権限に制限がかかっている
        {
            return "private";
        }
        
        // インデックス閲覧グループ権限のチェック
        $result = $this->getIndexBrowsingGroups($indexId);
        // なんらかの権限情報が存在する場合
        for($ii=0; $ii < count($result); $ii++) {
            // 「非会員」のグループ権限が除外権限に設定されている場合
            if($result[$ii]["exclusive_acl_group_id"] == 0) {
                return "private";
            }
        }
        
        return "public";
    }
    
    /**
      * Check the public and private for the specified index
      * 指定したインデックスの公開・非公開をチェックする
      *
      * @param int $indexId Index ID インデックスID
      * @return string "public" or "private" 「公開」か「非公開」
      */
    public function checkIndexState($indexId) {
        // インデックス自身の公開フラグのチェック
        $result = $this->getIndexAllData($indexId);
        if(count($result) == 0) {
            return "private";
        }
        
        if($result[0]["public_state"] == 0) {
            return "private";
        }
        
        // インデックス閲覧権限のチェック
        $result = $this->getIndexBrowsingAuthority($indexId);
        if(count($result) == 0) {
            return "private";
        }
        
        if($result[0]["public_state"] == 0 ||          // 公開フラグが非公開状態
            $result[0]["exclusive_acl_role_id"] > 0 ||  // 閲覧ベース権限に制限がかかっている
            $result[0]["exclusive_acl_room_auth"] > -1) // 閲覧ルーム権限に制限がかかっている
        {
            return "private";
        }
        
        // インデックス閲覧グループ権限のチェック
        $result = $this->getIndexBrowsingGroups($indexId);
        // なんらかの権限情報が存在する場合
        for($ii=0; $ii < count($result); $ii++) {
            // 「非会員」のグループ権限が除外権限に設定されている場合
            if($result[$ii]["exclusive_acl_group_id"] == 0) {
                return "private";
            }
        }
        
        return "public";
    }

    /**
      * Check the harvest public and private for the specified index recursively
      * 指定したインデックスのハーベスト公開・非公開を再帰的にチェックする
      *
      * @param int $indexId Index ID インデックスID
      * @return string "public" or "private" 「公開」か「非公開」
      */
    public function checkIndexHarvestStateRecursive($indexId) {
        $status = $this->checkIndexHarvestState($indexId);
        if($status == "private")
        {
            return "private";
        }
        else
        {
            $result = $this->getIndexAllData($indexId);
            if($result[0]["parent_index_id"] == self::ID_ROOT_INDEX)
            {
                return "public";
            }
            else
            {
                return $this->checkIndexHarvestStateRecursive($result[0]["parent_index_id"]);
            }
        }
    }
    
    /**
     * Get index ID recursive
     * 配下のインデックスIDを再帰的に取得する
     *
     * @param $indexId int index id インデックスID
     * @return array index id list インデックスID配列
     *                array[$ii]
     */
    public function getIndexIdRecursive($indexId) {
        $indexIds = array();
        $indexIds[] = $indexId;
        
        $query = "SELECT index_id FROM {repository_index} ".
                 "WHERE parent_index_id = ? ".
                 "AND is_delete = ? ;";
        $params = array();
        $params[] = $indexId;
        $params[] = 0;
        $result = $this->executeSql($query, $params);
    
        for($ii = 0; $ii < count($result); $ii++) {
            $indexIds[] = $result[$ii]["index_id"];
            $tmpIds = $this->getIndexIdRecursive($result[$ii]["index_id"]);
            $indexIds = array_merge($indexIds, $tmpIds);
        }
        
        return array_values($indexIds);
    }
    
    /**
     * get index of item in other index
     * 指定のインデックス以外に所属しているインデックスを取得する
     *
     * @param $itemId int item ID アイテムID
     * @param $itemNo int item number アイテム通番
     * @param $indexList array index list インデックスリスト
     *                          array[$ii] = IndexID
     * @return array|bool array[$ii]["index_id"]
     *                     true/false 所属している/していない
     */
    public function searchItemPosInOtherIndex($itemId, $itemNo, $indexList) {
        $query = "SELECT index_id FROM {repository_position_index} ".
                 "WHERE item_id = ? ".
                 "AND item_no = ? ".
                 "AND index_id NOT IN (" . implode(",", array_fill(0, count($indexList), "?")) . ") " .
                 "AND is_delete = ?; ";
        $params = array();
        $params[] = $itemId;
        $params[] = $itemNo;
        $params = array_merge($params, $indexList);
        $params[] = 0;
        $ret = $this->executeSql($query, $params);
        
        return $ret;
    }
    
    /**
      * Check the harvest public and private for the specified index
      * 指定したインデックスのハーベスト公開・非公開をチェックする
      *
      * @param int $indexId Index ID インデックスID
      * @return string "public" or "private" 「公開」か「非公開」
      */
    private function checkIndexHarvestState($indexId) {
        // インデックス自身のハーベスト公開フラグのチェック
        $result = $this->getIndexAllData($indexId);
        if($result[0]["harvest_public_state"] == 1) {
            return "public";
        }
        else
        {
            return "private";
        }
    }

    /**
     * Get all index data
     * 全てのインデックス情報を取得する
     *
     * @param int $indexId index ID インデックスID
     * @return array index data 全インデックスデータ
     *                array[$ii]["index_id"|"index_name"|"index_name_english"...]
     * @throws AppException
     */
    private function getIndexAllData($indexId) {
        $query = "SELECT * FROM ".DATABASE_PREFIX."repository_index ".
                 "WHERE index_id = ? ".
                 "AND is_delete = ?;";
        $params = array();
        $params[] = $indexId;
        $params[] = 0;
        $result = $this->Db->execute($query, $params);
        if($result === false) {
            throw new AppException($this->Db->ErrorMsg());
        }
        
        return $result;
    }

    /**
     * Get index browsing authority
     * インデックス閲覧権限取得
     *
     * @param int $indexId index ID インデックスID
     * @return array index authority data インデックス閲覧権限データ
     *                array[0]["index_id"|"exclusive_acl_role_id"|"exclusive_acl_room_auth"|"public_state"|"pub_date"|"harvesting_public_state"...]
     * @throws AppException
     */
    private function getIndexBrowsingAuthority($indexId) {
        $query = "SELECT * FROM ". DATABASE_PREFIX. "repository_index_browsing_authority ".
                 "WHERE index_id = ? ".
                 "AND is_delete = ? ;";
        $params = array();
        $params[] = $indexId;
        $params[] = 0;
        $result = $this->Db->execute($query, $params);
        if($result === false) {
            throw new AppException($this->Db->ErrorMsg());
        }
        
        return $result;
    }

    /**
     * Get index browsing group authority
     * インデックス閲覧グループ権限取得
     *
     * @param int $indexId index ID インデックスID
     * @return array index authority data インデックス閲覧グループ権限データ
     *                array[0]["index_id"|"exclusive_acl_group_id"...]
     * @throws AppException
     */
    private function getIndexBrowsingGroups($indexId) {
        $query = "SELECT * FROM ". DATABASE_PREFIX. "repository_index_browsing_groups ".
                 "WHERE index_id = ? ".
                 "AND is_delete = ? ;";
        $params = array();
        $params[] = $indexId;
        $params[] = 0;
        $result = $this->Db->execute($query, $params);
        if($result === false) {
            $this->errorLog("");
            throw new AppException($this->Db->ErrorMsg());
        }
        
        return $result;
    }
    
    /**
     * Search parent Ids
     * 親インデックスを再帰的に取得する
     *
     * @param $parentIndexIds array parent index ID array 親インデックス配列
     *                               array[$ii]
     * @param $indexId int index ID 基点となるインデックスID
     */
    public function searchParentIndexId(&$parentIndexIds, $indexId) {
        $query = "SELECT parent_index_id FROM {repository_index} ".
                 "WHERE index_id = ? ".
                 "AND is_delete = ? ;";
        $params = array();
        $params[] = $indexId;
        $params[] = 0;
        $ret = $this->executeSql($query, $params);
        if(count($ret) > 0 && $ret[0]["parent_index_id"] > 0) {
            $parentIndexIds[] = $ret[0]["parent_index_id"];
            $this->searchParentIndexId($parentIndexIds, $ret[0]["parent_index_id"]);
        }
    }
    
    /**
     * Get position index ID list table data
     * アイテムIDとアイテム通番から、アイテムの所属インデックスIDリストを取得する
     * 
     * @param int $item_id Item ID アイテムID
     * @param int $item_no Item No アイテム通番
     * 
     * @return array Index information インデックス情報
     *               array[$ii]["index_id"]
     */
    public function selectItemIndexIdList($item_id, $item_no)
    {
        $query = "SELECT index_id ".
                 "FROM ". DATABASE_PREFIX ."repository_position_index ".    // 所属インデックステーブル
                 "WHERE item_id = ? AND ".          // アイテムID
                 "item_no = ?  AND ".               // アイテム通番
                 "is_delete = ?; ";                 // 削除されていない
        $params = null;
        // $queryの?を置き換える配列
        $params[] = $item_id;
        $params[] = $item_no;
        $params[] = 0;
        // SELECT実行
        $result = $this->executeSql($query, $params);
        
        return $result;
    }
    
    /**
     * Recalculate the number of contents of specified index and parent index
     * 指定インデックスおよび親インデックスのコンテンツ数を再計算
     * 
     * @param array $index Index information that the item belongs to or belonged to アイテムが所属している、または所属していたインデックス情報
     *                     array[$ii]["index_id"]
     */
    public function recountIndexContentsNum($index) {
        for($ii = 0; $ii < count($index); $ii++)
        {
            $this->recountContents($index[$ii]["index_id"]);
            $this->recountPrivateContents($index[$ii]["index_id"]);
            
            // 指定インデックスの親インデックスを取得
            $parentIndexIds = array();
            $this->searchParentIndexId($parentIndexIds, $index[$ii]["index_id"]);
            for($jj = 0; $jj < count($parentIndexIds); $jj++)
            {
                $this->recountContents($parentIndexIds[$jj]);
                $this->recountPrivateContents($parentIndexIds[$jj]);
            }
        }
    }
    
    /**
     * It recalculates the number of contents of the specified index, to update
     * 指定インデックスのコンテンツ数を再計算し、更新する
     *
     * @param  int $index_id index ID
     * @return int $contents contents count インデックス以下のコンテンツ数
     */
    private function recountContents($index_id=0) {
        $contents = 0;
        if($index_id != 0) {    // ルートインデックス以外
            // インデックス公開フラグ
            $index_public_flag = $this->checkIndexState($index_id);
            if($index_public_flag === false) {
                return false;
            }
            
            if($index_public_flag == "public"){
                // インデックス直下のコンテンツ数取得
                $query = "SELECT ".DATABASE_PREFIX."repository_position_index.item_id, ".
                         "       ".DATABASE_PREFIX."repository_position_index.item_no, ".
                         "       ".DATABASE_PREFIX."repository_position_index.index_id ".
                         "FROM   ".DATABASE_PREFIX."repository_position_index, ".
                         "       ".DATABASE_PREFIX."repository_item ".
                         "WHERE  ".DATABASE_PREFIX."repository_position_index.index_id = ? ".
                         "AND    ".DATABASE_PREFIX."repository_position_index.is_delete = 0 ".
                         "AND    ".DATABASE_PREFIX."repository_position_index.item_id = ".DATABASE_PREFIX."repository_item.item_id ".
                         "AND    ".DATABASE_PREFIX."repository_position_index.item_no = ".DATABASE_PREFIX."repository_item.item_no ".
                         "AND    ".DATABASE_PREFIX."repository_item.shown_status = 1 ".
                         "AND    ".DATABASE_PREFIX."repository_item.is_delete = 0; ";
                $params = array();
                $params[] = $index_id;
                $result = $this->executeSql($query, $params);
                
                for($ii=0; $ii<count($result); $ii++){
                    $contents++;
                }
            }
        }
        
        // 子インデックスのindex_id, contentsを取得
        $query = "SELECT index_id, contents ".
                 "FROM ".DATABASE_PREFIX."repository_index ".
                 "WHERE parent_index_id = ? ".
                 "AND is_delete = 0;";
        $params = array();
        $params[] = $index_id;
        $result = $this->executeSql($query, $params);
        
        for($ii=0; $ii<count($result); $ii++){
            // 子インデックスのアイテム数を取得(子インデックスの再集計は実施しない)
            $contents += intval($result[$ii]["contents"]);
        }
        
        // update contents
        $query = "UPDATE ". DATABASE_PREFIX ."repository_index ".
                 "SET contents = ? ".
                 "WHERE index_id = ? ";
                 "AND is_delete = 0; ";
        $params = array();
        $params[] = $contents;
        $params[] = $index_id;
        //execute
        $result = $this->executeSql($query,$params);
        
        return $contents;
    }
    
    /**
     * Recalculates the number of private content specified index, to update
     * 指定インデックスの非公開コンテンツ数を再計算し、更新する
     *
     * @param  int $index_id index ID インデックスID
     * @return int $contents contents count インデックス以下のコンテンツ数
     */
    private function recountPrivateContents($index_id=0) {
        $contents = 0;
        if($index_id != 0) {    // ルートインデックス以外
            // インデックス公開フラグ
            $index_public_flag = $this->checkIndexState($index_id);
            if($index_public_flag === false) {
                return false;
            }
            
            if($index_public_flag == "public"){
                // インデックスが公開状態の場合、非公開設定のアイテムのみ取得する
                $query = "SELECT ".DATABASE_PREFIX."repository_position_index.item_id, ".
                         "       ".DATABASE_PREFIX."repository_position_index.item_no, ".
                         "       ".DATABASE_PREFIX."repository_position_index.index_id ".
                         "FROM   ".DATABASE_PREFIX."repository_position_index, ".
                         "       ".DATABASE_PREFIX."repository_item ".
                         "WHERE  ".DATABASE_PREFIX."repository_position_index.index_id = ? ".
                         "AND    ".DATABASE_PREFIX."repository_position_index.is_delete = 0 ".
                         "AND    ".DATABASE_PREFIX."repository_position_index.item_id = ".DATABASE_PREFIX."repository_item.item_id ".
                         "AND    ".DATABASE_PREFIX."repository_position_index.item_no = ".DATABASE_PREFIX."repository_item.item_no ".
                         "AND    ".DATABASE_PREFIX."repository_item.shown_status = 0 ".
                         "AND    ".DATABASE_PREFIX."repository_item.is_delete = 0; ";
                $params = array();
                $params[] = $index_id;
                $result = $this->executeSql($query, $params);
                
                for($ii=0; $ii<count($result); $ii++){
                    $contents++;
                }
            } else {
                // インデックスが非公開状態の場合、全てのアイテムを取得する
                $query = "SELECT ".DATABASE_PREFIX."repository_position_index.item_id, ".
                         "       ".DATABASE_PREFIX."repository_position_index.item_no, ".
                         "       ".DATABASE_PREFIX."repository_position_index.index_id ".
                         "FROM   ".DATABASE_PREFIX."repository_position_index, ".
                         "       ".DATABASE_PREFIX."repository_item ".
                         "WHERE  ".DATABASE_PREFIX."repository_position_index.index_id = ? ".
                         "AND    ".DATABASE_PREFIX."repository_position_index.is_delete = 0 ".
                         "AND    ".DATABASE_PREFIX."repository_position_index.item_id = ".DATABASE_PREFIX."repository_item.item_id ".
                         "AND    ".DATABASE_PREFIX."repository_position_index.item_no = ".DATABASE_PREFIX."repository_item.item_no ".
                         "AND    ".DATABASE_PREFIX."repository_item.is_delete = 0; ";
                $params = array();
                $params[] = $index_id;
                $result = $this->executeSql($query, $params);
                for($ii=0; $ii<count($result); $ii++){
                    $contents++;
                }               
            }
        }
        
        // 子インデックスのindex_id, private_contentsを取得
        $query = "SELECT index_id, private_contents ".
                 "FROM ".DATABASE_PREFIX."repository_index ".
                 "WHERE parent_index_id = ? ".
                 "AND is_delete = 0;";
        $params = array();
        $params[] = $index_id;
        $result = $this->executeSql($query, $params);
        
        for($ii=0; $ii<count($result); $ii++){
            // 子インデックスのアイテム数を取得(子インデックスの再集計は実施しない)
            $contents += intval($result[$ii]["private_contents"]);
        }
        
        // update contents
        $query = "UPDATE ". DATABASE_PREFIX ."repository_index ".
                 "SET private_contents = ? ".
                 "WHERE index_id = ? ";
                 "AND is_delete = 0; ";
        $params = array();
        $params[] = $contents;
        $params[] = $index_id;
        //execute
        $result = $this->executeSql($query, $params);
        
        return $contents;
    }
}
?>