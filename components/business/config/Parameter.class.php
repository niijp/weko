<?php

/**
 * Get the set value of the value (repository_parameter) to be set from the WEKO management screen.
 * WEKO管理画面から設定する値(repository_parameter)の設定値を取得する。
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Parameter.class.php 76604 2017-02-24 12:54:58Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics,
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Business logic abstract class
 * ビジネスロジック基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/FW/BusinessBase.class.php';

/**
 * Get the set value of the value (repository_parameter) to be set from the WEKO management screen.
 * WEKO管理画面から設定する値(repository_parameter)の設定値を取得する。
 *
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class Repository_Components_Business_Config_Parameter extends BusinessBase 
{
    /**
     * Help icon display setting value.
     * ヘルプアイコン表示設定値
     */
    public function helpIconDisplay()
    {
        return $this->parameterValue('help_icon_display');
    }
    
    /**
     * How to output files to be exported (0: all files are output to 1 folder, 1: output files one by one to individual folders)
     * Exportするファイルの出力方法(0: 1フォルダに全ファイルを出力, 1: ファイルを1つずつ個別のフォルダに出力)
     *
     * @return string parameter 設定値
     */
    public function selectHowToOutputExportFile()
    {
        return $this->parameterValue('export_files_by_directory_separately');
    }
    
    /**
     * Get administrator's value that related to parameter name in parameter table
     * パラメータテーブル情報を取得
     *
     * @param string $param_name parameter name パラメータ名
     * @return string parameter. 設定値
     */
    private function parameterValue($param_name)
    {
        // パラメータ値を取得
        $params = array();
        $params[] = $param_name;
        $params[] = 0;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Parameter/parameterValue.sql", $params);
        
        // 1件以上取得できた or 値が取得できなかった場合はエラー
        if(count($result)!= 1 || !isset($result[0]['param_value']))
        {
            $this->errorLog("The parameter is illegal. there is a lot of data gotten.", __FILE__, __CLASS__, __LINE__);
            throw new AppException("Failed parameter is illegal. there is a lot of data gotten.".$this->Db->errorMsg());
        }
        
        return $result[0]['param_value'];
    }
}
?>