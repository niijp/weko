SELECT COUNT(DISTINCT ITEM.item_id, ITEM.item_no) AS total 
FROM {repository_item} AS ITEM 
WHERE ITEM.item_type_id = ? 
AND ITEM.is_delete = ? 
ORDER BY ITEM.item_id ASC;
