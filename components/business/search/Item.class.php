<?php

/**
 * Item table search class
 * アイテムテーブル検索クラス
 *
 * @package     WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Item.class.php 76075 2017-02-07 10:18:47Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics,
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Business logic abstract class
 * ビジネスロジック基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/FW/BusinessBase.class.php';
/**
 * Item table search class
 * アイテムテーブル検索クラス
 *
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Components_Business_Search_Item extends BusinessBase
{
    /**
     * Retrieve the number of items whose itemtype match
     * アイテムタイプが合致するアイテム数の取得
     *
     * @param string $itemtypeId Itemtype ID アイテムタイプID
     *
     * @return int Search result num 検索結果の個数
     */
    public function countItemMatchingItemtype($itemTypeId)
    {
        $params = array();
        $params[] = $itemTypeId;
        $params[] = 0;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Item/selectItemMatchingItemtype.sql", $params);
        if(!isset($result[0]["total"]))
        {
            return 0;
        }
        return $result[0]["total"];
    }
    
    /**
     * Retrieve items with itemtype matching
     * アイテムタイプが合致するアイテムの取得
     *
     * @param string $itemtypeId Itemtype ID アイテムタイプID
     * @param int $pageNo Display page No 表示するページ番号
     * @param int $listViewNum Item count in a page 1ページに表示するアイテム数
     *
     * @return array Search result 検索結果
     *               array[$ii]["item_id"|"item_no"|"uri"]
     */
    public function searchItemMatchingItemtype($itemTypeId, $pageNo, $listViewNum)
    {
        $offset = (intval($pageNo) - 1) * intval($listViewNum);
        
        $query = "SELECT DISTINCT ITEM.item_id, ITEM.item_no, ITEM.uri ".
                 "FROM {repository_item} AS ITEM ".
                 "WHERE ITEM.item_type_id = ? ".
                 "AND ITEM.is_delete = ? ".
                 "ORDER BY ITEM.item_id ASC ".
                 "LIMIT ?, ?;";
        $params = array();
        $params[] = $itemTypeId;
        $params[] = 0;
        $params[] = $offset;
        $params[] = $listViewNum;
        $result = $this->executeSql($query, $params);
        
        return $result;
    }
}
?>