<?php

/**
 * Item metadata (item table) processing class
 * アイテムメタデータ(アイテムテーブル)処理クラス
 *
 * @package     WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Item.class.php 76075 2017-02-07 10:18:47Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics,
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Business logic abstract class
 * ビジネスロジック基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/FW/BusinessBase.class.php';
/**
 * Item metadata (item table) processing class
 * アイテムメタデータ(アイテムテーブル)処理クラス
 *
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Components_Business_Itemmetadata_Item extends BusinessBase
{
    /**
     * Update update date of item overwriting author information.
     * 著者情報を上書きしたアイテムの更新日時をアップデート
     *
     * @param int $author_id Author ID to update. 更新する著者ID
     * @param String $language language.言語 / japanese, english ,""
     */
    public function updateItemModDateForOverwriteMetadata($author_id, $language)
    {
        $params = array();
        $params[] = $this->accessDate;
        $params[] = $this->user_id;
        $params[] = $author_id;
        $params[] = 0;
        $params[] = "name";
        $params[] = $language;
        $params[] = 0;
        $params[] = 0;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Item/updateItemModDateForOverwriteMetadata.sql", $params);
    }
    
    /**
     * Updated instructions for updating items overwritten with e-mail address.
     * メールアドレスを上書きしたアイテムの更新に指示をアップデート
     *
     * @param int $author_id Author ID to update. 更新する著者ID
     */
    public function updateItemModDateForOverwriteMailAddress($author_id)
    {
        $params = array();
        $params[] = $this->accessDate;
        $params[] = $this->user_id;
        $params[] = $author_id;
        $params[] = 0;
        $params[] = 0;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Item/updateItemModDateForOverwriteMailAddress.sql", $params);
    }
}
?>