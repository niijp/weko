<?php

/**
 * Check grant doi business class
 * DOI付与チェックビジネスクラス
 *
 * @package     WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Personalname.class.php 76247 2017-02-10 05:13:24Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics,
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Business logic abstract class
 * ビジネスロジック基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/FW/BusinessBase.class.php';
/**
 * Check grant doi business class
 * DOI付与チェックビジネスクラス
 *
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Components_Business_Itemmetadata_Personalname extends BusinessBase
{
    
    /**
     * Overwrite item metadata with author authority data.
     * 著者名典拠のデータでアイテムのメタデータを上書きする
     *
     * @param int $author_id Author ID to update. 更新する著者ID
     * @param String $language language.言語 / japanese, english ,""
     * @param String $family surname. 姓
     * @param String $name name. 名
     * @param String $family_ruby surname(kana). 姓(ヨミ)
     * @param String $name_ruby name(kana). 名(ヨミ)
     */
    public function overwritePersonalNameWithNameAuthority($author_id, $language, $family, $name, $family_ruby="", $name_ruby="")
    {
        // メタデータ上書き
        $params = array();
        $params[] = $family;
        $params[] = $name;
        $params[] = $family_ruby;
        $params[] = $name_ruby;
        $params[] = $this->accessDate;
        $params[] = $this->user_id;
        $params[] = $author_id;
        $params[] = 0;
        $params[] = "name";
        $params[] = $language;
        $params[] = 0;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Personalname/overwritePersonalNameWithNameAuthority.sql", $params);
    }
    
    /**
     * Overwrite author's email address
     * 著者のメールアドレスを上書きする
     *
     * @param int $author_id Author ID to update. 更新する著者ID
     * @param String $mail 更新するメールアドレス
     */
    public function overwriteMailAddressWithNameAuthority($author_id, $mail)
    {
        $params = array();
        $params[] = $mail;
        $params[] = $this->accessDate;
        $params[] = $this->user_id;
        $params[] = $author_id;
        $params[] = 0;
        $params[] = 0;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Personalname/overwriteMailAddressWithNameAuthority.sql", $params);
    }
    
    /**
     * Change the WEKO author ID of the name table for the specified item.
     * 指定されたアイテムに対して氏名テーブルのWEKO著者IDを変更する。
     *
     * @param int[] $item_id_list Item ID to be replaced. 付け替え対象のアイテムID
     * @param int $before_author_id Author name authority ID before integration. 統合前のWEKO著者ID
     * @param int $after_author_idAuthor name authority ID of integration destination. 統合先のWEKO著者ID
     */
    public function replaceAuthorIdWithAuthorId($item_id_list, $before_author_id, $after_author_id)
    {
        $query = "UPDATE {repository_personal_name} ".
                " SET author_id = ? ".
                " WHERE author_id = ? ".
                " AND is_delete = ? ".
                " AND item_id IN ( ".implode(",", $item_id_list)." ); ";
        $params = array();
        $params[] = $after_author_id;
        $params[] = $before_author_id;
        $params[] = 0;
        $result = $this->executeSql($query, $params);
    }
}
?>