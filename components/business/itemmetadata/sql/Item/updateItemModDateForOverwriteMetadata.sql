UPDATE {repository_item}
SET mod_date = ?, mod_user_id = ? 
WHERE (item_id, item_no) IN (
	SELECT item_id, item_no 
	FROM {repository_personal_name}
	WHERE author_id = ? 
	AND is_delete = ? 
	AND (item_type_id, attribute_id) IN (
		SELECT item_type_id, attribute_id 
		FROM {repository_item_attr_type} 
		WHERE input_type = ? 
		AND display_lang_type = ? 
		AND is_delete = ? 
	)
)
AND is_delete = ?;
