UPDATE {repository_item} 
SET mod_date = ?, mod_user_id = ? 
WHERE (item_id, item_no) IN(
	SELECT item_id, item_no 
	FROM {repository_personal_name}
	WHERE author_id = ? 
	AND is_delete = ? 
)
AND is_delete = ?;
