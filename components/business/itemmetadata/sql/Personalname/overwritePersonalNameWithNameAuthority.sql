UPDATE {repository_personal_name} 
SET family = ?, name = ?, family_ruby = ?, name_ruby = ?, mod_date = ?, mod_user_id = ? 
WHERE author_id = ? 
AND is_delete = ? 
AND (item_type_id, attribute_id) IN (
	SELECT item_type_id, attribute_id 
	FROM {repository_item_attr_type} 
	WHERE input_type = ? 
	AND display_lang_type = ? 
	AND is_delete = ? 
);
