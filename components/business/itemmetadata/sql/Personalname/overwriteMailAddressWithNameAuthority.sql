UPDATE {repository_personal_name} 
SET e_mail_address = ?, mod_date = ?, mod_user_id = ? 
WHERE author_id = ? 
AND LENGTH(e_mail_address) > ? 
AND is_delete = ? ;
