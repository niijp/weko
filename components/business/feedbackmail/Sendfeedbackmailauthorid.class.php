<?php

/**
 * Feedback Mail Destination Table operation class of author ID
 * フィードバックメール送信先著者IDのテーブル操作クラス
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Sendfeedbackmailauthorid.class.php 76512 2017-02-22 11:50:18Z keiya_sugimoto $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics,
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Business logic abstract class
 * ビジネスロジック基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/FW/BusinessBase.class.php';

/**
 * Feedback Mail Destination Table operation class of author ID
 * フィードバックメール送信先著者IDのテーブル操作クラス
 *
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class Repository_Components_Business_Feedbackmail_Sendfeedbackmailauthorid extends BusinessBase 
{
    /**
     * フィードバックメール送信先に指定した著者IDが設定されているか判定する。
     * 
     * @param int Author ID. 著者名典拠IO
     * @return bool true=>紐づくアイテムが登録されている。 / false=>紐づくアイテムが登録されていない。
     */
    public function feedbackmailItemExists($author_id)
    {
        $params = array();
        $params[] = $author_id;
        $params[] = 0;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Sendfeedbackmailauthorid/feedbackmailItemExists.sql", $params);
        
        // レコード件数が1件あればファイルは存在する。
        // 1件以外の場合、削除されているかレコード自体未登録のどちらかである。
        if(isset($result[0]["cnt"]) && $result[0]["cnt"] > 0)
        {
            return true;
        }
        
        return false;
    }
    
    /**
     * For the specified item, change the WEKO author ID in the feedback mail sender ID table.
     * 指定されたアイテムに対して、フィードバックメール送信者IDテーブルのWEKO著者IDを変更する。
     * 
     * @param int[] $item_id_list Item ID to be replaced. 付け替え対象のアイテムID
     * @param int $before_author_id Author name authority ID before integration. 統合前のWEKO著者ID
     * @param int $after_author_idAuthor name authority ID of integration destination. 統合先のWEKO著者ID
     */
    public function replaceAuthorIdWithAuthorId($item_id_list, $before_author_id, $after_author_id)
    {
        // 統合前のWEKO著者IDを統合先のWEKO著者IDで置換
        $query = "UPDATE {repository_send_feedbackmail_author_id} ".
                " SET author_id = ? ".
                " WHERE author_id = ? ".
                " AND item_id IN ( ".implode(",", array_fill(0, count($item_id_list), "?"))." );";
        $params = array();
        $params[] = $after_author_id;
        $params[] = $before_author_id;
        $params = array_merge($params, $item_id_list);
        $result = $this->executeSql($query, $params);
        
        // アイテムのフィードバックメール送信先に統合前と統合先のWEKO著者IDが両方設定されている場合
        // ⇒統合前のWEKO著者IDを削除
        //   author_id_noが歯抜けになるが、他機能への影響はないので歯抜けのままとする。
        $this->deleteDuplicateReplaceAuthorIdGroupByItem($item_id_list);
    }
    
    /**
     * If there is a record linked to the same WEKO author ID for one item, leave the first one and delete all duplicate records.
     * 一つのアイテムに対して同じWEKO著者IDに紐づくレコードがあれば最初の1つを残し重複するレコードをすべて削除する。
     * 
     * @param int[] $item_id_list Item IDs. 対象のアイテムID
     */
    private function deleteDuplicateReplaceAuthorIdGroupByItem($item_id_list)
    {
        $query = "DELETE FROM {repository_send_feedbackmail_author_id} ".
                " WHERE item_id IN ( ".implode(",", array_fill(0, count($item_id_list), "?"))." ) ".
                " AND (item_id, item_no, author_id_no) NOT IN ( ".
                "     SELECT item_id, item_no, min_author_id_no ".
                "     FROM ( ".
                "     SELECT t1.item_id, t1.item_no, min(t1.author_id_no) AS min_author_id_no ".
                "     FROM {repository_send_feedbackmail_author_id} AS t1 ".
                "     WHERE item_id IN ( ".implode(",", array_fill(0, count($item_id_list), "?"))." ) ".
                "     GROUP BY t1.item_id, t1.item_no, t1.author_id ".
                " ) AS t2 ".
                ");";
        $params = array_merge($item_id_list, $item_id_list);
        $result = $this->executeSql($query, $params);
    }
    
    /**
     * It is checked whether the specified author is set as the feedback mail sending destination of the target item
     * 指定された著者が対象アイテムのフィードバックメール送信先に設定されているかチェックする
     * 
     * @param array $item_id_list Item ID to be replaced. 付け替え対象のアイテムID
     *                            array[$ii]
     * @param int $author_id Author name authority ID WEKO著者ID
     * 
     * @return boolean Whether it is set as feedback feedback mail destination フィードバックメール送信先に設定されているかどうか
     */
    public function checkSetFeedbackMailByAuthor($item_id_list, $author_id)
    {
        $query = "SELECT * ".
                " FROM {repository_send_feedbackmail_author_id} MAIL ".
                " INNER JOIN {repository_item} ITEM ".
                " ON MAIL.item_id = ITEM.item_id ".
                " AND MAIL.item_no = ITEM.item_no ".
                " WHERE MAIL.author_id = ? ".
                " AND ITEM.is_delete = ? ".
                " AND MAIL.item_id IN ( ".implode(",", array_fill(0, count($item_id_list), "?"))." );";
        $params = array();
        $params[] = $author_id;
        $params[] = 0;
        $params = array_merge($params, $item_id_list);
        $result = $this->executeSql($query, $params);
        
        if(count($result) > 0)
        {
            return true;
        }
        
        return false;
    }
}
?>