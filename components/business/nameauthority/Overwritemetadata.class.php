<?php

/**
 * Overwrite item metadata with author authority data.
 * 著者名典拠のデータでアイテムのメタデータを上書きする
 * 
 * @package     WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Overwritemetadata.class.php 76542 2017-02-23 07:05:56Z keiya_sugimoto $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Business logic abstract class
 * ビジネスロジック基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/FW/BusinessBase.class.php';

/**
 * Search table manager class
 * 検索テーブル管理クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositorySearchTableProcessing.class.php';

/**
 * Overwrite item metadata with author authority data.
 * 著者名典拠のデータでアイテムのメタデータを上書きする
 * 
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Components_Business_Nameauthority_Overwritemetadata extends BusinessBase
{
    /**
     * Session data management class.
     * セッションデータ管理クラス
     * 
     * @var SessionObject
     */
    private $_session = null;
    
    /**
     * Database management class.
     * データベース管理クラス
     * 
     * @var DBObject
     */
    private $_db = null;
    
    /**
     * Set session and database management class.
     * セッションおよびデータベース管理クラスを設定する。
     * 
     * @param SessionObject $session Session data management class. セッションデータ管理クラス
     * @param DBObject $db Database management class. データベース管理クラス
     */
    public function setComponents($session, $db)
    {
        if(isset($session)) $this->_session = $session;
        if(isset($db)) $this->_db = $db;
    }
    
    /**
     * Overwrite item metadata with author authority data.
     * 著者名典拠のデータでアイテムのメタデータを上書きする
     
     * @param int $author_id WEKO author ID to overwrite. 上書きするWEKO著者ID
     * @return bool 検索テーブル更新フラグ
     */
    public function overwriteMetadataWithNameAuthority($author_id)
    {
        // 検索テーブル更新フラグ
        $searchTableProcessing = false;
        
        // 典拠情報を取得
        $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        $authorData = $busNameAuthority->getNameAuthority(array('author_id'=>$author_id, 'is_delete'=>0));
        if($authorData === false)
        {
            throw new AppException("Failed select NameAuthority at 'author_id' = $author_id. ".__CLASS__."::".__METHOD__);
        }
        
        // 典拠データでアイテムのメタデータを更新
        for($ii=0; $ii<count($authorData); $ii++)
        {
            $this->overwriteMetadata($author_id, $authorData[$ii]['language'], $authorData[$ii]['family'], $authorData[$ii]['name'], $authorData[$ii]['family_ruby'], $authorData[$ii]['name_ruby']);
            $searchTableProcessing = true;
        }
        
        // メールアドレスを上書き
        $mail = $busNameAuthority->externalAuthorIdSuffix($author_id, 0);
        if(strlen($mail) > 0)
        {
            $this->overwriteMailAddress($author_id, $mail);
            $searchTableProcessing = true;
        }
        return $searchTableProcessing;
    }
    
    /**
     * Overwrite the item's metadata with the specified input value.
     * 指定された入力値でアイテムのメタデータを上書きする。
     * 
     * @param int $author_id Author ID to update. 更新する著者ID
     * @param String $language language.言語 / japanese, english ,""
     * @param String $family surname. 姓
     * @param String $name name. 名
     * @param String $family_ruby surname(kana). 姓(ヨミ)
     * @param String $name_ruby name(kana). 名(ヨミ)
     */
    public function overwriteMetadata($author_id, $language, $family, $name, $family_ruby="", $name_ruby="")
    {
        // メタデータ上書き
        $busPersonalName = BusinessFactory::getFactory()->getBusiness('businessItemMetadataPersonalName');
        
        // 日本語以外のヨミはメタデータに上書きしない
        if($language != "japanese")
        {
            $family_ruby = "";
            $name_ruby = "";
        }
        
        $busPersonalName->overwritePersonalNameWithNameAuthority($author_id, $language, $family, $name, $family_ruby, $name_ruby);
        
        // 更新したアイテムの更新日時を変更
        $busItem = BusinessFactory::getFactory()->getBusiness('businessItemMetadataItem');
        $busItem->updateItemModDateForOverwriteMetadata($author_id, $language);
        
        // 更新したアイテムの検索テーブルを再構築
        $searchTableProcessing = new RepositorySearchTableProcessing($this->_session, $this->_db);
        $searchTableProcessing->updateSearchTableForOverwriteMetadataFromNameAuthority($author_id, $language);
    }
    
    /**
     * Overwrite author's email address
     * 著者のメールアドレスを上書きする
     *
     * @param int $author_id Author ID to update. 更新する著者ID
     * @param String $mail 更新するメールアドレス
     */
    public function overwriteMailAddress($author_id, $mail)
    {
        // メールアドレスを上書き
        $busPersonalName = BusinessFactory::getFactory()->getBusiness('businessItemMetadataPersonalName');
        $busPersonalName->overwriteMailAddressWithNameAuthority($author_id, $mail);
        
        // 更新したアイテムの更新日時を変更
        $busItem = BusinessFactory::getFactory()->getBusiness('businessItemMetadataItem');
        $busItem->updateItemModDateForOverwriteMailAddress($author_id);
        
        // 更新したアイテムの検索テーブルを再構築
        $searchTableProcessing = new RepositorySearchTableProcessing($this->_session, $this->_db);
        $searchTableProcessing->updateSearchTableForOverwriteMailAddressFromNameAuthority($author_id);
    }
}
?>