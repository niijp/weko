<?php

/**
 * Load author information business class
 * 著者情報抽出ビジネスクラス
 *
 * @package     WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Loadauthorinformation.class.php 76468 2017-02-21 11:05:23Z keiya_sugimoto $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Business logic abstract class
 * ビジネスロジック基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/FW/BusinessBase.class.php';

/**
 * Load author information business class
 * 著者情報抽出ビジネスクラス
 * 
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Components_Business_Nameauthority_Loadauthorinformation extends BusinessBase
{
    /**
     * Get author information of JSON array
     * JSON配列の著者情報を取得する
     *
     * @param array $authorIdList WEKO author ID list WEKO著者IDリスト
     *                            array[$ii]
     * @param int $is_delete Delete flag 削除フラグ
     * @param int $start_num Start position got author information 取得する著者情報の開始位置
     * @param int $per_num Number of search result per request リクエストあたりの検索結果件数
     *
     * @return array Author information of JSON array JSON配列の著者情報
     *               array[$ii|"author_list"|"start_num"|"per_num"|"total_num"]["family"|"name"|"family_ruby"|...]
     */
    public function selectAuthorInfoJsonArray($authorIdList, $is_delete, $start_num, $per_num) {
        $authorList = array();
        
        if($start_num + $per_num < count($authorIdList))
        {
            $end_num = $start_num + $per_num;
        }
        else
        {
            $end_num = count($authorIdList);
        }
        
        for($ii = $start_num; $ii < $end_num; $ii++)
        {
            $authorInfo = $this->selectAuthorInfo($authorIdList[$ii], $is_delete);
            $externalAuthorInfo = $this->selectExternalAuthorInfo($authorIdList[$ii], $is_delete);
            $this->createAuthorListJsonArray($authorInfo, $externalAuthorInfo, $authorList);
        }
        
        $jsonArray = array();
        $jsonArray["author_list"] = $authorList;
        $jsonArray["start_num"] = $start_num;
        $jsonArray["per_num"] = $per_num;
        $jsonArray["total_num"] = count($authorIdList);
        
        return $jsonArray;
    }
    
    /**
     * Get author information
     * 著者情報を取得する
     *
     * @param int $author_id WEKO author ID WEKO著者ID
     * @param int $is_delete Delete flag 削除フラグ
     *
     * @return array Author information 著者情報
     *               array[$ii]["author_id"|"language"|"family"|...]
     */
    private function selectAuthorInfo($author_id, $is_delete) {
        // 削除済み検索において削除された著者でない場合は情報なしと扱う
        if($is_delete == 1 && !$this->checkDeletedAuthor($author_id))
        {
            return array();
        }
        
        $query = "SELECT author_id, language, family, name, family_ruby, name_ruby, is_delete ".
                 "FROM {repository_name_authority} ".
                 "WHERE author_id = ? ".
                 "AND is_delete = ?;";
        $params = array();
        $params[] = $author_id;
        $params[] = $is_delete;
        
        try
        {
            $result = $this->executeSql($query, $params);
        }
        catch (AppException $ex)
        {
            // 削除された著者が見つからない場合は情報なしと扱う
            $result = array();
        }
        
        return $result;
    }
    
    /**
     * Get external author information
     * 外部著者情報を取得する
     *
     * @param int $author_id WEKO author ID WEKO著者ID
     * @param int $is_delete Delete flag 削除フラグ
     *
     * @return array External author information 外部著者情報
     *               array[$ii]["author_id"|"prefix_id"|"suffix"|...]
     */
    private function selectExternalAuthorInfo($author_id, $is_delete) {
        // 削除済み検索において削除された著者でない場合は情報なしと扱う
        if($is_delete == 1 && !$this->checkDeletedAuthor($author_id))
        {
            return array();
        }
        
        $query = "SELECT SUF.author_id, SUF.prefix_id, SUF.suffix, SUF.is_delete, PRE.prefix_name ".
                 "FROM {repository_external_author_id_suffix} AS SUF ".
                 "INNER JOIN {repository_external_author_id_prefix} AS PRE ".
                 "ON SUF.prefix_id = PRE.prefix_id ".
                 "WHERE author_id = ? ".
                 "AND SUF.is_delete = ? ".
                 "AND PRE.is_delete = ?;";
        $params = array();
        $params[] = $author_id;
        $params[] = $is_delete;
        $params[] = 0;
        
        try
        {
            $result = $this->executeSql($query, $params);
        }
        catch (AppException $ex)
        {
            // 削除された著者が見つからない場合は情報なしと扱う
            $result = array();
        }
        
        return $result;
    }
    
    /**
     * Check if it is a deleted author
     * 削除済みの著者であるかチェックする
     *
     * @param int $author_id WEKO author ID WEKO著者ID
     *
     * @return boolean Whether or not it has been deleted 削除済みかどうか
     */
    private function checkDeletedAuthor($author_id) {
        $params = array();
        $params[] = $author_id;
        $result = $this->executeSqlFile(WEBAPP_DIR."/modules/repository/components/business/nameauthority/sql/Loadauthorinformation/name_authority_select_getAuthorDeletedFlag.sql", $params);
        for($ii = 0; $ii < count($result); $ii++)
        {
            // 1件でも未削除の著者情報があれば削除済みの著者ではない
            if($result[$ii]["is_delete"] == 0)
            {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Create a JSON array that summarizes author information
     * 著者情報をまとめたJSON配列を作成する
     *
     * @param array $authorInfo Author information 著者情報
     *                          array[$ii]["author_id"|"language"|"family"|...]
     * @param array $externalAuthorInfo External author information 外部著者情報
     *                                  array[$ii]["author_id"|"prefix_id"|"suffix"|...]
     * @param array $authorList Author list of JSON array format JSON配列形式の著者リスト
     *                          array[$ii]["author_id"|"family"|"name"|...]
     */
    private function createAuthorListJsonArray($authorInfo, $externalAuthorInfo, &$authorList) {
        $author = array();
        if(count($authorInfo) > 0)
        {
            $author["author_id"] = $authorInfo[0]["author_id"];
            for($ii = 0; $ii < count($authorInfo); $ii++)
            {
                if($authorInfo[$ii]["language"] == "japanese")
                {
                    $author["family"] = $authorInfo[$ii]["family"];
                    $author["name"] = $authorInfo[$ii]["name"];
                    $author["family_ruby"] = $authorInfo[$ii]["family_ruby"];
                    $author["name_ruby"] = $authorInfo[$ii]["name_ruby"];
                }
                else if($authorInfo[$ii]["language"] == "english")
                {
                    $author["family_en"] = $authorInfo[$ii]["family"];
                    $author["name_en"] = $authorInfo[$ii]["name"];
                }
                else
                {
                    $author["family_other"] = $authorInfo[$ii]["family"];
                    $author["name_other"] = $authorInfo[$ii]["name"];
                    $author["family_other_ruby"] = $authorInfo[$ii]["family_ruby"];
                    $author["name_other_ruby"] = $authorInfo[$ii]["name_ruby"];
                }
            }
            for($ii = 0; $ii < count($externalAuthorInfo); $ii++)
            {
                if($externalAuthorInfo[$ii]["prefix_id"] == 0)
                {
                    $author["mail"] = $externalAuthorInfo[$ii]["suffix"];
                }
                else
                {
                    if(!array_key_exists("external_id", $author))
                    {
                        $author["external_id"] = array();
                    }
                    $external_id = array();
                    $external_id["id"] = $externalAuthorInfo[$ii]["prefix_id"];
                    $external_id["name"] = $externalAuthorInfo[$ii]["prefix_name"];
                    $external_id["suffix"] = $externalAuthorInfo[$ii]["suffix"];
                    $author["external_id"][] = $external_id;
                }
            }
            $author["is_delete"] = $authorInfo[0]["is_delete"];
            
            $authorList[] = $author;
        }
    }
    
    /**
     * Get the items that have been set in the feedback e-mail sent at the specified author
     * 指定の著者でフィードバックメール送信先に設定されているアイテムを取得する
     *
     * @param int $author_id WEKO author ID WEKO著者ID
     *
     * @return array Item ID List アイテムIDリスト
     *               array[$ii]["item_id"|"item_no"]
     */
    public function selectFeedbackInfo($author_id) {
        $params = array();
        $params[] = intval($author_id);
        $params[] = 0;
        $result = $this->executeSqlFile(WEBAPP_DIR."/modules/repository/components/business/nameauthority/sql/Loadauthorinformation/send_feedbackmail_author_id_select_getItemId.sql", $params);
        
        return $result;
    }
    
    /**
     * Acquire the author information necessary for name authority download
     * 著者名典拠DLに必要な著者情報を取得する
     *
     * @param int $author_id WEKO author ID WEKO著者ID
     * @param int $is_delete Delete flag 削除フラグ
     *
     * @return array Author information 著者情報
     *               array["author_id"|"family_ja"|"family_en"|...]
     */
    public function selectAuthorInfoForNameAuthorityDownload($author_id, $is_delete) {
        $result = array("author_id" => "", 
                        "family_ja" => "", 
                        "name_ja" => "", 
                        "family_ruby" => "", 
                        "name_ruby" => "", 
                        "family_en" => "", 
                        "name_en" => "", 
                        "family_other" => "", 
                        "name_other" => "", 
                        "family_other_ruby" => "", 
                        "name_other_ruby" => "", 
                        "is_delete" => "");
        
        $authorInfo = $this->selectAuthorInfo($author_id, $is_delete);
        if(count($authorInfo) > 0)
        {
            $result["author_id"] = $author_id;
            $result["is_delete"] = $is_delete;
            for($ii = 0; $ii < count($authorInfo); $ii++)
            {
                if($authorInfo[$ii]["language"] == "japanese")
                {
                    $result["family_ja"] = $authorInfo[$ii]["family"];
                    $result["name_ja"] = $authorInfo[$ii]["name"];
                    $result["family_ruby"] = $authorInfo[$ii]["family_ruby"];
                    $result["name_ruby"] = $authorInfo[$ii]["name_ruby"];
                }
                else if($authorInfo[$ii]["language"] == "english")
                {
                    $result["family_en"] = $authorInfo[$ii]["family"];
                    $result["name_en"] = $authorInfo[$ii]["name"];
                }
                else
                {
                    $result["family_other"] = $authorInfo[$ii]["family"];
                    $result["name_other"] = $authorInfo[$ii]["name"];
                    $result["family_other_ruby"] = $authorInfo[$ii]["family_ruby"];
                    $result["name_other_ruby"] = $authorInfo[$ii]["name_ruby"];
                }
            }
        }
        
        return $result;
    }
    
    /**
     * Acquire the external author information necessary for name authority download
     * 著者名典拠DLに必要な外部著者情報を取得する
     *
     * @param int $author_id WEKO author ID WEKO著者ID
     * @param int $is_delete Delete flag 削除フラグ
     *
     * @return array External author information 外部著者情報
     *               array["mail"|"external_author_id_1"|"external_author_id_2"|...]
     */
    public function selectExternalAuthorInfoForNameAuthorityDownload($author_id, $is_delete) {
        $result = array("mail" => "");
        
        $businessNameauthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        $prefixList = $businessNameauthority->getExternalAuthorIdPrefixList();
        for($ii = 0; $ii < count($prefixList); $ii++)
        {
            $result["external_author_id_".$prefixList[$ii]["prefix_id"]] = "";
        }
        
        $externalAuthorInfo = $this->selectExternalAuthorInfo($author_id, $is_delete);
        
        for($ii = 0; $ii < count($externalAuthorInfo); $ii++)
        {
            if($externalAuthorInfo[$ii]["prefix_id"] == 0)
            {
                $result["mail"] = $externalAuthorInfo[$ii]["suffix"];
            }
            else
            {
                $result["external_author_id_".$externalAuthorInfo[$ii]["prefix_id"]] = $externalAuthorInfo[$ii]["suffix"];
            }
        }
        
        return $result;
    }
    
    /**
     * Acquire item information associated with the author necessary for name authority download
     * 著者名典拠DLに必要な著者に紐付くアイテム情報を取得する
     *
     * @param int $author_id WEKO author ID WEKO著者ID
     * @param int $item_id Item ID アイテムID
     * @param int $item_no Item No アイテム通番
     *
     * @return array Item information アイテム情報
     *               array["item_id"|"title"|"title_english"|...]
     */
    public function selectItemInfoForNameAuthorityDownload($author_id, $item_id, $item_no) {
        $result = array("item_id" => $item_id, 
                        "title" => "", 
                        "title_english" => "", 
                        "url" => "", 
                        "family_ja" => "", 
                        "name_ja" => "", 
                        "family_ruby" => "", 
                        "name_ruby" => "", 
                        "family_en" => "", 
                        "name_en" => "", 
                        "family_other" => "", 
                        "name_other" => "", 
                        "mail" => "");
        
        $itemResult = $this->selectItemInfo($item_id, $item_no);
        $nameResult = $this->selectNameInfo($author_id, $item_id, $item_no);
        
        $result["title"] = $itemResult[0]["title"];
        $result["title_english"] = $itemResult[0]["title_english"];
        $result["url"] = $itemResult[0]["uri"];
        for($ii = 0; $ii < count($nameResult); $ii++)
        {
            if($nameResult[$ii]["display_lang_type"] == "japanese")
            {
                $result["family_ja"] = $nameResult[$ii]["family"];
                $result["name_ja"] = $nameResult[$ii]["name"];
                $result["family_ruby"] = $nameResult[$ii]["family_ruby"];
                $result["name_ruby"] = $nameResult[$ii]["name_ruby"];
            }
            else if($nameResult[$ii]["display_lang_type"] == "english")
            {
                $result["family_en"] = $nameResult[$ii]["family"];
                $result["name_en"] = $nameResult[$ii]["name"];
            }
            else
            {
                $result["family_other"] = $nameResult[$ii]["family"];
                $result["name_other"] = $nameResult[$ii]["name"];
            }
            $result["mail"] = $nameResult[$ii]["e_mail_address"];
        }
        
        return $result;
    }
    
    /**
     * Get item information
     * アイテム情報を取得する
     *
     * @param int $item_id Item ID アイテムID
     * @param int $item_no Item No アイテム通番
     *
     * @return array Author information 著者情報
     *               array[$ii]["item_id"|"title"|"title_english"|"uri"]
     */
    private function selectItemInfo($item_id, $item_no) {
        $params = array();
        $params[] = intval($item_id);
        $params[] = intval($item_no);
        $params[] = 0;
        $result = $this->executeSqlFile(WEBAPP_DIR."/modules/repository/components/business/nameauthority/sql/Loadauthorinformation/item_select_getItemInfo.sql", $params);
        
        return $result;
    }
    
    /**
     * Get name information
     * 氏名情報を取得する
     *
     * @param int $author_id WEKO author ID WEKO著者ID
     * @param int $item_id Item ID アイテムID
     * @param int $item_no Item No アイテム通番
     *
     * @return array Author information 著者情報
     *               array[$ii]["family"|"name"|"family_ruby"|...]
     */
    private function selectNameInfo($author_id, $item_id, $item_no) {
        $params = array();
        $params[] = intval($item_id);
        $params[] = intval($item_no);
        $params[] = intval($author_id);
        $params[] = 0;
        $params[] = 0;
        $result = $this->executeSqlFile(WEBAPP_DIR."/modules/repository/components/business/nameauthority/sql/Loadauthorinformation/personal_name_select_getNameInfo.sql", $params);
        
        return $result;
    }
    
    /**
     * Create item ID list of items associated with the author
     * 著者に紐付くアイテムのアイテムIDリストを作成する
     *
     * @param array $searchList List of items associated with designated WEKO author ID as metadata 指定のWEKO著者IDにメタデータとして紐付くアイテムのリスト
     *              array[$ii]["item_id"|"item_no"|"url"]
     * @param array $feedbackList List of items associated with designated WEKO author ID as feedback mail destination 指定のWEKO著者IDにフィードバックメール送信先として紐付くアイテムのリスト
     *              array[$ii]["item_id"|"item_no"]
     *
     * @return array Item ID list of items associated with the author 著者に紐付くアイテムのアイテムIDリスト
     *               array[$ii]["item_id"|"item_no"|"metadata_flag"|"feedback_flag"]
     */
    public function createItemIdListAssociatedWithAuthor($searchList, $feedbackList) {
        $itemList = array();
        for($ii = 0; $ii < count($searchList); $ii++)
        {
            $itemList[] = $searchList[$ii]["item_id"];
        }
        for($ii = 0; $ii < count($feedbackList); $ii++)
        {
            $itemList[] = $feedbackList[$ii]["item_id"];
        }
        $itemList = array_unique($itemList);
        sort($itemList, SORT_NUMERIC);
        
        $result = array();
        for($ii = 0; $ii < count($itemList); $ii++)
        {
            for($jj = 0; $jj < count($searchList); $jj++)
            {
                if($itemList[$ii] == $searchList[$jj]["item_id"])
                {
                    $result[] = array("item_id" => $searchList[$jj]["item_id"], 
                                      "item_no" => $searchList[$jj]["item_no"], 
                                      "metadata_flag" => true, 
                                      "feedback_flag" => false);
                    break;
                }
            }
            for($jj = 0; $jj < count($feedbackList); $jj++)
            {
                if($itemList[$ii] == $feedbackList[$jj]["item_id"])
                {
                    if(isset($result[$ii]))
                    {
                        $result[$ii]["feedback_flag"] = true;
                    }
                    else
                    {
                        $result[] = array("item_id" => $feedbackList[$jj]["item_id"], 
                                          "item_no" => $feedbackList[$jj]["item_no"], 
                                          "metadata_flag" => false, 
                                          "feedback_flag" => true);
                    }
                    break;
                }
            }
        }
        
        return $result;
    }
}
?>