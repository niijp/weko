SELECT item_id, title, title_english, uri 
FROM {repository_item} 
WHERE item_id = ? 
AND item_no = ? 
AND is_delete = ?;
