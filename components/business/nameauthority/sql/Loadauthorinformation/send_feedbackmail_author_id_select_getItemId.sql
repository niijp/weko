SELECT DISTINCT MAIL.item_id, MAIL.item_no 
FROM {repository_send_feedbackmail_author_id} MAIL 
INNER JOIN {repository_item} ITEM 
ON MAIL.item_id = ITEM.item_id 
AND MAIL.item_no = ITEM.item_no 
WHERE MAIL.author_id = ? 
AND ITEM.is_delete = ?;
