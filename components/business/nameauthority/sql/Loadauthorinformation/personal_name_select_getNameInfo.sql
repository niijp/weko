SELECT NAME.family, NAME.name, NAME.family_ruby, NAME.name_ruby, NAME.e_mail_address, ATTRTYPE.display_lang_type 
FROM {repository_personal_name} NAME, {repository_item_attr_type} ATTRTYPE
WHERE NAME.item_type_id = ATTRTYPE.item_type_id 
AND NAME.attribute_id = ATTRTYPE.attribute_id 
AND NAME.item_id = ? 
AND NAME.item_no = ? 
AND NAME.author_id = ? 
AND NAME.is_delete = ? 
AND ATTRTYPE.is_delete = ? 
ORDER BY ATTRTYPE.show_order, NAME.personal_name_no;