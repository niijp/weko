UPDATE {repository_external_author_id_suffix} 
SET is_delete = ?, del_user_id = ?, del_date = ?, mod_user_id = ?, mod_date = ? 
WHERE author_id = ? ;
