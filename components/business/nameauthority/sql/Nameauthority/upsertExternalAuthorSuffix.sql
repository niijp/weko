INSERT INTO {repository_external_author_id_suffix} (author_id, prefix_id, suffix, ins_user_id, mod_user_id, del_user_id, ins_date, mod_date, del_date, is_delete)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) 
ON DUPLICATE KEY UPDATE 
suffix = ?, mod_user_id = ?, del_user_id = ?, mod_date = ?, del_date = ?, is_delete = ?;
