SELECT count(*) AS cnt 
FROM {repository_name_authority}
WHERE author_id = ? 
AND language = ? 
AND is_delete = ?;
