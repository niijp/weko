INSERT INTO {repository_name_authority} (author_id, language, family, name, family_ruby, name_ruby, ins_user_id, mod_user_id, del_user_id, ins_date, mod_date, del_date, is_delete)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) 
ON DUPLICATE KEY UPDATE 
family = ?, name = ?, family_ruby = ?, name_ruby = ?, mod_user_id = ?, del_user_id = ?, mod_date = ?, del_date = ?, is_delete = ?;
