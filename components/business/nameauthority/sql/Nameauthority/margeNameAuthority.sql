INSERT IGNORE INTO {repository_name_authority} 
SELECT ?, language, family, name, family_ruby, name_ruby, ?, ?, ?, ?, ?, ?, ? 
FROM {repository_name_authority} 
WHERE author_id = ? 
AND is_delete = ?;
