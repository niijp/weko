SELECT suffix 
FROM {repository_external_author_id_suffix} 
WHERE author_id = ? 
AND prefix_id = ? 
AND is_delete = ?;
