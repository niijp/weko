SELECT author_id 
FROM {repository_external_author_id_suffix} 
WHERE prefix_id = ? 
AND suffix LIKE ? 
AND is_delete = ?;
