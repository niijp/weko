<?php

/**
 * Name authority business class
 * 著者名典拠ビジネスクラス
 *
 * @package     WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Nameauthority.class.php 76601 2017-02-24 11:24:13Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Business logic abstract class
 * ビジネスロジック基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/FW/BusinessBase.class.php';

/**
 * Name authority business class
 * 著者名典拠ビジネスクラス
 * 
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Components_Business_Nameauthority_Nameauthority extends BusinessBase
{
    // member
    /**
     * Block ID of arranged WEKO to NC2
     * NC2に配置されたWEKOのブロックID
     *
     * @var int
     */
    private $block_id = 0;
    /**
     * ID of the page WEKO is located
     * WEKOが配置されているページのID
     *
     * @var int
     */
    private $room_id = 0;
    
    /**
     * Set block id
     * ブロックID設定
     *
     * @param int $block_id Block id ブロックID
     */
    public function setBlockId($block_id){
        $this->block_id = $block_id;
    }
    
    /**
     * Set room id
     * ルームID設定
     *
     * @param int $room_id Room id ルームID
     */
    public function setRoomId($room_id){
        $this->room_id = $room_id;
    }
    
    /**
     * Name authority data insertion
     * 著者名典拠データ挿入
     *
     * @param array $params Insert parameters 挿入パラメータ
     *                      array["author_id"|"language"|"family"|"name"|"family_ruby"|"name_ruby"|"ins_user_id"|"mod_user_id"|"del_user_id"|"ins_date"|"mod_date"|"del_date"|"is_delete"]
     * @return boolean Execution result 実行結果
     */
    private function insNameAuthority($params=array())
    {
        $result = $this->Db->insertExecute("repository_name_authority", $params);
        if ($result === false) {
            return false;
        }
        return true;
    }
    
    /**
     * Name authority data update
     * 著者名典拠データ更新
     *
     * @param array $params Update parameters 更新パラメータ
     *                      array["author_id"|"language"|"family"|"name"|"family_ruby"|"name_ruby"|"ins_user_id"|"mod_user_id"|"del_user_id"|"ins_date"|"mod_date"|"del_date"|"is_delete"]
     * @param array $where_params Condition parameters 条件パラメータ
     *                            array["author_id"|"language"|"family"|"name"|"family_ruby"|"name_ruby"|"ins_user_id"|"mod_user_id"|"del_user_id"|"ins_date"|"mod_date"|"del_date"|"is_delete"]
     * @return boolean Execution result 実行結果
     */
    private function updNameAuthority($params=array(), $where_params=array())
    {
        $result = $this->Db->updateExecute("repository_name_authority", $params, $where_params);
        if ($result === false) {
            return false;
        }
        return true;
    }
    
    /**
     * Name authority acquisition
     * 著者名典拠取得
     * 
     * @param array $where_params Condition parameters 条件パラメータ
     *                            array["author_id"|"language"|"family"|"name"|"family_ruby"|"name_ruby"|"ins_user_id"|"mod_user_id"|"del_user_id"|"ins_date"|"mod_date"|"del_date"|"is_delete"]
     * @param array order_params Order parameters 順序パラメータ
     *                           array["author_id"|"language"|"family"|"name"|"family_ruby"|"name_ruby"|"ins_user_id"|"mod_user_id"|"del_user_id"|"ins_date"|"mod_date"|"del_date"|"is_delete"]
     * @param function Callback function コールバック関数
     * @param array Callback argument コールバック引数
     * @return array Execution result 実行結果
     *                         array[$ii]["author_id"|"language"|"family"|"name"|"family_ruby"|"name_ruby"|"ins_user_id"|"mod_user_id"|"del_user_id"|"ins_date"|"mod_date"|"del_date"|"is_delete"]
     */
    public function getNameAuthority($where_params=array(), $order_params=array(), $func = null, $func_param = null)
    {
        $result = $this->Db->selectExecute("repository_name_authority", $where_params, $order_params, null, null, $func, $func_param);
        if ($result === false) {
            return $result;
        }
        return $result;
    }
    
    /**
     * External author ID prefix insertion
     * 外部著者ID prefix挿入
     *
     * @param array $params Insert parameters 挿入パラメータ
     *                      array["prefix_id"|"prefix_name"|"url"|"block_id"|"room_id"|"ins_user_id"|"mod_user_id"|"del_user_id"|"ins_date"|"mod_date"|"del_date"|"is_delete"]
     * @return boolean Execution result 実行結果
     */
    private function insExternalAuthorIdPrefix($params=array())
    {
        $result = $this->Db->insertExecute("repository_external_author_id_prefix", $params);
        if ($result === false) {
            return false;
        }
        return true;
    }
    
    /**
     * External author ID prefix update
     * 外部著者ID prefix更新
     *
     * @param array $params Update parameters 挿入パラメータ
     *                      array["prefix_id"|"prefix_name"|"url"|"block_id"|"room_id"|"ins_user_id"|"mod_user_id"|"del_user_id"|"ins_date"|"mod_date"|"del_date"|"is_delete"]
     * @param array $where_params Condition parameters 条件パラメータ
     *                            array["prefix_id"|"prefix_name"|"url"|"block_id"|"room_id"|"ins_user_id"|"mod_user_id"|"del_user_id"|"ins_date"|"mod_date"|"del_date"|"is_delete"]
     * @return boolean Execution result 実行結果
     */
    private function updExternalAuthorIdPrefix($params=array(), $where_params=array())
    {
        $result = $this->Db->updateExecute("repository_external_author_id_prefix", $params, $where_params);
        if ($result === false) {
            return false;
        }
        return true;
    }
    
    /**
     * External author ID prefix acquisition
     * 外部著者ID prefix取得
     * 
     * @param array $where_params Condition parameters 条件パラメータ
     *                            array["prefix_id"|"prefix_name"|"url"|"block_id"|"room_id"|"ins_user_id"|"mod_user_id"|"del_user_id"|"ins_date"|"mod_date"|"del_date"|"is_delete"]
     * @param array $order_params Order parameters 順序パラメータ
     *                           array["prefix_id"|"prefix_name"|"url"|"block_id"|"room_id"|"ins_user_id"|"mod_user_id"|"del_user_id"|"ins_date"|"mod_date"|"del_date"|"is_delete"]
     * @param function $func Callback function コールバック関数
     * @param array $func_param Callback argument コールバック引数
     * @return array Execution result 実行結果
     *               array[$ii]["prefix_id"|"prefix_name"|"url"|"block_id"|"room_id"|"ins_user_id"|"mod_user_id"|"del_user_id"|"ins_date"|"mod_date"|"del_date"|"is_delete"]
     */
    public function getExternalAuthorIdPrefix($where_params=array(), $order_params=array(), $func = null, $func_param = null)
    {
        $result = $this->Db->selectExecute("repository_external_author_id_prefix", $where_params, $order_params, null, null, $func, $func_param);
        if ($result === false) {
            return $result;
        }
        return $result;
    }
    
    /**
     * External author ID prefix list acquisition
     * 外部著者ID prefix一覧取得
     *
     * @return array External author ID prefix list 外部著者ID prefix一覧
     *               array[$ii]["prefix_id"|"prefix_name"|"block_id"|"room_id"]
     */
    public function getExternalAuthorIdPrefixList(){
        $query = "SELECT prefix_id, prefix_name, url, block_id, room_id ".
                 "FROM ".DATABASE_PREFIX."repository_external_author_id_prefix ".
                 "WHERE ((block_id = 0 AND room_id = 0) OR (block_id = ? AND room_id = ?)) ".
                 "AND is_delete = 0 ".
                 "AND prefix_id > 0 ".
                 "ORDER BY prefix_id ASC;";
        $params = array();
        $params[] = $this->block_id;  // block_id
        $params[] = $this->room_id;  // room_id
        $result = $this->executeSql($query, $params);
        
        return $result;
    }
    
    /**
     * External author ID prefix and suffix acquisition
     * 外部著者ID prefixおよびsuffix取得
     *
     * @param int $author_id Author id 著者ID
     * @param boolean $getEmailFlag Whether or not to get the e-mail address メールアドレスを取得するか否か
     * @return array External author ID prefix and suffix 外部著者ID prefixおよびsuffix
     *               array[$ii]["suffix.prefix_id"|"suffix.suffix"]
     */
    public function getExternalAuthorIdPrefixAndSuffix($author_id, $getEmailFlag=false){
        $query = "SELECT suffix.prefix_id, suffix.suffix ".
                 "FROM ".DATABASE_PREFIX."repository_external_author_id_suffix AS suffix, ".
                 "     ".DATABASE_PREFIX."repository_external_author_id_prefix AS prefix ".
                 "WHERE suffix.author_id = ? ".
                 "AND suffix.prefix_id = prefix.prefix_id ".
                 "AND ((prefix.block_id = 0 AND prefix.room_id = 0) OR (prefix.block_id = ? AND prefix.room_id = ?)) ".
                 "AND prefix.is_delete = 0 ".
                 "AND suffix.is_delete = 0 ";
        if(!$getEmailFlag)
        {
            $query .= "AND prefix.prefix_id > 0 ";
        }
        $query .= "ORDER BY suffix.author_id ASC;";
        $params = array();
        $params[] = $author_id; // author_id
        $params[] = $this->block_id;  // block_id
        $params[] = $this->room_id;   // room_id
        $result = $this->executeSql($query, $params);
        if(count($result)==0){
            $result = array(array('prefix_id'=>'', 'suffix'=>''));
        }
        return $result;
    }
    
    /**
     * External author ID prefix added
     * 外部著者ID prefix追加
     *
     * @param string $prefix_name External author ID prefix name 外部著者ID prefix名
     * @param int $prefix_id Unique ID of the external author ID prefix 外部著者ID prefixのユニークID
     * @return boolean Execution result 実行結果
     */
    public function addExternalAuthorIdPrefix($prefix_name, $prefix_id=0){
        if($prefix_id==0){
            $prefix_id = $this->getNewPrefixId();
        }
        $params = array(
                        "prefix_id" => $prefix_id,
                        "prefix_name" => $prefix_name,
                        "block_id" => $this->block_id,
                        "room_id" => $this->room_id,
                        "ins_user_id" => $this->user_id,
                        "mod_user_id" => $this->user_id,
                        "del_user_id" => 0,
                        "ins_date" => $this->accessDate,
                        "mod_date" => $this->accessDate,
                        "del_date" => "",
                        "is_delete" => 0
                    );
        $result = $this->insExternalAuthorIdPrefix($params);
        if($result === false){
            return false;
        }
        return $prefix_id;
    }
    
    /**
     * External author ID prefix update
     * 外部著者ID prefix更新
     *
     * @param int $prefix_id Unique ID of the external author ID prefix 外部著者ID prefixのユニークID
     * @param string $url External author ID URL 外部著者ID URL
     * @return boolean Execution result 実行結果
     */
    private function updateExternalAuthorIdPrefix($prefix_id, $url){
        $params = array(
                        "url" => $url,
                        "mod_user_id" => $this->user_id,
                        "del_user_id" => 0,
                        "mod_date" => $this->accessDate,
                        "del_date" => "",
                        "is_delete" => 0
                    );
        $where_params = array("prefix_id" => $prefix_id);
        $result = $this->updExternalAuthorIdPrefix($params, $where_params);
        if($result===false){
            return false;
        }
        return true;
    }
    
    /**
     * New unique ID acquisition of external author ID
     * 外部著者IDの新規ユニークID取得
     *
     * @return int Unique ID ユニークID
     */
    private function getNewPrefixId(){
        $new_prefix_id = intval($this->Db->nextSeq("repository_external_author_id_prefix"));
        return $new_prefix_id;
    }
    
    /**
     * Entry external authorID prefix data
     * 外部著者IDのプレフィックス情報を追加する
     *
     * @param array $prefix_data External author id prefix data
     *                           外部著者IDのプレフィックス情報
     *                           array[$ii]["prefix_id"|"prefix_name"|"url"]
     *
     * @return boolean Whether or not entry external authorID prefix data success
     *                 外部著者IDのプレフィックス情報の追加に成功したかどうか
     */
    public function entryExternalAuthorIdPrefix($prefix_data){
        // Delete record by block_id and room_id
        $params = array(
                        "mod_user_id" => $this->user_id,
                        "del_user_id" => $this->user_id,
                        "mod_date" => $this->accessDate,
                        "del_date" => $this->accessDate,
                        "is_delete" => 1,
                    );
        $where_params = array(
                                "block_id" => $this->block_id,
                                "room_id" => $this->room_id,
                                "prefix_id!=0" => null,
                                "prefix_id!=1" => null,
                                "prefix_id!=2" => null,
                                "prefix_id!=3" => null
                            );
        $result = $this->updExternalAuthorIdPrefix($params, $where_params);
        if($result===false){
            return false;
        }
        
        // Update or Insert record
        for($ii=0;$ii<count($prefix_data);$ii++){
            if($prefix_data[$ii]["prefix_name"]!="e_mail_address"){
                if(($prefix_data[$ii]["prefix_id"]==0 || $prefix_data[$ii]["prefix_id"]==null) && $prefix_data[$ii]["prefix_name"]!=""){
                    // Insert record
                    $prefixId = $this->addExternalAuthorIdPrefix($prefix_data[$ii]["prefix_name"]);
                    if($prefixId===false){
                        return false;
                    }
                    $result = $this->updateExternalAuthorIdPrefix($prefixId, $prefix_data[$ii]["url"]);
                    if($result===false){
                        return false;
                    }
                } else if($prefix_data[$ii]["prefix_name"]!="") {
                    // Update record
                    $result = $this->updateExternalAuthorIdPrefix($prefix_data[$ii]["prefix_id"], $prefix_data[$ii]["url"]);
                    if($result===false){
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    /**
     * Get new author_id
     * 新規著者ID取得
     * 
     * @return int Author id 著者ID
     */
    public function getNewAuthorId(){
        $query = "SELECT MAX(author_id) FROM ".DATABASE_PREFIX."repository_name_authority;";
        $result = $this->executeSql($query);
        
        $author_id = intval($result[0]['MAX(author_id)'])+1;
        return $author_id;
    }
    
    /**
     * Name authority data acquisition
     * 著者名典拠データ取得
     *
     * @param int $author_id Author id 著者ID
     * @param string $language Language 言語
     * @return array Name authority data 著者名典拠データ
     *               array[$ii]["author_id"|"language"|"family"|"name"|"family_ruby"|"name_ruby"|"ins_user_id"|"mod_user_id"|"del_user_id"|"ins_date"|"mod_date"|"del_date"|"is_delete"]
     */
    public function getNameAuthorityData($author_id, $language){
        $where_params = array(
                                "author_id" => $author_id,
                                "language" => $language,
                                "is_delete" => 0
                            );
        $order_params = array("author_id" => "ASC");
        $result = $this->getNameAuthority($where_params, $order_params);
        return $result;
    }
    
    /**
     * Get the external author ID prefix name
     * 外部著者ID prefix名を取得
     *
     * @param int $prefix_id Unique ID ユニークID
     * @return string External author ID prefix name 外部著者ID prefix名
     */
    public function getExternalAuthorIdPrefixName($prefix_id){
        $where_params = array("prefix_id" => $prefix_id, "is_delete" => 0);
        $result = $this->getExternalAuthorIdPrefix($where_params);
        if($result===false){
            return false;
        }
        return $result[0]["prefix_name"];
    }
    
    /**
     * External author ID prefix and suffix get attached straps to the author ID
     * 著者IDに紐付く外部著者ID prefixおよびsuffix取得
     *
     * @param int $author_id Author id 著者ID
     * @return array Execution result 実行結果
     *               array[$ii]["SUFFIX.author_id"|"SUFFIX.prefix_id"|"PREFIX.prefix_name"|"SUFFIX.suffix"]
     */
    public function getExternalAuthorIdData($author_id){
        $query = "SELECT SUFFIX.author_id, SUFFIX.prefix_id, PREFIX.prefix_name, SUFFIX.suffix ".
                 "FROM ".DATABASE_PREFIX."repository_external_author_id_suffix AS SUFFIX, ".
                 "     ".DATABASE_PREFIX."repository_external_author_id_prefix AS PREFIX ".
                 "WHERE SUFFIX.author_id = ? ".
                 "AND SUFFIX.prefix_id = PREFIX.prefix_id ".
                 "AND ((PREFIX.block_id = 0 AND PREFIX.room_id = 0) OR (PREFIX.block_id = ? AND PREFIX.room_id = ?)) ".
                 "AND PREFIX.is_delete = 0 ".
                 "AND SUFFIX.is_delete = 0 ".
                 "AND PREFIX.prefix_id > 0 ".
                 "ORDER BY SUFFIX.author_id ASC;";
        $params = array();
        $params[] = $author_id; // author_id
        $params[] = $this->block_id;  // block_id
        $params[] = $this->room_id;   // room_id
        $result = $this->executeSql($query, $params);
        
        return $result;
    }
    
    /**
     * Unique ID acquisition of external author ID prefix
     * 外部著者ID prefixのユニークID取得
     *
     * @param string $prefix_name External author ID prefix name 外部著者ID prefix名
     * @return int Unique ID of external author ID prefix
     */
    public function getExternalAuthorIdPrefixId($prefix_name){
        $where_params = array(
                                "prefix_name" => $prefix_name,
                                "block_id" => $this->block_id,
                                "room_id" => $this->room_id,
                                "is_delete" => 0
                            );
        $result = $this->getExternalAuthorIdPrefix($where_params);
        if($result===false){
            return false;
        }
        if(count($result)>0 && strlen($result[0]["prefix_id"])>0){
            $prefix_id = intval($result[0]["prefix_id"]);
        } else {
            $prefix_id = 0;
        }
        return $prefix_id;
    }
    

    
    /**
     * Search for author information for suggestions display
     * サジェスト表示用に著者情報を検索
     *
     * @param string $surName Last name 姓
     * @param string $givenName Name 名
     * @param string $surNameRuby Last name (reading) 姓(ヨミ)
     * @param string $givenNameRuby Name (reading) 名(ヨミ)
     * @param string $emailAddress Mail address メールアドレス
     * @param string $externalAuthorID External author ID suffix 外部著者ID suffix
     * @param string $language Language 言語
     * @return array Author information 著者情報
     *               array[$ii]["AUTHOR.author_id"|"AUTHOR.family"|"$surNameRuby"|"AUTHOR.family_ruby"|"AUTHOR.name_ruby"|"SUFFIX.suffix"]
     */
    public function searchSuggestData($surName, $givenName, $surNameRuby, $givenNameRuby, $emailAddress, $externalAuthorID, $language=""){
        $query = "SELECT DISTINCT AUTHOR.author_id, AUTHOR.family, AUTHOR.name, ".
                 "AUTHOR.family_ruby, AUTHOR.name_ruby, SUFFIX.suffix ".
                 "FROM ".DATABASE_PREFIX."repository_name_authority AS AUTHOR ".
                 "LEFT JOIN ".
                 "( SELECT author_id, suffix FROM ".DATABASE_PREFIX."repository_external_author_id_suffix WHERE prefix_id = 0 AND is_delete = 0) AS SUFFIX ".
                 "ON AUTHOR.author_id = SUFFIX.author_id ";
        $where_query = "";
        $params = array();
        if(strlen($surName)>0){
            if(strlen($where_query)>0){
                $where_query .= "AND ";
            } else {
                $where_query .= "WHERE ";
            }
            $where_query .= "AUTHOR.family LIKE ? ";
            $params[] = $surName."%";
        }
        if(strlen($givenName)>0){
            if(strlen($where_query)>0){
                $where_query .= "AND ";
            } else {
                $where_query .= "WHERE ";
            }
            $where_query .= "AUTHOR.name LIKE ? ";
            $params[] = $givenName."%";
        }
        if(strlen($surNameRuby)>0){
            if(strlen($where_query)>0){
                $where_query .= "AND ";
            } else {
                $where_query .= "WHERE ";
            }
            $where_query .= "AUTHOR.family_ruby LIKE ? ";
            $params[] = $surNameRuby."%";
        }
        if(strlen($givenNameRuby)>0){
            if(strlen($where_query)>0){
                $where_query .= "AND ";
            } else {
                $where_query .= "WHERE ";
            }
            $where_query .= "AUTHOR.name_ruby LIKE ? ";
            $params[] = $givenNameRuby."%";
        }
        if(strlen($emailAddress)>0){
            if(strlen($where_query)>0){
                $where_query .= "AND ";
            } else {
                $where_query .= "WHERE ";
            }
            $where_query .= "SUFFIX.suffix LIKE ? ";
            $params[] = $emailAddress."%";
        }
        if(strlen($externalAuthorID)>0){
            $authorId = $this->getSuggestAuthorBySuffix($externalAuthorID);
            if(count($authorId) > 0) {
                if(strlen($where_query)>0){
                    $where_query .= "AND ";
                } else {
                    $where_query .= "WHERE ";
                }
                for($cnt = 0; $cnt < count($authorId); $cnt++)
                {
                    if($cnt == 0)
                    {
                        $where_query .= "AUTHOR.author_id IN( ?";
                        $params[] = $authorId[$cnt]["author_id"];
                    }
                    else
                    {
                        $where_query .= ", ?";
                        $params[] = $authorId[$cnt]["author_id"];
                    }
                }
                $where_query .= ") ";
            }
            else
            {
                return array();
            }
        }
        if(strlen($language)>0){
            if(strlen($where_query)>0){
                $where_query .= "AND ";
            } else {
                $where_query .= "WHERE ";
            }
            $where_query .= "(AUTHOR.language = ? ".
                            "OR AUTHOR.language = '') ";
            $params[] = $language;  // Selected languege
        }
        
        if(strlen($where_query)>0){
            $where_query .= "AND ";
        } else {
            $where_query .= "WHERE ";
        }
        $where_query .= "AUTHOR.is_delete = ? ";
        $params[] = 0;
        
        $query .= $where_query."ORDER BY AUTHOR.author_id ASC;";
        $result = $this->executeSql($query, $params);
        
        return $result;
    }
    
    /**
     * duplicate key insert external author id
     * 外部著者IDを上書き保存する
     *
     * @param array $extAuthorIdArray External author ID prefix and suffix 外部著者ID prefixおよびsuffix
     *                                array[$ii]["prefix_id"|"suffix"|"old_prefix_id"|"old_suffix"|"prefix_name"]
     * @param int $authorId Author id 著者ID
     */
    private function upsertExternalAuthorId($extAuthorIdArray, $authorId){
        // Prefixが配列内に含まれているので、それを利用して保存する
        // 上書きする必要があるため、Duplicate Key Insertを利用する
        for($ii = 0; $ii < count($extAuthorIdArray); $ii++){
            $params = array();
            $params[] = $authorId;
            $params[] = $extAuthorIdArray[$ii]["prefix_id"];
            $params[] = $extAuthorIdArray[$ii]["suffix"];
            $params[] = $this->user_id;
            $params[] = $this->user_id;
            $params[] = "";
            $params[] = $this->accessDate;
            $params[] = $this->accessDate;
            $params[] = "";
            $params[] = 0;
            $params[] = $extAuthorIdArray[$ii]["suffix"];
            $params[] = $this->user_id;
            $params[] = "";
            $params[] = $this->accessDate;
            $params[] = "";
            $params[] = 0;
            
            $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Nameauthority/upsertExternalAuthorSuffix.sql", $params);
        }
    }
    
    /**
     * Get a list of the author ID that partially match the external author ID
     * 外部著者ID群に部分一致する著者IDの一覧を取得する
     *
     * @param array $extAuthorIdArray External author ID prefix and suffix 外部著者ID prefixおよびsuffix
     *                                array[$ii]["prefix_id"|"suffix"|"old_prefix_id"|"old_suffix"|"prefix_name"]
     * @return array List of author ID that matches the one of the external author ID 外部著者IDのいずれかに一致した著者IDの一覧
     *               $authorIds[$ii]["author_id"]
     */
    private function selectAuthorIdList($extAuthorIdArray){
        $params = array();
        $whereString = "";
        for($ii = 0; $ii < count($extAuthorIdArray); $ii++){
            if(strlen($whereString) > 0){
                $whereString .= " OR ";
            } else {
                $whereString = " WHERE ";
            }
            $whereString .= " ( prefix_id = ? AND suffix = ? AND is_delete = ? ) ";
            $params[] = $extAuthorIdArray[$ii]["prefix_id"];
            $params[] = $extAuthorIdArray[$ii]["suffix"];
            $params[] = 0;
        }
        
        // 入力された外部著者IDとの比較のため、suffixが部分一致する著者IDのリストを作成する
        $query = "SELECT author_id ". 
                 " FROM ". DATABASE_PREFIX. "repository_external_author_id_suffix ". 
                 $whereString. 
                 " GROUP BY author_id ". 
                 " ORDER BY COUNT(author_id) DESC, author_id ASC;";
        
        $authorIds = $this->executeSql($query, $params);
        
        return $authorIds;
    }
    
    /**
     * With the exception of the non-input, it is confirmed that there is no difference 
     * in the external author ID stick string to an external author ID and the author ID
     * 未入力を除き、外部著者ID群と著者IDに紐付く外部著者ID群に差異がないことを確認する
     * 
     * [Attention]
     *   This method does not work properly if $ exAuthorIdArray contains exclusion targets such as "& EMPTY &".
     *   Be sure to call the "exclusionIdentifyAuthoId" method to make a judgment and make an identification judgment by this method.
     * 
     * 【注意事項】
     *   本メソッドは$extAuthorIdArrayに「&EMPTY&」など同定判定の除外対象が含まれる場合正常に動作しない。
     *   必ず「exclusionIdentifyAuthoId」メソッドを呼び出して判定してから本メソッドによる同定判定を行うこと。
     *
     * @param array $extAuthorIdArray External author ID prefix and suffix 外部著者ID prefixおよびsuffix
     *                                array[$ii]["prefix_id"|"suffix"|"old_prefix_id"|"old_suffix"|"prefix_name"]
     * @param int $authorId Authur id 著者ID
     * @return boolean Whether or not there is a difference in the database and input データベースと入力に差異があるか否か
     */
    private function isDiffExternalAuthorId($extAuthorIdArray, $authorId){
        // 著者IDのprefixおよびsuffixを全て取得する
        $query = "SELECT prefix_id, suffix ". 
                 " FROM ". DATABASE_PREFIX. "repository_external_author_id_suffix ". 
                 " WHERE author_id = ? ". 
                 " AND is_delete = ?;";
        $params = array();
        $params[] = $authorId;
        $params[] = 0;
        $result = $this->executeSql($query, $params);
        
        // 入力された外部著者ID群と比較する
        $isDiff = false;
        for($jj = 0; $jj < count($extAuthorIdArray); $jj++){
            for($kk = 0; $kk < count($result); $kk++){
                if($extAuthorIdArray[$jj]["prefix_id"] == $result[$kk]["prefix_id"]){
                    if(strcmp($extAuthorIdArray[$jj]["suffix"], $result[$kk]["suffix"]) == 0){
                        // 問題無し
                        break;
                    } else {
                        // 入力された外部著者ID群と著者IDを指定した外部著者ID群の間に差異がある
                        $isDiff = true;
                        break;
                    }
                }
            }
        }
        
        if($isDiff === false){
            // 外部著者ID群と著者IDに紐付く外部著者ID群に差異がない。
            return false;
        } else {
            // 外部著者ID群と著者IDに紐付く外部著者ID群に差異がある。
            return true;
        }
    }
    
    /**
     * identify author id by input external id list and database
     * データベースに登録されている外部著者IDと入力された外部著者ID群から著者を特定し、外部著者IDを登録する
     *
     * @param array $extAuthorIdArray External author ID prefix and suffix 外部著者ID prefixおよびsuffix
     *                                array[$ii]["prefix_id"|"suffix"|"old_prefix_id"|"old_suffix"|"prefix_name"]
     * @param int $authorId Author id 著者ID
     * @return int Author id 著者ID
     */
    private function identifyAuthorId($extAuthorIdArray, $authorId){
        $retAuthorId = 0;
        if(!isset($authorId) || $authorId === 0){
            // 著者の新規登録時
            $retAuthorId = $this->identifyAuthorIdForNew($extAuthorIdArray);
        } else {
            // 著者の更新時
            $retAuthorId = $this->identifyAuthorIdForEdit($extAuthorIdArray, $authorId);
        }
        return $retAuthorId;
    }
    
    
    /**
     * identify author id by external id for new
     * 著者新規登録時に外部著者ID群より著者IDを特定し、外部著者IDを登録する
     *
     * @param array $extAuthorIdArray External author ID prefix and suffix 外部著者ID prefixおよびsuffix
     *                                array[$ii]["prefix_id"|"suffix"|"old_prefix_id"|"old_suffix"|"prefix_name"]
     * @return int Corresponding author ID(0: None) 該当著者ID(0:該当なし)
     */
    private function identifyAuthorIdForNew($extAuthorIdArray){
        $authorId = 0;
        
        // 外部著者ID群の要素が0である時、確実に該当著者は存在しない
        if(count($extAuthorIdArray) > 0){
            
            // Mod WEKO-350 著者名典拠管理 2017/01/23 Y.Nakao --start--
            // 同定の除外ルールを適用した外部著者IDリストを用いて著者を同定する。
            $tmpExtAuthorIdArray = $this->exclusionIdentifyAuthoId($extAuthorIdArray);
            if(count($tmpExtAuthorIdArray) > 0)
            {
                // 外部著者ID群に部分一致する著者IDの一覧を取得する
                $authorIds = $this->selectAuthorIdList($extAuthorIdArray);
                
                for($ii = 0; $ii < count($authorIds); $ii++){
                    // 入力された外部著者IDとデータベースに保存されている著者IDに紐付く外部著者IDで差異があるかを調べる
                    // 未入力分は互いに無視される
                    if(!$this->isDiffExternalAuthorId($tmpExtAuthorIdArray, $authorIds[$ii]["author_id"])){
                        $authorId = $authorIds[$ii]["author_id"];
                        break;
                    }
                }
            }
            // Mod WEKO-350 著者名典拠管理 2017/01/23 Y.Nakao --end--
            
            // upsert
            if($authorId === 0){
                $authorId = $this->getNewAuthorId();
            }
            
            $this->upsertExternalAuthorId($extAuthorIdArray, $authorId);
        } else {
            // 外部著者ID群が空だった場合でも著者IDの発番は実施する
            // 外部著者IDを登録はしないが個人名や著者名典拠にデータを入力するために必要
            $authorId = $this->getNewAuthorId();
        }
        
        return $authorId;
    }
    
    /**
     * is exists mail address by external id list
     * 外部著者ID群内にメールアドレスが存在するかを確認する
     *
     * @param array $extAuthorIdArray External author ID prefix and suffix 外部著者ID prefixおよびsuffix
     *                                array[$ii]["prefix_id"|"suffix"|"old_prefix_id"|"old_suffix"|"prefix_name"]
     * @return boolean Whether or not the e-mail address in the external author ID group is present 外部著者ID群の中にメールアドレスが存在するか否か
     */
    private function isExistsMailaddress($extAuthorIdArray){
        // 外部著者ID群の中からpreifxが0である値がないかを探す
        for($ii = 0; $ii < count($extAuthorIdArray); $ii++){
            if($extAuthorIdArray[$ii]["prefix_id"] === 0){
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * identify author id by external id for editting
     * 編集している著者の著者IDを外部著者IDから特定し、外部著者IDを登録する
     *
     * @param array $extAuthorIdArray External author ID prefix and suffix 外部著者ID prefixおよびsuffix
     *                                array[$ii]["prefix_id"|"suffix"|"old_prefix_id"|"old_suffix"|"prefix_name"]
     * @param int $editAuthorId Edited author ID 編集中著者ID
     * @return int Author ID that you identified from outside the author ID 外部著者IDから特定した著者ID
     */
    private function identifyAuthorIdForEdit($extAuthorIdArray, $editAuthorId){
        if(count($extAuthorIdArray) === 0){
            // 外部著者IDが空である場合、著者の同定を実施することはできない
            // 著者の同定も行われないため、入力された著者IDを返す
            return $editAuthorId;
        }
        
        $authorId = 0;
        
        // Mod WEKO-350 著者名典拠管理 2017/01/23 Y.Nakao --start--
        // 編集中著者IDに紐付く外部著者ID群と入力の外部著者ID群と比較する
        $tmpExtAuthorIdArray = $this->exclusionIdentifyAuthoId($extAuthorIdArray);
        if(count($tmpExtAuthorIdArray) > 0){
            if($this->isDiffExternalAuthorId($tmpExtAuthorIdArray, $editAuthorId)){
                // 差異があるならば新しく同定可能な著者IDを探し、登録を実施する
                $authorId = $this->identifyAuthorIdForNew($extAuthorIdArray);
            } else {
                // 外部著者IDを保存する
                $authorId = $editAuthorId;
                
                // 差異がないなら追加分を含めて登録を実施する
                $this->upsertExternalAuthorId($extAuthorIdArray, $authorId);
            }
        } else {
            // 外部著者IDを保存する
            $authorId = $editAuthorId;
            
            // 差異がないなら追加分を含めて登録を実施する
            $this->upsertExternalAuthorId($extAuthorIdArray, $authorId);
        }
        // Mod WEKO-350 著者名典拠管理 2017/01/23 Y.Nakao --endt--
        
        return $authorId;
    }
    
    /**
     * Author name authority to perform identification process data registration
     * 同定処理を行う著者名典拠データ登録
     * 
     * @param array $metadata Author information 著者メタデータ
     *                        array["family"|"name"|"family_ruby"|"name_ruby"|"author_id"|"language"]
     *                        array["external_author_id"][x]["prefix_id"|"suffix"]
     */
    public function entryNameAuthority($metadata){
        if(count($metadata)==0){
            $this->errorLog("Cannot regist author data.", __FILE__, __CLASS__, __LINE__);
            return false;
        }
        $metadata["author_id"] = $this->identifyAuthorId($metadata["external_author_id"], $metadata["author_id"]);
        
        // Check exist same author ID
        $result = $this->getNameAuthorityData($metadata["author_id"], $metadata["language"]);
        if(count($result)==0){
            // Mod WEKO-350 著者名典拠管理 2017/01/23 Y.Nakao --start--
            // 著者名典拠を追加する。
            // ※著者名典拠データを削除できるようになったので、INSERTではなくUPSERTで追加・更新する。
            $result = $this->upsertNameAuthority($metadata["author_id"], $metadata["language"], $metadata["family"], $metadata["name"], $metadata["family_ruby"], $metadata["name_ruby"]);
            if($result === false){
                $this->errorLog($this->Db->ErrorMsg(), __FILE__, __CLASS__, __LINE__);
                return false;
            }
            // Mod WEKO-350 著者名典拠管理 2017/01/23 Y.Nakao --end--
        } else if(count($result)>0){
            // Add author data
            // 空のカラムがある場合、追加更新を行う
            $update_params = array();
            $where_params = array();
            if(strlen($result[0]["family"])==0 && strlen($metadata["family"])>0){
                $update_params["family"] = $metadata["family"];
            }
            if(strlen($result[0]["name"])==0 && strlen($metadata["name"])>0){
                $update_params["name"] = $metadata["name"];
            }
            if(strlen($result[0]["family_ruby"])==0 && strlen($metadata["family_ruby"])>0){
                $update_params["family_ruby"] = $metadata["family_ruby"];
            }
            if(strlen($result[0]["name_ruby"])==0 && strlen($metadata["name_ruby"])>0){
                $update_params["name_ruby"] = $metadata["name_ruby"];
            }
            if(count($update_params)>0){
                $update_params["mod_user_id"] = $this->user_id;
                $update_params["mod_date"] = $this->accessDate;
                $where_params = array(
                                    "author_id" => $metadata["author_id"],
                                    "language" => $metadata["language"]
                                );
                $result = $this->updNameAuthority($update_params, $where_params);
                if($result === false){
                    $this->errorLog($this->Db->ErrorMsg(), __FILE__, __CLASS__, __LINE__);
                    return false;
                }
            }
        }
        
        return $metadata["author_id"];
    }
    
    /**
     * Get the author information from external author ID
     * 外部著者IDから著者情報を取得
     *
     * @param int $prefixId Unique ID of the external author ID prefix 外部著者ID prefixのユニークID
     * @param string $suffix External author ID suffix 外部著者ID suffix
     * @return array Author information 著者情報
     *               array[$ii]["AUTHOR.author_id"|"AUTHOR.language"|"AUTHOR.family"|"AUTHOR.name"|"AUTHOR.family_ruby"|"AUTHOR.name_ruby"|"SUFFIX.prefix_id"|"SUFFIX.suffix"]
     *               array[$ii]["external_author_id"][$ii]["suffix.prefix_id"|"suffix.suffix"]
     */
    public function getAuthorByPrefixAndSuffix($prefixId, $suffix){
        $query = "SELECT AUTHOR.author_id, AUTHOR.language, AUTHOR.family, ".
                 "AUTHOR.name, AUTHOR.family_ruby, AUTHOR.name_ruby, ".
                 "SUFFIX.prefix_id, SUFFIX.suffix ".
                 "FROM ". DATABASE_PREFIX ."repository_external_author_id_suffix AS SUFFIX ".
                 "INNER JOIN ".DATABASE_PREFIX ."repository_name_authority AS AUTHOR ".
                 "ON SUFFIX.author_id = AUTHOR.author_id ".
                 "WHERE SUFFIX.prefix_id = ? ".
                 "AND SUFFIX.suffix = ? ".
                 "AND SUFFIX.prefix_id >= 0 ".
                 "AND SUFFIX.is_delete = 0 ".
                 "AND AUTHOR.is_delete = 0;";
        $params = array();
        $params[] = $prefixId;    // prefix_id
        $params[] = $suffix;    // suffix
        // Execution SELECT
        $author_id_suffix = $this->executeSql($query, $params);
        
        if(count($author_id_suffix) != 0){
            for($ii=0; $ii<count($author_id_suffix); $ii++){
                $author_id_suffix[$ii]["external_author_id"] = $this->getExternalAuthorIdPrefixAndSuffix($author_id_suffix[$ii]["author_id"], true);
            }
        }
        return $author_id_suffix;
    }
    
    /**
     * Get author by PrefixID and Suffix
     * 著者IDを外部著者IDから取得
     *
     * @param string $suffix External author ID suffix 外部著者ID suffix
     * @return int Author id 著者ID
     */
    public function getSuggestAuthorBySuffix($suffix){
        $query = "SELECT DISTINCT author_id ".
                 "FROM ". DATABASE_PREFIX ."repository_external_author_id_suffix ".
                 "WHERE suffix LIKE ? ".
                 "AND prefix_id > 0 ".
                 "AND is_delete = 0;";
        $params = array();
        $params[] = $suffix."%";    // suffix
        // Execution SELECT
        $author_id = $this->executeSql($query, $params);
        
        return $author_id;
    }
    
    // Mod WEKO-350 著者名典拠管理 2017/01/23 Y.Nakao --start--
    /**
     * 著者メタデータを登録する。同定はしない。
     *
     * @param array $nameAuthority Author information. 著者メタデータ
     * @param array $externalAuthorId external author data. 外部著者IDデータ
     * @return int Author ID newly registered. 新規登録した著者ID
     */
    public function entryNameAuthorityNotIdentify($nameAuthority, $externalAuthorId)
    {
        // 新しい著者IDを発番
        $authorId = $this->getNewAuthorId();
        
        foreach ($nameAuthority as $authority)
        {
            // 「姓」は必須入力
            if(strlen($authority["family"]) == 0) continue;
            // 著者名典拠を登録
            $params = array(
                    "author_id" => $authorId,
                    "language" => $authority["language"],
                    "family" => $authority["family"],
                    "name" => $authority["name"],
                    "family_ruby" => $authority["family_ruby"],
                    "name_ruby" => $authority["name_ruby"],
                    "ins_user_id" => $this->user_id,
                    "mod_user_id" => $this->user_id,
                    "del_user_id" => 0,
                    "ins_date" => $this->accessDate,
                    "mod_date" => $this->accessDate,
                    "is_delete" => 0
            );
            $result = $this->insNameAuthority($params);
            if($result === false)
            {
                // エラーログを出力する
                $this->errorLog($this->Db->ErrorMsg(), __FILE__, __CLASS__, __LINE__);
                throw new AppException("Failed insert NameAuthority : ".__CLASS__."::".__METHOD__);
            }
        }
        
        // 外部著者IDを新規登録する。
        $this->upsertExternalAuthorId($externalAuthorId, $authorId);
        
        return $authorId;
    }
    
    /**
     * Delete NameAuthority.
     * 著者名典拠を削除する。
     * 
     * @param int $author_id deleted nameathority ID. 削除する著者名典拠ID
     */
    public function deleteNameAuthority($author_id)
    {
        $params = array();
        $params[] = 1;
        $params[] = $this->user_id;
        $params[] = $this->user_id;
        $params[] = $this->accessDate;
        $params[] = $this->accessDate;
        $params[] = $author_id;
        
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Nameauthority/deleteNameAuthority_NameAuthority.sql", $params);
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Nameauthority/deleteNameAuthority_ExternalAuthorIdSuffix.sql", $params);
    }
    
    /**
     * Revive authorized delegated authority.
     * 削除済の著者名典拠を復活する。
     * 
     * @param int $author_id Author ID to cancel deletion.削除をとりけす著者ID
     */
    public function restoreNameAuthority($author_id)
    {
        // 著者名典拠の削除を取り消す。
        $params = array();
        $params[] = 0;
        $params[] = "";
        $params[] = "";
        $params[] = $this->user_id;
        $params[] = $this->accessDate;
        $params[] = $author_id;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Nameauthority/restoreNameAuthority_RestoreNameAuthority.sql", $params);
        
        // 3.1で取得した削除者、日時と一致する外部著者情報のみ復活する。
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Nameauthority/restoreNameAuthority_RestoreExternamAuthorIdSuffix.sql", $params);
    }
    
    /**
     * Check whether the specified author ID exists in the authority (it is not deleted).
     * 指定された著者IDが典拠に存在する(削除されていない)か確認する。
     * 
     * @param int $author_id Author id.著者ID
     */
    public function existsAuthor($author_id)
    {
        $params = array();
        $params[] = $author_id;
        $params[] = 0;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Nameauthority/existsAuthor.sql", $params);
        
        // 言語に応じてレコードが存在するので、削除されていないレコードが1件以上あればOK。
        if(isset($result[0]["cnt"]) && $result[0]["cnt"] > 0)
        {
            return true;
        }
        
        return false;
    }
    
    /**
     * Make sure the author of the specified language exists in the authority (not deleted).
     * 指定された言語の著者が典拠に存在する(削除されていない)か確認する。
     * 
     * @param int $author_id Author id.著者ID
     * @param String $language 言語
     */
    public function existsAuthorLanguage($author_id, $language)
    {
        $params = array();
        $params[] = $author_id;
        $params[] = $language;
        $params[] = 0;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Nameauthority/existsAuthorLanguage.sql", $params);
        // 削除されていないレコードが1件あればOK。
        if(isset($result[0]["cnt"]) && $result[0]["cnt"] == 1)
        {
            return true;
        }
        return false;
    }
    
    /**
     * Author name authority data update
     * 著者名典拠データ更新
     * 
     * @param int $author_id Author ID to update. 更新する著者ID
     * @param String $language language.言語 / japanese, english ,""
     * @param String $family surname. 姓
     * @param String $name name. 名
     * @param String $family_ruby surname(kana). 姓(ヨミ)
     * @param String $name_ruby name(kana). 名(ヨミ)
     */
    public function editNameAuthority($author_id, $language, $family, $name, $family_ruby="", $name_ruby="")
    {
        if(strlen($family)==0 && strlen($name)==0 && strlen($family_ruby)==0 && strlen($name_ruby)==0)
        {
            // 入力がすべて空＝削除する
            $this->deleteNameAuthorityRecord($author_id, $language);
        }
        else
        {
            // 著者データ更新。
            $this->upsertNameAuthority($author_id, $language, $family, $name, $family_ruby, $name_ruby);
        }
    }
    
    /**
     * Delete the specified author ID and author authority information of the language.
     * 指定された著者IDと言語の著者典拠情報を削除する。
     *
     * @param int $author_id Author ID to update. 更新する著者ID
     * @param String $language language.言語 / japanese, english ,""
     */
    private function deleteNameAuthorityRecord($author_id, $language)
    {
        $params = array();
        $params[] = $author_id;
        $params[] = $language;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Nameauthority/deleteNameAuthorityRecord.sql", $params);
    }
    
    /**
     * Author name authority data update
     * 著者名典拠データ更新
     *
     * @param int $author_id Author ID to update. 更新する著者ID
     * @param String $language language.言語 / japanese, english ,""
     * @param String $family surname. 姓
     * @param String $name name. 名
     * @param String $family_ruby surname(kana). 姓(ヨミ)
     * @param String $name_ruby name(kana). 名(ヨミ)
     */
    private function upsertNameAuthority($author_id, $language, $family, $name, $family_ruby="", $name_ruby="")
    {
        // データがなければ新規登録。データが有れば更新。削除されていれば復活。
        $params = array();
        $params[] = $author_id;
        $params[] = $language;
        $params[] = $family;
        $params[] = $name;
        $params[] = $family_ruby;
        $params[] = $name_ruby;
        $params[] = $this->user_id;
        $params[] = $this->user_id;
        $params[] = "";
        $params[] = $this->accessDate;
        $params[] = $this->accessDate;
        $params[] = "";
        $params[] = 0;
        $params[] = $family;
        $params[] = $name;
        $params[] = $family_ruby;
        $params[] = $name_ruby;
        $params[] = $this->user_id;
        $params[] = $this->user_id;
        $params[] = $this->accessDate;
        $params[] = $this->accessDate;
        $params[] = 0;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Nameauthority/upsertNameAuthority.sql", $params);
    }
    
    /**
     * Delete the external author ID data of the designated author
     * 指定された著者の外部著者IDデータを削除する
     *
     * @param int $author_id Author ID to update. 削除対象の著者ID
     */
    public function deleteAllExternalAuthorSuffix($author_id)
    {
        $params = array();
        $params[] = $author_id;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Nameauthority/deleteAllExternalAuthorSuffix.sql", $params);
    }
    
    
    /**
     * Author's name Authority's email address update
     * 著者名典拠のメールアドレス更新
     *
     * @param int $author_id Author ID to update. 更新する著者ID
     * @param int $prefix_id external author id. 外部著者ID
     * @param String $suffix Value for external author ID. 外部著者IDに対する値
     */
    public function editExternalAuthorSuffix($author_id, $prefix_id, $suffix)
    {
        if(strlen($suffix)>0)
        {
            // メールアドレス、外部著者IDを更新。
            $this->upsertExternalAuthorSuffix($author_id, $prefix_id, $suffix);
        }
    }
    
    /**
     * Update external author ID
     * 外部著者IDの更新
     * 
     * @param int $author_id Author ID to update. 更新する著者ID
     * @param int $prefix_id external author id. 外部著者ID
     * @param String $suffix Value for external author ID. 外部著者IDに対する値
     */
    private function upsertExternalAuthorSuffix($author_id, $prefix_id, $suffix)
    {
        // データがなければ新規登録。データが有れば更新。削除されていれば復活。
        $params = array();
        $params[] = $author_id;
        $params[] = $prefix_id;
        $params[] = $suffix;
        $params[] = $this->user_id;
        $params[] = $this->user_id;
        $params[] = "";
        $params[] = $this->accessDate;
        $params[] = $this->accessDate;
        $params[] = "";
        $params[] = 0;
        $params[] = $suffix;
        $params[] = $this->user_id;
        $params[] = "";
        $params[] = $this->accessDate;
        $params[] = "";
        $params[] = 0;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Nameauthority/upsertExternalAuthorSuffix.sql", $params);
    }
    
    /**
     * Change the author name authority of the item.
     * アイテムの著者名典拠を付け替える。
     *
     * @param int[] $item_id_list Item ID to be replaced. 付け替え対象のアイテムID
     * @param int $before_author_id Author name authority ID before integration. 統合前のWEKO著者ID
     * @param int $after_author_idAuthor name authority ID of integration destination. 統合先のWEKO著者ID
     */
    public function changeNameAuthority($item_id_list, $before_author_id, $after_author_id)
    {
        // 指定されたアイテムに対して氏名テーブルのWEKO著者IDを変更する。
        $busPersonalName = BusinessFactory::getFactory()->getBusiness('businessItemMetadataPersonalName');
        $busPersonalName->replaceAuthorIdWithAuthorId($item_id_list, $before_author_id, $after_author_id);
        
        // 指定されたアイテムに対して、フィードバックメール送信者IDテーブルのWEKO著者IDを変更する。
        $busFeedbackMail = BusinessFactory::getFactory()->getBusiness('businessSendfeedbackmail');
        $busFeedbackMail->replaceAuthorIdWithAuthorId($item_id_list, $before_author_id, $after_author_id);
    }
    
    /**
     * Convert "& EMPTY &" to null character. Also, only double-byte / byte spaces are converted to null characters.
     * 「&EMPTY&」を空文字に変換する。また、全角/半角スペースのみを空文字に変換する。
     *
     * @param String $str 変換した文字列
     */
    public function emptyFilter($str)
    {
        $str = preg_replace("/^(".RepositoryConst::BLANK_WORD."| |　)+$/", "", $str);
        return $str;
    }
    
    /**
     * Gets the value of the specified external author ID.
     * 指定した外部著者IDの値を取得する。
     * 
     * @param int $author_id WEKO Author ID. WEKO著者ID
     * @param int $prefix_id External author ID. 外部著者ID.
     * @return string suffix. 外部著者IDの値
     */
    public function externalAuthorIdSuffix($author_id, $prefix_id)
    {
        $params = array();
        $params[] = $author_id;
        $params[] = $prefix_id;
        $params[] = 0;
        $result = $this->executeSqlFile(dirname(__FILE__)."/sql/Nameauthority/externalAuthorIdSuffix.sql", $params);
        if(!isset($result[0]['suffix']))
        {
            return "";
        }
        return $result[0]['suffix'];
    }
    
    /**
     * If there are targets to be excluded from the identification rule of the author name authority, it is processed.
     * In addition, when making an identification by "isDiffExternalAuthorId" method, execute this method before calling.
     * 著者名典拠の同定ルールから除外する対象があれば処理する。
     * なお「isDiffExternalAuthorId」メソッドによる同定判定をする場合は呼び出す前に本メソッドを実行すること。
     * 
     * @param array $extAuthorIdArray External author ID prefix and suffix 外部著者ID prefixおよびsuffix
     *                                array[$ii]["prefix_id"|"suffix"|"old_prefix_id"|"old_suffix"|"prefix_name"]
     * @return array External author ID prefix and suffix excluding information unnecessary for identification determination.同定判定に不要な情報を除外した外部著者ID prefixおよびsuffix。
     */
    private function exclusionIdentifyAuthoId($extAuthorIdArray)
    {
        // 除外ルールを適用する。
        $tmpExternalAuthorIds = array();
        for($ii=0; $ii<count($extAuthorIdArray); $ii++)
        {
            // &EMPTY&および空文字の場合は同定ルールから除外する
            $extAuthorIdArray[$ii]['suffix'] = $this->emptyFilter($extAuthorIdArray[$ii]['suffix']);
            
            // 有効値をセット
            if(strlen($extAuthorIdArray[$ii]['suffix']) > 0)
            {
                array_push($tmpExternalAuthorIds, $extAuthorIdArray[$ii]);
            }
        }
        return $tmpExternalAuthorIds;
    }
    
    /**
     * It judges whether there is an author ID that holds all designated external authors.
     * 指定された外部著者をすべて持つ著者IDがあるか判定する。
     * 
     * @param array $extAuthorIdArray External author ID prefix and suffix 外部著者ID prefixおよびsuffix
     *                                array[$ii]["prefix_id"|"suffix"|"old_prefix_id"|"old_suffix"|"prefix_name"]
     * @param array $exclusionAuthorIds Author ID to exclude from identification. (Example: You exclude yourself etc). 同定から除外する著者ID。(例：自分自身は除外する等)
     * @return bool true =>There is an author ID that has all the specified external authors. 指定された外部著者をすべて持つ著者IDが存在する。
     *              false=>There is no author ID that has all the specified external authors. 指定された外部著者をすべて持つ著者IDが存在しない。
     */
    public function isIdentificationExternalAuthorId($extAuthorIdArray, $exclusionAuthorIds=array())
    {
        // 同定の除外ルールを適用
        $extAuthorIdArray = $this->exclusionIdentifyAuthoId($extAuthorIdArray);
        
        // 同定する外部著者の情報がないため存在なし。
        if(count($extAuthorIdArray)==0) return false;
        
        // 外部著者ID群に部分一致する著者IDの一覧を取得する
        $authorIds = $this->selectAuthorIdList($extAuthorIdArray);
        
        for($ii = 0; $ii < count($authorIds); $ii++)
        {
            // 除外対象の著者IDは無視する
            if(count($exclusionAuthorIds) > 0 && is_numeric(array_search($authorIds[$ii]["author_id"], $exclusionAuthorIds))) continue;
            
            // 入力された外部著者IDとデータベースに保存されている著者IDに紐付く外部著者IDで差異があるかを調べる
            if(!$this->isDiffExternalAuthorId($extAuthorIdArray, $authorIds[$ii]["author_id"]))
            {
                // 指定された外部著者をすべて持つ著者IDが存在する。
                return true;
            }
        }
        return false;
    }
    
    // Mod WEKO-350 著者名典拠管理 2017/01/23 Y.Nakao --end--
}
?>