<?php

/**
 * Search name authority business class
 * 著者名典拠検索ビジネスクラス
 *
 * @package     WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Searchnameauthority.class.php 76465 2017-02-21 09:50:24Z keiya_sugimoto $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Business logic abstract class
 * ビジネスロジック基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/FW/BusinessBase.class.php';

/**
 * Search name authority business class
 * 著者名典拠検索ビジネスクラス
 * 
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Components_Business_Nameauthority_Searchnameauthority extends BusinessBase
{
    /**
     * Search name authority from author info
     * 著者情報から著者名典拠を検索する
     *
     * @param int $author_id WEKO author ID WEKO著者ID
     * @param string $family Family name 姓
     * @param string $name Given name 名
     * @param string $mail Mail address メールアドレス
     * @param int $prefix_id External author ID prefix ID 外部著者ID prefix ID
     * @param string $suffix External author ID 外部著者ID
     * @param int $is_delete Delete flag 削除フラグ
     *
     * @return array WEKO author ID list WEKO著者IDリスト
     *               array[$ii]
     */
    public function searchNameAuthorityFromAuthorInfo($author_id, $family, $name, $mail, $prefix_id, $suffix, $is_delete) {
        $nameAuthorityTableList = $this->searchNameAuthorityFromNameAuthorityTable($author_id, $family, $name, $is_delete);
        if(isset($mail) && strlen($mail) > 0)
        {
            $mailAddressList = $this->searchNameAuthorityFromMailAddress($mail, $is_delete);
        }
        if(isset($prefix_id) && is_numeric($prefix_id) && isset($suffix) && strlen($suffix) > 0)
        {
            $externalAuthorIdList = $this->searchNameAuthorityFromExternalAuthorId($prefix_id, $suffix, $is_delete);
        }
        
        if(isset($nameAuthorityTableList) && isset($mailAddressList) && isset($externalAuthorIdList))
        {
            $merge = array_intersect($nameAuthorityTableList, $mailAddressList, $externalAuthorIdList);
        }
        else if(isset($nameAuthorityTableList) && isset($mailAddressList))
        {
            $merge = array_intersect($nameAuthorityTableList, $mailAddressList);
        }
        else if(isset($nameAuthorityTableList) && isset($externalAuthorIdList))
        {
            $merge = array_intersect($nameAuthorityTableList, $externalAuthorIdList);
        }
        else if(isset($mailAddressList) && isset($externalAuthorIdList))
        {
            $merge = array_intersect($mailAddressList, $externalAuthorIdList);
        }
        else if(isset($nameAuthorityTableList))
        {
            $merge = $nameAuthorityTableList;
        }
        else if(isset($mailAddressList))
        {
            $merge = $mailAddressList;
        }
        else if(isset($externalAuthorIdList))
        {
            $merge = $externalAuthorIdList;
        }
        else
        {
            $merge = array();
        }
        $authorIdList = array_unique($merge);
        sort($authorIdList, SORT_NUMERIC);
        return $authorIdList;
    }
    
    /**
     * Search name authority from name authority table
     * 著者名典拠を著者名典拠テーブルから検索する
     *
     * @param int $author_id WEKO author ID WEKO著者ID
     * @param string $family Family name 姓
     * @param string $name Given name 名
     * @param int $is_delete Delete flag 削除フラグ
     *
     * @return array WEKO author ID list WEKO著者IDリスト
     *               array[$ii]
     */
    private function searchNameAuthorityFromNameAuthorityTable($author_id, $family, $name, $is_delete) {
        $authorIdList = array();
        
        $query = "SELECT author_id ". 
                 "FROM {repository_name_authority} ". 
                 "WHERE ";
        $where_column = "";
        $params = array();
        if(isset($author_id) && is_numeric($author_id) && $author_id > 0)
        {
            if(strlen($where_column) > 0)
            {
                $where_column .= "AND ";
            }
            $where_column .= "author_id = ? ";
            $params[] = intval($author_id);
        }
        if(isset($family) && strlen($family) > 0)
        {
            if(strlen($where_column) > 0)
            {
                $where_column .= "AND ";
            }
            $where_column .= "(family LIKE ? OR family_ruby LIKE ?) ";
            $params[] = "%".$family."%";
            $params[] = "%".$family."%";
        }
        if(isset($name) && strlen($name) > 0)
        {
            if(strlen($where_column) > 0)
            {
                $where_column .= "AND ";
            }
            $where_column .= "(name LIKE ? OR name_ruby LIKE ?) ";
            $params[] = "%".$name."%";
            $params[] = "%".$name."%";
        }
        if(strlen($where_column) > 0)
        {
            $where_column .= "AND ";
        }
        $where_column .= "is_delete = ?;";
        $params[] = intval($is_delete);
        $result = $this->executeSql($query.$where_column, $params);
        
        for($ii = 0; $ii < count($result); $ii++)
        {
            $authorIdList[] = $result[$ii]["author_id"];
        }
        return $authorIdList;
    }
    
    /**
     * Search name authority from mail address
     * 著者名典拠をメールアドレスから検索する
     *
     * @param string $mail Mail address メールアドレス
     * @param int $is_delete Delete flag 削除フラグ
     *
     * @return array WEKO author ID list WEKO著者IDリスト
     *               array[$ii]
     */
    private function searchNameAuthorityFromMailAddress($mail, $is_delete) {
        $authorIdList = array();
        
        $params= array();
        $params[] = 0;
        $params[] = "%".$mail."%";
        $params[] = intval($is_delete);
        $result = $this->executeSqlFile(WEBAPP_DIR."/modules/repository/components/business/nameauthority/sql/Searchnameauthority/external_author_id_suffix_select_getAuthorId.sql", $params);
        
        for($ii = 0; $ii < count($result); $ii++)
        {
            $authorIdList[] = $result[$ii]["author_id"];
        }
        
        return $authorIdList;
    }
    
    /**
     * Search name authority from external author ID
     * 著者名典拠を外部著者IDから検索する
     *
     * @param int $prefix_id External author ID prefix ID 外部著者ID prefix ID
     * @param string $suffix External author ID 外部著者ID
     * @param int $is_delete Delete flag 削除フラグ
     *
     * @return array WEKO author ID list WEKO著者IDリスト
     *               array[$ii]
     */
    private function searchNameAuthorityFromExternalAuthorId($prefix_id, $suffix, $is_delete) {
        $authorIdList = array();
        
        $params= array();
        $params[] = intval($prefix_id);
        $params[] = "%".$suffix."%";
        $params[] = intval($is_delete);
        $result = $this->executeSqlFile(WEBAPP_DIR."/modules/repository/components/business/nameauthority/sql/Searchnameauthority/external_author_id_suffix_select_getAuthorId.sql", $params);
        
        for($ii = 0; $ii < count($result); $ii++)
        {
            $authorIdList[] = $result[$ii]["author_id"];
        }
        
        return $authorIdList;
    }
}
?>