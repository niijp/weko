<?php

/**
 * Mapping List Reqquired DOI class
 * DOIに必要なメタデータリストクラス
 *
 * @package     WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Mappinglistrequireddoi.class.php 75663 2017-01-25 06:17:45Z keiya_sugimoto $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics,
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/**
 * Mapping List Reqquired DOI class
 * DOIに必要なマッピングリストクラス
 * 
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class MappingListRequiredDoi
{
    /**
     * Mapping list for journal article
     * ジャーナルアーティクル用のマッピングリスト
     *
     * @var array array[$ii]
     */
    public $mappingListJournalArticle = array( "fullTextURL",
                                               "title",
                                               "publisher",
                                               "jtitle",
                                               "issn",
                                               "spage",
                                               "language" );
    
    /**
     * Mapping list for thesis or dissertation
     * 学位論文用のマッピングリスト
     *
     * @var array array[$ii]
     */
    public $mappingListThesisOrDissertation = array( "fullTextURL",
                                                     "title" );
    
    /**
     * Mapping list for book
     * 書籍用のマッピングリスト
     *
     * @var array array[$ii]
     */
    public $mappingListBook = array( "fullTextURL",
                                     "title",
                                     "publisher",
                                     "isbn",
                                     "language" );
    
    /**
     * Mapping list for research data
     * 研究データ用のマッピングリスト
     *
     * @var array array[$ii]
     */
    public $mappingListResearchData = array( "fullTextURL",
                                             "title",
                                             "publisher",
                                             "creator",
                                             "language" );
    
    /**
     * Mapping list for journal article JaLC DOI
     * JaLC DOI版のジャーナルアーティクル用のマッピングリスト
     *
     * @var array array["fullTextURL"|"title"|"publisher"|...]["required"|"not_multiple"|"english"]
     */
    public $mappingListJournalArticleForJalc = array( "fullTextURL" => array( "required" => 1, 
                                                                              "not_multiple" => 0,
                                                                              "english" => 0 ),
                                                      "title" => array( "required" => 1, 
                                                                        "not_multiple" => 0,
                                                                        "english" => 0 ),
                                                      "publisher" => array( "required" => 0, 
                                                                            "not_multiple" => 0,
                                                                            "english" => 0 ),
                                                      "jtitle" => array( "required" => 0, 
                                                                         "not_multiple" => 0,
                                                                         "english" => 0 ),
                                                      "issn" => array( "required" => 0, 
                                                                       "not_multiple" => 0,
                                                                       "english" => 0 ),
                                                      "spage" => array( "required" => 1, 
                                                                        "not_multiple" => 0,
                                                                        "english" => 0 ),
                                                      "language" => array( "required" => 0, 
                                                                           "not_multiple" => 0,
                                                                           "english" => 0 ) );
    
    /**
     * Mapping list for journal article CrossRef DOI
     * CrossRef DOI版のジャーナルアーティクル用のマッピングリスト
     *
     * @var array array["fullTextURL"|"title"|"publisher"|...]["required"|"not_multiple"|"english"]
     */
    public $mappingListJournalArticleForCrossref = array( "fullTextURL" => array( "required" => 1, 
                                                                                  "not_multiple" => 0,
                                                                                  "english" => 0 ),
                                                          "title" => array( "required" => 1, 
                                                                            "not_multiple" => 0,
                                                                            "english" => 1 ),
                                                          "publisher" => array( "required" => 1, 
                                                                                "not_multiple" => 1,
                                                                                "english" => 1 ),
                                                          "jtitle" => array( "required" => 1, 
                                                                             "not_multiple" => 1,
                                                                             "english" => 1 ),
                                                          "issn" => array( "required" => 1, 
                                                                           "not_multiple" => 1,
                                                                           "english" => 0 ),
                                                          "spage" => array( "required" => 1, 
                                                                            "not_multiple" => 0,
                                                                            "english" => 0 ),
                                                          "language" => array( "required" => 1, 
                                                                               "not_multiple" => 1,
                                                                               "english" => 1 ) );
    
    /**
     * Mapping list for thesis or dissertation JaLC DOI
     * JaLC DOI版の学位論文用のマッピングリスト
     *
     * @var array array["fullTextURL"|"title"]["required"|"not_multiple"|"english"]
     */
    public $mappingListThesisOrDissertationForJalc = array( "fullTextURL" => array( "required" => 1, 
                                                                                    "not_multiple" => 0,
                                                                                    "english" => 0 ),
                                                            "title" => array( "required" => 1, 
                                                                              "not_multiple" => 0,
                                                                              "english" => 0 ) );
    
    /**
     * Mapping list for book JaLC DOI
     * JaLC DOI版の書籍用のマッピングリスト
     *
     * @var array array["fullTextURL"|"title"|"publisher"|...]["required"|"not_multiple"|"english"]
     */
    public $mappingListBookForJalc = array( "fullTextURL" => array( "required" => 1, 
                                                                    "not_multiple" => 0,
                                                                    "english" => 0 ),
                                            "title" => array( "required" => 1, 
                                                              "not_multiple" => 0,
                                                              "english" => 0 ),
                                            "publisher" => array( "required" => 0, 
                                                                  "not_multiple" => 0,
                                                                  "english" => 0 ),
                                            "isbn" => array( "required" => 0, 
                                                             "not_multiple" => 0,
                                                             "english" => 0 ),
                                            "language" => array( "required" => 0, 
                                                                 "not_multiple" => 0,
                                                                 "english" => 0 ) );
    
    /**
     * Mapping list for book CrossRef DOI
     * CrossRef DOI版の書籍用のマッピングリスト
     *
     * @var array array["fullTextURL"|"title"|"publisher"|...]["required"|"not_multiple"|"english"]
     */
    public $mappingListBookForCrossref = array( "fullTextURL" => array( "required" => 1, 
                                                                        "not_multiple" => 0,
                                                                        "english" => 0 ),
                                                "title" => array( "required" => 1, 
                                                                  "not_multiple" => 0,
                                                                  "english" => 1 ),
                                                "publisher" => array( "required" => 1, 
                                                                      "not_multiple" => 1,
                                                                      "english" => 1 ),
                                                "isbn" => array( "required" => 1, 
                                                                 "not_multiple" => 1,
                                                                 "english" => 0 ),
                                                "language" => array( "required" => 1, 
                                                                     "not_multiple" => 1,
                                                                     "english" => 1 ) );
    
    /**
     * Mapping list for research data JaLC DOI
     * JaLC DOI版の研究データ用のマッピングリスト
     *
     * @var array array["fullTextURL"|"title"|"publisher"|...]["required"|"not_multiple"|"english"]
     */
    public $mappingListResearchDataForJalc = array( "fullTextURL" => array( "required" => 1, 
                                                                            "not_multiple" => 0,
                                                                            "english" => 0 ),
                                                    "title" => array( "required" => 1, 
                                                                      "not_multiple" => 0,
                                                                      "english" => 0 ),
                                                    "publisher" => array( "required" => 1, 
                                                                          "not_multiple" => 1,
                                                                          "english" => 0 ),
                                                    "creator" => array( "required" => 1, 
                                                                        "not_multiple" => 0,
                                                                        "english" => 0 ),
                                                    "language" => array( "required" => 0, 
                                                                         "not_multiple" => 0,
                                                                         "english" => 0 ) );
    
    /**
     * Mapping list for research data DataCite DOI
     * DataCite DOI版の研究データ用のマッピングリスト
     *
     * @var array array["fullTextURL"|"title"|"publisher"|...]["required"|"not_multiple"|"english"]
     */
    public $mappingListResearchDataForDataCite = array( "fullTextURL" => array( "required" => 1, 
                                                                                "not_multiple" => 0,
                                                                                "english" => 0 ),
                                                        "title" => array( "required" => 1, 
                                                                          "not_multiple" => 0,
                                                                          "english" => 1 ),
                                                        "publisher" => array( "required" => 1, 
                                                                              "not_multiple" => 1,
                                                                              "english" => 1 ),
                                                        "creator" => array( "required" => 1, 
                                                                            "not_multiple" => 0,
                                                                            "english" => 1 ),
                                                        "language" => array( "required" => 1, 
                                                                             "not_multiple" => 1,
                                                                             "english" => 1 ) );
}
?>