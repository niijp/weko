UPDATE %%DATABASE_PREFIX%%repository_item_attr_type 
 SET `lom_mapping` = 'generalDescription' 
 WHERE `dublin_core_mapping` = 'description' 
 AND `item_type_id` >= 20001 
 AND `item_type_id` <= 20016;