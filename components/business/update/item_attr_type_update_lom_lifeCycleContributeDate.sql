UPDATE %%DATABASE_PREFIX%%repository_item_attr_type 
 SET `lom_mapping` = 'lifeCycleContributeDate' 
 WHERE `lom_mapping` = 'lifeCycleContributePublishDate' 
 AND `item_type_id` >= 20001 
 AND `item_type_id` <= 20016;