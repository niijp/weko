UPDATE %%DATABASE_PREFIX%%repository_item_attr_type 
 SET `lom_mapping` = 'lifeCycleContributeRolePublisher' 
 WHERE `lom_mapping` = 'lifeCycleContributePublisher' 
 AND `item_type_id` >= 20001 
 AND `item_type_id` <= 20016;