UPDATE %%DATABASE_PREFIX%%repository_item_attr_type 
 SET `lom_mapping` = 'lifeCycleContributeRoleAuthor' 
 WHERE `lom_mapping` = 'lifeCycleContributeAuthor' 
 AND `item_type_id` >= 20001 
 AND `item_type_id` <= 20016;