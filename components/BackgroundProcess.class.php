<?php

/**
 * Base class for carrying out asynchronously and recursively possibility is the ability to process a long period of time
 * 長時間処理する可能性がある機能を非同期かつ再帰的に実施するための基底クラス
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: BackgroundProcess.class.php 85773 2019-03-22 11:01:37Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Action base class for the WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryAction.class.php';

/**
 * Asynchronous processing run common classes
 * 非同期処理実行共通クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryProcessUtility.class.php';

/**
 * Base class for carrying out asynchronously and recursively possibility is the ability to process a long period of time
 * 長時間処理する可能性がある機能を非同期かつ再帰的に実施するための基底クラス
 * 
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class BackgroundProcess extends RepositoryAction
{
    /**
     * Process name
     * プロセス名
     *
     * @var string
     */
    private $process_name = null;
    
    /**
     * Asynchronous processing end flag
     * 非同期処理終了フラグ
     *
     * @var boolean
     */
    private $isFinish = false;
    
    /**
     * Random key (negative integer, 9 digits) for determining the execution authority
     * 実行権限有無を判断するためのランダムキー(負の整数, 9桁)
     * 
     * @var int
     */
    public $request_id = null;
    
    /**
     * Valid time of request_id (seconds). Default 30 seconds
     * request_idの有効時間(秒)。デフォルト30秒
     * 
     * @var int
     */
    const TIME_OUT_SEC = 30;
    
    /**
     * Constructer(To set the process name)
     * コンストラクタ(プロセス名を設定する)
     *
     * @param string paramter Process name プロセス名
     */
    protected function __construct($parameter)
    {
        $this->process_name = $parameter;
    }
    
    /**
     * Override function used when determining execution authority other than request_id.
     * request_id以外で実行権限を判定する場合に使用するオーバーライド関数
     * 
     * @return false
     */
    protected function checkAuthority() { return false; }
    
    /**
     * Data to be processed is read, and executes the processing
     * 処理対象のデータを読み込み、処理を実行する
     */
    protected function executeApp()
    {
        $this->exitFlag = true;
        
        // check authority
        if($this->checkAuthority() === true){
            if(!isset($this->request_id)){
                // authority = OK and Not set request_id, then set request_id.
                $this->request_id = RepositoryProcessUtility::genelateBackgroundProcessRequestId();
                $_GET["request_id"] = $this->request_id;
                if($this->startBackgroundProcessLockTable($this->process_name, $this->request_id) === false) {
                    // error. エラー終了
                    $this->isFinish = true;
                    return;
                }
            }
        } else if($this->validateRequestId() === false){
            // request_id is wrong, then error termination.
            $this->errorLog("Miss BackgroundProcess request_id.", __FILE__, __CLASS__, __LINE__);
            $this->isFinish = true;
            return;
        }
        
        // check process
        $status = $this->lockProcess();

        // init background process
        if($status === false){
            $this->errorLog("Can't lock BackgroundProcess.", __FILE__, __CLASS__, __LINE__);
            $this->isFinish = true;
            return;
        }
        
        // get target 
        $executeFlag = $this->prepareBackgroundProcess($target);
        
        if($executeFlag == false){
            $this->completeProcess();
            $this->isFinish = true;
            return;
        }
        
        // execute Background Process
        $this->executeBackgroundProcess($target);
        
        // execute next process
        $this->unlockProcess();
    }
    
    /**
     * Transaction outside the post-processing (calling the following processing)
     * トランザクション外後処理(次の処理を呼び出す)
     */
    final protected function afterTrans()
    {
        if(!$this->isFinish)
        {
            $this->callAsyncProcess();
        }
    }
    
    /**
     * As the same asynchronous processing is not multiple execution, leaving the effect that running the database
     * 同じ非同期処理が多重実行されないよう、データベースに実行中である旨を残す
     * 
     * @return bool 成否
     */
    private function lockProcess()
    {
        // update process status
        $query = "UPDATE ".DATABASE_PREFIX."repository_lock ".
                 "SET status = ? ".
                 "WHERE process_name = ? ".
                 "AND status = ? ".
                 "AND LENGTH(comment) > ? ".
                 "AND TIME_TO_SEC(TIMEDIFF(DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s.000'), comment)) < ?;";
        $params = array();
        $params[] = 1;
        $params[] = $this->process_name;
        $params[] = $this->request_id;
        $params[] = 0;
        $params[] = self::TIME_OUT_SEC;
        $retRef = $this->dbAccess->executeQuery($query, $params);
        if($retRef === false){
            return false;
        }
        $count = $this->dbAccess->affectedRows();
        if($count == 0){
            return false;
        }
        return true;
    }
    
    /**
     * Read the data to be processed
     * 処理対象のデータを読み込む
     * 
     * @param $target Data to be processed 処理対象のデータ
     */
    protected function prepareBackgroundProcess(&$target)
    {
        // for override
        return true;
    }
    
    /**
     * To perform the time-consuming process
     * 時間のかかる処理を実行する
     * 
     * @param $target Data to be processed 処理対象のデータ
     */
    protected function executeBackgroundProcess($target)
    {
        // for override
    }
    
    /**
     * To perform an action to asynchronous
     * アクションを非同期に実行する
     */
    private function callAsyncProcess()
    {
        // Request parameter for next URL
        $nextRequest = BASE_URL;
        $count = 0;
        foreach($_GET as $key => $value){
            if($count == 0){
                $nextRequest .= "/?";
            } else {
                $nextRequest .= "&";
            }
            $nextRequest .= $key."=".$value;
            $count++;
        }
        $result = RepositoryProcessUtility::callAsyncProcess($nextRequest);
        return $result;
    }
    
    /**
     * Set the flag for multiple execution prevention to OFF, and set the access date and time in request_id and comment in status.
     * 多重実行防止用のフラグをOFFにし、statusにrequest_id、commentに現在日時(access_dateだと処理時間が長い場合タイムアウト時間を超えるので現在時刻)を設定する
     */
    private function unlockProcess()
    {
        // update process status
        $query = "UPDATE ".DATABASE_PREFIX."repository_lock ".
                 "SET status = ?, comment = DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s.000') ".
                 "WHERE process_name = ?; ";
        $params = array();
        $params[] = $this->request_id;
        $params[] = $this->process_name;
        $retRef = $this->dbAccess->executeQuery($query, $params);
        return;
    }
    
    /**
     * Loop execution complete termination processing
     * ループ実行の完全終了処理
     */
    private function completeProcess()
    {
        // update process status
        $query = "UPDATE ".DATABASE_PREFIX."repository_lock ".
                "SET status = ?, comment = ? ".
                "WHERE process_name = ?; ";
        $params = array();
        $params[] = 0;
        $params[] = "";
        $params[] = $this->process_name;
        $retRef = $this->dbAccess->executeQuery($query, $params);
        return;
    }
    
    /**
     * Check if request_id is a 9-digit negative integer
     * request_idが9桁の負の整数であるか確認
     * 
     * @return boolean 成否
     */
    private function validateRequestId()
    {
        $this->request_id = intval($this->request_id);
        if(-1000000000 < $this->request_id && $this->request_id < -99999999) return true;
        return false;
    }
    
    /**
      * Set execution start of BackgroundProcess.
      * BackgroundProcessの実行開始設定を行う
      *
      * @param string $process_name Name of background process to start execution. 実行開始するバックグラウンド処理の名称
      * @param int $request_id Key figure that determines the execution authority of background processing. バックグラウンド処理の実行権限を判定するキー数字
      * @return boolean Execution start status. 実行開始有無
      */
     private function startBackgroundProcessLockTable($process_name, $request_id) {
         $query = "UPDATE {repository_lock} ".
                 " SET status = ?, comment = DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s.000') ".
                 " WHERE process_name = ? ".
                 " AND status = ?";
         $params = array();
         $params[] = $request_id;
         $params[] = $process_name;
         $params[] = 0;
         $result = $this->dbAccess->executeQuery($query, $params);
         $count = $this->dbAccess->affectedRows();
         if($count == 0){
             return false;
         }
         $_GET["request_id"] = $this->request_id;
         return true;
     }
}

?>
