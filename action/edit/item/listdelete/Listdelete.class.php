<?php

/**
 * Action class for items bulk deletion
 * アイテム一括削除用アクションクラス
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Listdelete.class.php 84367 2018-12-15 12:35:56Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/**
 * Action base class for the WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryAction.class.php';

/**
 * Action class for items bulk deletion
 * アイテム一括削除用アクションクラス
 * 
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class Repository_Action_Edit_Item_Listdelete extends RepositoryAction
{
    // *********************
    // リクエストパラメータ
    // *********************
    /**
     * check exists doi flag
     * DOI存在確認フラグ
     *
     * @var int
     */
    public $doi_check_flg = null;
    /**
     * Select index id
     * 選択インデックスID
     *
     * @var int
     */
    public $targetIndexId = null;
    // サブインデックス以下削除フラグ
    /**
     * Sub-index following the deletion flag
     * サブインデックス以下削除フラグ
     *
     * @var boolean
     */
    public $isDeleteSubIndexItem = null;
    
    /**
     * Item bulk deletion
     * アイテム一括削除
     *
     * @return string Result 結果
     */
    function executeApp()
    {
        // 選択タブの保存
        $this->Session->setParameter("item_setting_active_tab", 2);
    
        // 引数チェック //
        if( $this->targetIndexId == null || $this->targetIndexId == "" ){
            $this->Session->setParameter("error_msg", "Select Index Error.");
            return 'error';
        }
        
        // DOIアイテムの検索
        if($this->doi_check_flg == "1") {
            $this->exitFlag = true;
            
            $checkDoi = BusinessFactory::getFactory()->getBusiness("businessCheckdoi");
            $ret = $checkDoi->checkDoiItemInIndex($this->targetIndexId, $this->isDeleteSubIndexItem, false);
            $message = ($ret === true) ? "exist" : "not";
            
            echo $message;
            return;
        }
        
        //セッションクリア
        $this->Session->removeParameter("targetIndexId");
        $this->Session->removeParameter("searchIndexId");
        $this->Session->removeParameter("isDeleteSubIndexItem");
    
        $this->Session->setParameter("targetIndexId", $this->targetIndexId);
        $this->Session->setParameter("searchIndexId", $this->targetIndexId);
        
        // 削除実行
        $this->deleteItemInIndex();
        
        // エラーメッセージ開放
        $this->Session->removeParameter("error_msg");
    
        return 'success';
    }
    
    /**
     * Delete Item in index
     * インデックス下のアイテムを削除実行する
     */
    private function deleteItemInIndex() {
        $indexManager = BusinessFactory::getFactory()->getBusiness("businessIndexmanager");
        $itemDelete = BusinessFactory::getFactory()->getBusiness("businessItemdelete");
        $checkDoi = BusinessFactory::getFactory()->getBusiness("businessCheckdoi");
        
        $index_info = array();
        if($this->isDeleteSubIndexItem == 0) {
            $this->Session->setParameter("isDeleteSubIndexItem", false);
            //自身のインデックスIDのみ格納する
            array_push($index_info,$this->targetIndexId);
        } else {
            $this->Session->setParameter("isDeleteSubIndexItem", true);
            // 自身のインデックスIDと配下のインデックスIDを格納する
            $index_info = $indexManager->getIndexIdRecursive($this->targetIndexId);
        }
        
        // インデックス配下のアイテム一覧取得
        $query = "SELECT item_id, item_no FROM {repository_position_index} ".
                 "WHERE index_id IN (" .implode(",", array_fill(0, count($index_info), "?")). ") " .
                 "AND is_delete = ? ".
                 "GROUP BY item_id ;";
        $params = array();
        $params = array_merge($params, $index_info);
        $params[] = 0;
        $itemIds = $this->executeSql($query, $params);
        
        // アイテム毎に処理する
        for($ii = 0; $ii < count($itemIds); $ii++) {
            // アイテムにDOIが付与されている場合は削除しない
            $doiCovered = $checkDoi->getDoiStatus($itemIds[$ii]["item_id"], $itemIds[$ii]["item_no"]);
            if($doiCovered == 1) { continue; }
            
            // アイテムが他インデックス以外に所属している場合はアイテム削除は行わず、所属情報のみ削除する
            $posOtherIndex = $indexManager->searchItemPosInOtherIndex($itemIds[$ii]["item_id"], $itemIds[$ii]["item_no"], $index_info);
            if(count($posOtherIndex) > 0) {
                // インデックス所属情報削除実行
                $query = "UPDATE {repository_position_index} ".
                         "SET is_delete = ?, ".
                         "del_user_id = ?, ".
                         "del_date = ? ".
                         "WHERE item_id = ? ".
                         "AND item_no = ? ".
                         "AND index_id IN (".implode(",", array_fill(0, count($index_info), "?")).") ".
                         "AND is_delete = ? ;";
                $params = array();
                $params[] = 1;
                $params[] = $this->Session->getParameter("_user_id"); // del_user_id
                $params[] = $this->TransStartDate; // del_date
                $params[] = $itemIds[$ii]["item_id"];
                $params[] = $itemIds[$ii]["item_no"];
                $params = array_merge($params, $index_info);
                $params[] = 0;
                $this->executeSql($query, $params);
                
                continue;
            }
            
            // アイテム削除を実行する。紐付くデータの更新やインデックスコンテンツ数再計算も行われる
            $itemDelete->deleteItem($itemIds[$ii]["item_id"], $itemIds[$ii]["item_no"], $this->Session, $this->repository_admin_base, $this->repository_admin_room);
        }
        
        // 指定インデックスのコンテンツ数を親まで再帰的に再計算する
        $recountIndexArray = array();
        for($ii = 0; $ii < count($index_info); $ii++) { $recountIndexArray[$ii]["index_id"] = $index_info[$ii]; }
        $indexManager->recountIndexContentsNum($recountIndexArray);
    }
}
?>
