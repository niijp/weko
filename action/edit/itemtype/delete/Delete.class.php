<?php

/**
 * Action class for deletion item type
 * アイテムタイプ削除用アクションクラス
 *
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Delete.class.php 80692 2017-08-08 03:03:43Z keiya_sugimoto $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics,
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Action base class for the WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryAction.class.php';
/**
 * Search table manager class
 * 検索テーブル管理クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositorySearchTableProcessing.class.php';
/**
 * Item type manager class
 * アイテムタイプ管理クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/ItemtypeManager.class.php';

/**
 * Action class for deletion item type
 * アイテムタイプ削除用アクションクラス
 *
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class Repository_Action_Edit_Itemtype_Delete extends RepositoryAction
{
	
	// リクエストパラメタ
	/**
	 * Item type ID
	 * アイテムタイプID
	 *
	 * @var int
	 */
	var $item_type_id = null;
	
	/**
	 * Check undeleted item flag
	 * 未削除アイテムの検索実施フラグ
	 *
	 * @var int
	 */
	var $check_undeleted_item_flg = null;
	
	/**
	 * Execute
	 * 実行
	 *
	 * @return string "success"/"error" success/failed 成功/失敗
	 * @throws AppException
	 */
	function executeApp()
	{
		
		// アイテムタイプ検索を行う　2017/01/23 K.Miura --start--
		if($this->check_undeleted_item_flg == "1") {
			$this->exitFlag = true;
			
			// アイテムタイプを使用している未削除のアイテム件数
			$total = 0;
			
			// 検索実行
            $SearchItem = BusinessFactory::getFactory()->getBusiness('businessSearchItem');
			// アイテム件数を取得
            $total = $SearchItem->countItemMatchingItemtype($this->item_type_id);
			
			// アイテム件数を標準出力して処理を抜ける
			echo $total;
			
			return;
		}
		// アイテムタイプ検索を行う　2017/01/23 K.Miura --end--
		
		
		//////////////////// Lock for update ///////////////////////
		$query = "SELECT mod_date ".
			"FROM ". DATABASE_PREFIX ."repository_item_type ".
			"WHERE item_type_id = ? AND ".
			"is_delete = ? ".
			"FOR UPDATE;";
		$params = null;
		$params[] = $this->item_type_id;			// item_type_id
		$params[] = 0;
		$result = $this->executeSql($query, $params);
		
		// count = 0 is no update recorde
		if(count($result)==0) {
			// TODO: 「既にアイテムタイプが削除されています。」を言語リソースに定義してエラーメッセージとする。
			$errMsg = "";
			throw new AppException($errMsg);
		}
		// Delete item type
		$query = "UPDATE ". DATABASE_PREFIX ."repository_item_type ".
			"SET mod_user_id = ?, ".
			"mod_date = ?, ".
			"del_user_id = ?, ".
			"del_date = ?, ".
			"is_delete = ? ".
			"WHERE item_type_id = ?; ";
		$params = null;
		$params[] = $this->Session->getParameter("_user_id");   // mod_user_id
		$params[] = $this->TransStartDate;				// mod_date
		$params[] = $this->Session->getParameter("_user_id");   // del_user_id
		$params[] = $this->TransStartDate;				// del_date
		$params[] = 1;									// is_delete
		$params[] = $this->item_type_id;						// item_type_id
		//Run update
		$result = $this->executeSql($query, $params);
		
		// Add detail search 2013/11/25 K.Matsuo --start--
		$searchTableProcessing = new RepositorySearchTableProcessing($this->Session, $this->Db);
		$searchTableProcessing->deleteDataFromSearchTable();
		// Add detail search 2013/11/25 K.Matsuo --end--
		
		// Add itemtype authority 2014/12/17 T.Ichikawa --start--
		$itemtypeManager = new Repository_Components_ItemtypeManager($this->Session, $this->Db, $this->TransStartDate);
		$itemtypeManager->removeExclusiveItemtypeAuthority($this->item_type_id);
		// Add itemtype authority 2014/12/17 T.Ichikawa --end--
		
		return 'success';
	}
}
?>
