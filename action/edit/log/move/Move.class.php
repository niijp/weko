<?php

/**
 * Log deletion action class
 * ログ削除アクションクラス
 *
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Move.class.php 80692 2017-08-08 03:03:43Z keiya_sugimoto $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics,
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
 * Action base class for the WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryAction.class.php';
/**
 * Fixed logging action class
 * 定型ログ作成アクションクラス
 */
require_once WEBAPP_DIR. '/modules/repository/logreport/Logreport.class.php';
/**
 * Custom report
 * カスタムレポート作成クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/business/Customreport.class.php';

/**
 * Log deletion action class
 * ログ削除アクションクラス
 *
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class Repository_Action_Edit_Log_Move extends RepositoryAction
{
	// component
    /**
     * Language Resource Management object
     * 言語リソース管理オブジェクト
     *
     * @var Smarty
     */
	var $smartyAssign = null;
	
	// request parameter
	/**
	 * Delete start year
	 * 削除開始年
	 *
	 * @var int
	 */
	var $start_year = null;
    /**
     * Delete starting month
     * 削除開始月
     *
     * @var int
     */
	var $start_month = null;
	/**
	 * Delete starting day
	 * 削除開始日
	 *
	 * @var int
	 */
	var $start_day = null;
    /**
     * Delete End Year
     * 削除終了年
     *
     * @var int
     */
	var $end_year = null;
    /**
     * Delete end month
     * 削除終了月
     *
     * @var int
     */
	var $end_month = null;
	/**
	 * Delete end day
	 * 削除終了日
	 *
	 * @var int
	 */
	var $end_day = null;
    /**
     * Delete period
     * 削除期間
     *
     * @var int
     */
	var $log_term = null;
	
	// login info
	/**
	 * User id
	 * ユーザID
	 *
	 * @var string
	 */
	var $user_id = null;
    /**
     * Administrator login ID
     * 管理者ログインID
     *
     * @var string
     */
	var $login_id = null;
    /**
     * Administrator password
     * 管理者パスワード
     *
     * @var string
     */
	var $password = null;
    /**
     * User of the base level of authority
     * ユーザのベース権限レベル
     *
     * @var string
     */
	var $user_authority_id = null;
    /**
     * User of room privilege level
     * ユーザのルーム権限レベル
     *
     * @var string
     */
	var $authority_id = null;
	
	// member
	/**
	 * Delete log exclusion subquery
	 * 削除ログ排除サブクエリ
	 *
	 * @var string
	 */
	var $log_exception = "";
	
	/**
	 * To delete the log
	 * ログを削除する
	 */
	function executeApp()
	{
		ini_set('memory_limit', -1);
		
		$this->exitFlag = true;
		// -----------------------------------------------
		// get lang resource
		// -----------------------------------------------
		$this->setLangResource();
		$this->smartyAssign = $this->Session->getParameter("smartyAssign");
		
		// -------------------------------------------
		// check login
		// -------------------------------------------
		$user_id = $this->Session->getParameter("_user_id");
		if($user_id != "0"){
			// check auth
			$this->user_authority_id = $this->Session->getParameter("_user_auth_id");
			$this->authority_id = $this->Session->getParameter("_auth_id");
		} else if($this->login_id != null && strlen($this->login_id) > 0 &&
			$this->password != null && strlen($this->password) > 0){
			$result = $this->checkLogin($this->login_id, $this->password, $Result_List, $error_msg);
			if($result === false){
				echo $this->smartyAssign->getLang("repository_log_move_error")."\n".
			 		  $this->smartyAssign->getLang("repository_log_move_error_login");
				return;
			}
		} else {
			echo $this->smartyAssign->getLang("repository_log_move_error")."\n".
				  $this->smartyAssign->getLang("repository_log_move_error_login");
			return;
		}
		
		// -------------------------------------------
		// Check request parameter, auto fill date
		//   start date <- first month
		//   end date <- first month + period or last month
		// -------------------------------------------
		if(strlen($this->start_year) == 0 && strlen($this->start_month) == 0){
			// not setting start date, auto fill first month
			$query = "SELECT DATE_FORMAT( MIN( record_date ) , '%Y' ) AS sy, ".
					 " DATE_FORMAT( MIN( record_date ) , '%m' ) AS sm ".
					 " FROM ".DATABASE_PREFIX."repository_log ";
			$result = $this->Db->execute($query);
			if($result === false || count($result) != 1){
				echo $this->smartyAssign->getLang("repository_log_move_error")."\n".
					  $this->smartyAssign->getLang("repository_log_move_error_date");
				return;
			}
			$this->start_year = $result[0]['sy'];
			$this->start_month = $result[0]['sm'];
		}
		if(strlen($this->end_year) == 0 && strlen($this->end_month) == 0){
			// not setting end date
			if(strlen($this->log_term) == 0 || !is_numeric($this->log_term)){
				// not setting term, auto fill last month
				$query = "SELECT DATE_FORMAT( DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y') AS ey, ".
						 "DATE_FORMAT( DATE_SUB(NOW(), INTERVAL 1 MONTH), '%m') AS em;";
				$result = $this->Db->execute($query);
				if($result === false || count($result) != 1){
					echo $this->smartyAssign->getLang("repository_log_move_error")."\n".
						  $this->smartyAssign->getLang("repository_log_move_error_date");
					return;
				}
				$this->end_year = $result[0]['ey'];
				$this->end_month = $result[0]['em'];
			} else {
				// setting term, fill month
				$query = "SELECT DATE_FORMAT( DATE_ADD('".$this->start_year."-".$this->start_month."-1', INTERVAL ".($this->log_term-1)." MONTH), '%Y') AS ey, ".
					"DATE_FORMAT( DATE_ADD('".$this->start_year."-".$this->start_month."-1', INTERVAL ".($this->log_term-1)." MONTH), '%m') AS em;";
				$result = $this->Db->execute($query);
				if($result === false || count($result) != 1){
					echo $this->smartyAssign->getLang("repository_log_move_error")."\n".
						$this->smartyAssign->getLang("repository_log_move_error_date");
					return;
				}
				$this->end_year = $result[0]['ey'];
				$this->end_month = $result[0]['em'];
			}
		}
		
		// -------------------------------------------
		// Check at period
		// -------------------------------------------
		// 0 burials
		$this->start_year = sprintf("%04d", $this->start_year);
		$this->start_month = sprintf("%02d", $this->start_month);
		$this->end_year = sprintf("%04d", $this->end_year);
		$this->end_month = sprintf("%02d", $this->end_month);
		// end date older than start date
		$start_date = $this->start_year.$this->start_month;
		$end_date = $this->end_year.$this->end_month;
		if(intval($start_date) > intval($end_date)){
			// when start date older than end date, error
			echo $this->smartyAssign->getLang("repository_log_move_error")."\n".
				  $this->smartyAssign->getLang("repository_log_move_error_date");
			return;
		}
		// end date now month is error
		//$today = getdate();
		$query = "SELECT DATE_FORMAT(NOW(), '%Y') AS tmp_y, ".
				 "DATE_FORMAT(NOW(), '%m') AS tmp_m;";
		$result = $this->Db->execute($query);
		if($result === false || count($result) != 1){
			echo $this->smartyAssign->getLang("repository_log_move_error")."\n".
				  $this->smartyAssign->getLang("repository_log_move_error_date");
			return;
		}
		if(	$this->end_year == sprintf("%04d", $result[0]['tmp_y']) &&
			$this->end_month == sprintf("%02d", $result[0]['tmp_m'])){
			// when start date older than end date, error
			echo $this->smartyAssign->getLang("repository_log_move_error")."\n".
				  $this->smartyAssign->getLang("repository_log_move_error_date");
			return;
		}
		// Get start day
		$query = "SELECT SUBSTRING(MIN(CAST( record_date AS DATE )),9,2) AS start_date ".
			" FROM ".DATABASE_PREFIX."repository_log ";
		$result = $this->Db->execute($query);
		if($result === false || count($result) != 1){
			return false;
		}
		$this->start_day = $result[0]['start_date'];
		// Get end day
		$query = "SELECT DATE_FORMAT(LAST_DAY('".$this->end_year."-".$this->end_month."-1'), '%d') AS lastday;";
		$result = $this->Db->execute($query);
		if($result === false || count($result) != 1){
			return false;
		}
		$this->end_day = $result[0]['lastday'];
		
		
		// -------------------------------------------
		// dump log data
		// -------------------------------------------
		$result = $this->dumpLogData();
		if($result === false){
			echo $this->smartyAssign->getLang("repository_log_move_error")."\n".
				  $this->smartyAssign->getLang("repository_log_move_error_dump");
			return;
		}
		
		// -------------------------------------------
		// make log report
		// -------------------------------------------
		$result = $this->makeLogReport();
		if($result === false){
			echo $this->smartyAssign->getLang("repository_log_move_error")."\n".
				  $this->smartyAssign->getLang("repository_log_move_error_report");
			return;
		}
		
		// -------------------------------------------
		// Aggregate usage statistics
		// -------------------------------------------
		$this->infoLog("businessUsagestatistics", __FILE__, __CLASS__, __LINE__);
		$usageStatistics = BusinessFactory::getFactory()->getBusiness("businessUsagestatistics");
		if(!$usageStatistics->aggregateUsagestatistics())
		{
			echo $this->smartyAssign->getLang("repository_log_move_error")."\n".
				  $this->smartyAssign->getLang("repository_log_move_error_usagestatistics");
			return;
		}
		
		// -------------------------------------------
		// Aggregate site license usage statistics
		// -------------------------------------------
		$this->aggregateSitelicenseUsagestatistics();
		
		// -------------------------------------------
		// move log data
		// -------------------------------------------
		$result = $this->moveLogData();
		if($result === false){
			echo $this->smartyAssign->getLang("repository_log_move_error")."\n".
				  $this->smartyAssign->getLang("repository_log_move_error_move");
			return;
		}
		
		// -------------------------------------------
		// delete log data
		// -------------------------------------------
		$result = $this->deleteLogData();
		if($result === false){
			echo $this->smartyAssign->getLang("repository_log_move_error")."\n".
				  $this->smartyAssign->getLang("repository_log_move_error_delete");
			return;
		}
		
		// -------------------------------------------
		// end
		// -------------------------------------------
		
		echo $this->smartyAssign->getLang("repository_log_move_success");
		
		return;
	}
	
	/**
	 * Move log data
	 * between start date to end date
	 * ログを退避する
	 *
	 * @return boolean Retult 結果
	 */
	function moveLogData(){
		// ------------------------------------------
		// make move folder
		// ------------------------------------------
		if(!file_exists(WEBAPP_DIR."/logs/weko/logfile")){
			mkdir(WEBAPP_DIR."/logs/weko/logfile");
		}
		chmod ( WEBAPP_DIR."/logs/weko/logfile", 0300 );
		
		// ------------------------------------------
		// Set custom report business
		// ------------------------------------------
		$this->infoLog("businessCustomreport", __FILE__, __CLASS__, __LINE__);
		$customReport = BusinessFactory::getFactory()->getBusiness("businessCustomreport");
		$customReport->setStartYear($this->start_year);
		$customReport->setStartMonth($this->start_month);
		$customReport->setStartDay($this->start_day);
		$customReport->setEndYear($this->end_year);
		$customReport->setEndMonth($this->end_month);
		$customReport->setEndDay($this->end_day);
		
		// ------------------------------------------
		// fill per date
		// ------------------------------------------
		$this->aggregateLogPerDate($customReport);
			
		// ------------------------------------------
		// fill per host
		// ------------------------------------------
		$this->aggregateLogPerHost($customReport);
			
		// ------------------------------------------
		// fill per item
		// ------------------------------------------
		$this->aggregateLogPerItem($customReport);
		
		return true;
	}
	
	/**
	 * Make log report
	 * save to webapp/logs/weko/logreport/logReport_YYYYMM.zip
	 * 定型ログ作成
	 * 
	 * @return bool Retult 結果
	 * @throws AppException
	 */
	function makeLogReport(){
		$block_id = $this->getBlockPageId();
        $this->infoLog("businessLogreport", __FILE__, __CLASS__, __LINE__);
        $logreport = BusinessFactory::getFactory()->getBusiness("businessLogreport");
        $logreport->setAdminBase($this->repository_admin_base);
        $logreport->setAdminRoom($this->repository_admin_room);
        $repositoryLogreport = new Repository_Logreport();
		
		$start_date = $this->start_year."-".$this->start_month."-01";
		$end_date = $this->end_year."-".$this->end_month."-01";
		$query = "SELECT DATEDIFF('".$end_date."', '".$start_date."') AS date_diff;";
		$result = $this->Db->execute($query);
		if($result === false){
            $this->errorLog($this->Db->ErrorMsg(), __FILE__, __CLASS__, __LINE__);
            throw new AppException($this->Db->ErrorMsg());
		}
        
        if(count($result) != 1) {
            return false;
        }
		$diff = $result[0]['date_diff'];
		$tmp_date = $start_date;
        
		while( $diff >= 0){
			$query = "SELECT DATE_FORMAT('".$tmp_date."', '%Y') AS tmp_y, ".
					 "DATE_FORMAT('".$tmp_date."', '%m') AS tmp_m;";
			$result = $this->Db->execute($query);
		    if($result === false){
                $this->errorLog($this->Db->ErrorMsg(), __FILE__, __CLASS__, __LINE__);
                throw new AppException($this->Db->ErrorMsg());
    		}
            
            if(count($result) != 1) {
                return false;
            }
			$tmp_year = $result[0]['tmp_y'];
			$tmp_month = $result[0]['tmp_m'];
			$report_file = WEBAPP_DIR."/logs/weko/logreport/logReport_".$tmp_year.$tmp_month.".zip";
			
			// -----------------------------------------------
			// make log report
			//   copy source from repository/view/edit/logreport.class.php
			// -----------------------------------------------
			// -----------------------------------------------
			// init
			// -----------------------------------------------
			// set start date
            $logreport->setStartYear($tmp_year);
            $logreport->setStartMonth($tmp_month);
            // set end date
            $logreport->setEndYear($tmp_year);
            $logreport->setEndMonth($tmp_month);
            // set parameter
            $repositoryLogreport->Session = $this->Session;
            $repositoryLogreport->Db = $this->Db;
            $repositoryLogreport->sy_log = $tmp_year;
            $repositoryLogreport->sm_log = $tmp_month;
            $repositoryLogreport->start_date = sprintf("%d-%02d-%02d",$repositoryLogreport->sy_log, $repositoryLogreport->sm_log, $repositoryLogreport->sd_log);
            $repositoryLogreport->disp_start_date = sprintf("%d-%02d",$repositoryLogreport->sy_log, $repositoryLogreport->sm_log);
            $repositoryLogreport->ey_log = $repositoryLogreport->sy_log;
            $repositoryLogreport->em_log = $repositoryLogreport->sm_log;
            $repositoryLogreport->end_date = sprintf("%d-%02d-%02d",$repositoryLogreport->ey_log, $repositoryLogreport->em_log,$repositoryLogreport->ed_log);
            $repositoryLogreport->disp_end_date = sprintf("%d-%02d",$repositoryLogreport->ey_log, $repositoryLogreport->em_log);
            $repositoryLogreport->setupLanguageResourceForOtherAction();
            $repositoryLogreport->setupGroupList();
            $repositoryLogreport->Logger = WekoBusinessFactory::getFactory()->logger;
            
			// add for compress files
			$output_files = array();
            $this->infoLog("businessWorkdirectory", __FILE__, __CLASS__, __LINE__);
            $businessWorkdirectory = BusinessFactory::getFactory()->getBusiness('businessWorkdirectory');
            $tmp_dir = $businessWorkdirectory->create();
            $tmp_dir = substr($tmp_dir, 0, -1);
			
			// -----------------------------------------------
			// make
			// -----------------------------------------------
            // Get data
			$logreport->execute();
            // Make file
            $output_files = $repositoryLogreport->createLogReport($tmp_dir);
            
			// -----------------------------------------------
			// compress zip file
			// -----------------------------------------------
			// set zip file name
			$zip_file = "logReport_".
						str_replace("-", "", sprintf("%d-%02d",$tmp_year, $tmp_month)).".zip";
			// compress zip file	
			File_Archive::extract(
				$output_files,
				File_Archive::toArchive($zip_file, File_Archive::toFiles( $tmp_dir."/" ))
			);
			// copy zip file
			copy($tmp_dir."/".$zip_file, $report_file);
			// delete tmp folder
			$this->removeDirectory($tmp_dir);
			if(!file_exists($report_file)){
				return false;
			}
			
			//$cnt_month++;
			$query = "SELECT DATE_ADD('".$tmp_date."', INTERVAL 1 MONTH) AS next_date;";
			$result = $this->Db->execute($query);
			if($result === false || count($result) != 1){
				return false;
			}
			$tmp_date = $result[0]['next_date'];
			$query = "SELECT DATEDIFF('".$end_date."', '".$tmp_date."') AS date_diff;";
			$result = $this->Db->execute($query);
			if($result === false || count($result) != 1){
				return false;
			}
			$diff = $result[0]['date_diff'];
		}
		return true;
	}
	
	/**
	 * Aggregate site license usagestatistics
	 * サイトライセンス利用統計集計
	 */
	private function aggregateSitelicenseUsagestatistics() {
		$this->infoLog("businessAggregatesitelicenseusagestatistics", __FILE__, __CLASS__, __LINE__);
		$siteLicenseUsageStatistics = BusinessFactory::getFactory()->getBusiness("businessAggregatesitelicenseusagestatistics");
		
		// 指定の年月範囲から集計対象月の一覧を作成する
		$dates = array();
		$sy = strtotime($this->start_year."-".$this->start_month."-01");
		$ey = strtotime($this->end_year."-".$this->end_month."-01");
		$startDate = date("Y-m", $sy);
		$endDate = date("Y-m", $ey);
		if($startDate > $endDate) { throw new AppException("Date range is wrong."); }
		
		$dateIndex = 0;
		while($startDate <= $endDate) {
			$tmpDate = explode("-", $startDate);
			$dates[$dateIndex] = array();
			$dates[$dateIndex]["year"] = $tmpDate[0];
			$dates[$dateIndex]["month"] = $tmpDate[1];
			
			$startDate = date("Y-m", strtotime($startDate."-01 +1 month"));
			$dateIndex++;
		}
		
		// サイトライセンス機関の一覧を取得する
		$query = "SELECT organization_id FROM {repository_sitelicense_info} WHERE is_delete = ?;";
		$params = array();
		$params[] = 0;
		$ret = $this->executeSql($query, $params);
		
		// 指定のサイトライセンス機関に対して日付範囲分の集計を行う
		for($ii = 0; $ii < count($ret); $ii++) {
			for($jj = 0; $jj < count($dates); $jj++) {
				$siteLicenseUsageStatistics->aggregateUsagestatistics($ret[$ii]["organization_id"], $dates[$jj]["year"], $dates[$jj]["month"]);
			}
		}
	}
	
	/**
	 * Dump log data for SQL or CSV
	 * between start date to end date
	 * ログテーブルダンプ作成
	 *
	 * @return bool Retult 結果
	 */
	function dumpLogData(){
		// check log folder exist
		if(!file_exists(WEBAPP_DIR ."/logs/weko/logdump" )){
			mkdir(WEBAPP_DIR ."/logs/weko/logdump");
		}
		// setting permission(d-wx------)
		chmod(WEBAPP_DIR ."/logs/weko/logdump", 0300);
		
		// dump file path
		//$path_dumpfile = WEBAPP_DIR ."/logs/weko/logdump/".date("Y-m-d_His");
		$query = "SELECT DATE_FORMAT(NOW(), '%Y-%m-%d_%H%i%s') AS now_date;";
		$result = $this->Db->execute($query);
		if($result === false || count($result) != 1){
			return false;
		}
		$path_dumpfile = WEBAPP_DIR ."/logs/weko/logdump/".$result[0]['now_date'];
		$path_dumpfile = str_replace('\\', '\\\\', $path_dumpfile);
		
		// check envpath for mysqldump
		$path_mysqldump = "";
		$path_env = getenv('PATH');
		$path = split(PATH_SEPARATOR, $path_env);
		for($ii=0; $ii<count($path); $ii++){
			if(	file_exists($path[$ii].DIRECTORY_SEPARATOR."mysqldump") || 
				file_exists($path[$ii].DIRECTORY_SEPARATOR."mysqldump.exe")){
				$path_mysqldump = $path[$ii];
				break;
			}
		}
		if(strlen($path_mysqldump) > 0){
			// Setting mysqldump PATH environment variable
			// try mysqldump
			$arrayList = explode("/",substr(stristr(DATABASE_DSN, "/"), 2));
			// Get of database name
			$dbname = $arrayList[1];
			$arrayList = explode(":", $arrayList[0]);
			// Get of mysql user name
			$mysqlUser = $arrayList[0];
			// Get of mysql password
			$mysqlPass = substr($arrayList[1], 0, strpos($arrayList[1], "@"));					
			//$logName = date(Ymd);
			$query = "SELECT DATE_FORMAT(NOW(), '%Y%m%d') AS now_date;";
			$result = $this->Db->execute($query);
			if($result === false || count($result) != 1){
				return false;
			}
			$logName = $result[0]['now_date'];
			$language = null;
			if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				$language = "sjis";
			} else {
				$language = "utf8";
			}
			$cmd = $path_mysqldump.DIRECTORY_SEPARATOR."mysqldump ".
						" --default-character-set=".$language." ".
						" --user=".$mysqlUser." ".
						" --password=".$mysqlPass." ".
					$dbname." ".DATABASE_PREFIX ."repository_log ". 
					" > ".
					$path_dumpfile.".sql";
			exec($cmd);
		} else {
			// Don't setting mysqldump PATH environment variable
			// Change the mode of the weko folder(d-wx----wx)
			chmod(WEBAPP_DIR ."/logs/weko/", 0303);
			$start_date = $this->start_year."-".$this->start_month."-01 00:00:00.000";
			$end_date = $this->end_year."-".$this->end_month."-31 23:59:59.999";
			$query = "SELECT * FROM ".DATABASE_PREFIX."repository_log ".
					" WHERE record_date >= '$start_date' ".
					" AND record_date <= '$end_date' ".
					" INTO OUTFILE '".$path_dumpfile.".csv' ".
					" FIELDS TERMINATED BY ',' ".
					" ENCLOSED BY '\'' LINES TERMINATED BY '\\r\\n'";
			$result = $this->Db->execute($query);
			if($result === false){
				return false;
			}
			// Change the mode of the weko folder(d-wx------)
			chmod(WEBAPP_DIR ."/logs/weko/", 0300);
		}
		
		$path_dumpfile = str_replace('\\\\', '\\', $path_dumpfile);
		if(!file_exists($path_dumpfile.".sql") && !file_exists($path_dumpfile.".csv")){
			return false;
		}
		return true;
	}
	
	/**
	 * Delete log data
	 * between start date to end date
	 * ログ削除
	 *
	 * @return bool Retult 結果
	 */
	function deleteLogData(){
		$start_date = $this->start_year."-".$this->start_month."-01 00:00:00.000";
		$end_date = $this->end_year."-".$this->end_month."-31 59:59:59.999";
		$query = "DELETE FROM ".DATABASE_PREFIX."repository_log ".
				" WHERE record_date >= '$start_date' ".
				" AND record_date <= '$end_date' ";
		$result = $this->Db->execute($query);
		if($result === false){
			return false;
		}
		
		// OPTIMIZE
		$query = "OPTIMIZE TABLE ".DATABASE_PREFIX."repository_log ";
		$result = $this->Db->execute($query);
		if($result === false){
			return false;
		}
	}
	
	/**
	 * write custom report accesss data(log per date)
	 * 日単位のカスタムレポートを作成
	 *
	 * @param array $log_per_date The number of accesses per month and year 年月ごとのアクセス数
	 *                            array[YYYYMM]["record_date"|"year_week"|"item_count"|"download_count"|"view_count"]
	 */
	private function writeLogPerPerDate($log_per_date){
		$year = "";
		foreach ($log_per_date as $date => $val){
			// 年が変わった時にファイルを分ける
			if(substr($date, 0, 4) != $year){
				if (strlen($year) > 0){
					fclose($fp);
				}
				$year = substr($date, 0, 4);
				$fp = fopen(WEBAPP_DIR."/logs/weko/logfile/log_per_date_$year.txt", "a");
			}
			
			$itemCnt = isset($val["item_count"]) ? $val["item_count"] : 0;
			$downloadCnt = isset($val["download_count"]) ? $val["download_count"] : 0;
			$viewCnt = isset($val["view_count"]) ? $val["view_count"] : 0;
			
			fwrite($fp, $val['record_date']."\t".
					  $val['year_week']."\t".
					  $itemCnt."\t".
					  $downloadCnt."\t".
					  $viewCnt."\n"
			);
		}
		fclose($fp);
	}
	
    /**
     * write custom report accesss data(log per host)
     * ホスト単位のカスタムレポートを作成
     *
     * @param array $log_per_host The number of accesses per month and year 年月ごとのアクセス数
     *                            array[YYYYMM][ipAddoress]["record_date"|"ip_address"|"host"|"item_count"|"download_count"|"view_count"]
     */
    private function writeLogPerHost($log_per_host){
        $month = "";
        
        foreach ($log_per_host as $date => $list){
            if(substr($date, 0, 4).substr($date, 5, 2) != $month){
                if (strlen($month) > 0){
                    fclose($fp);
                }
                $month = substr($date, 0, 4).substr($date, 5, 2);
                $fp = fopen(WEBAPP_DIR."/logs/weko/logfile/log_per_host_".$month.".txt", "a"); 
            }
            foreach ($list as $ip => $val){
                fwrite($fp, $val['record_date']."\t".
                            $val['ip_address']."\t".
                            $val['host']."\t".
                            $val['item_count']."\t".
                            $val['download_count']."\t".
                            $val['view_count']."\n"
                );
            }
        }
        
        if(isset($fp)){
            fclose($fp);
        }
    }
    
    /**
     * Write custom report access data(log per item)
     * アイテム毎のカスタムレポートを作成
     *
     * @param array $log_per_item The number of accesses per month and year 年月ごとのアクセス数
     *                            array[YYYYMM][ipAddoress]["record_date"|"item_id"|"download_count"|"download_count"|"view_count"]
     */
    private function writeLogPerItem($log_per_item){
        $month = "";
        
        foreach ($log_per_item as $date => $list){
            if(substr($date, 0, 4).substr($date, 5, 2) != $month){
                if (strlen($month) > 0){
                    fclose($fp);
                }
                $month = substr($date, 0, 4).substr($date, 5, 2);
                $fp = fopen(WEBAPP_DIR."/logs/weko/logfile/log_per_item_$month.txt", "a");
            }
            foreach ($list as $item_id => $item){
                foreach ($item as $item_no => $val){
                    fwrite($fp, $val['record_date']."\t".
                                $val['item_id']."-".$val['item_no']."\t".
                                $val['download_count']."\t".
                                $val['view_count']."\n"
                    );
                }
            }
        }
        if(isset($fp)){
            fclose($fp);
        }
    }
	
	/**
	 * Aggregate log per date
	 * 日毎にログを集計する
	 *
	 * @param $customReport Repository_Components_Business_Customreport Custom report object カスタムレポート処理オブジェクト
	 */
    private function aggregateLogPerDate($customReport) {
		$customReport->setCountPer(Repository_Components_Business_Customreport::PER_LOG_DAY);
		$log_per_date = array();
	
		// アイテム登録数
		$customReport->setCountType(Repository_Components_Business_Customreport::TYPE_LOG_REGIST_ITEM);
		$customReport->execute();
		$log_per_date_items = $customReport->getCustomReportData();
		for($ii = 0; $ii < count($log_per_date_items); $ii++) {
			$date = $log_per_date_items[$ii]["day"];
			// 配列初期化
			if(!isset($log_per_date[$date])){
				// 年の何週目か
				$result = $this->Db->execute("SELECT YEARWEEK('".$date."') AS d2 ");
				$year_week = ($result !== false) ?  $result[0]['d2'] : "";
				
				$log_per_date[$date] = array();
				$log_per_date[$date]["record_date"] = $date;
				$log_per_date[$date]["year_week"] = $year_week;
				$log_per_date[$date]["item_count"] = 0;
				$log_per_date[$date]["download_count"] = 0;
				$log_per_date[$date]["view_count"] = 0;
			}
			// アイテム登録数
			$log_per_date[$date]["item_count"] = isset($log_per_date_items[$ii]["cnt"]) ? $log_per_date_items[$ii]["cnt"] : 0;
		}
		unset($log_per_date_items);
	
		// ファイルダウンロード回数
		$customReport->setCountType(Repository_Components_Business_Customreport::TYPE_LOG_DOWNLOAD);
		$customReport->execute();
		$log_per_date_downloads = $customReport->getCustomReportData();
		for($ii = 0; $ii < count($log_per_date_downloads); $ii++) {
			$date = $log_per_date_downloads[$ii]["day"];
			// 配列初期化
			if(!isset($log_per_date[$date])){
				// 年の何週目か
				$result = $this->Db->execute("SELECT YEARWEEK('".$date."') AS d2 ");
				$year_week = ($result !== false) ?  $result[0]['d2'] : "";
				
				$log_per_date[$date] = array();
				$log_per_date[$date]["record_date"] = $date;
				$log_per_date[$date]["year_week"] = $year_week;
				$log_per_date[$date]["item_count"] = 0;
				$log_per_date[$date]["download_count"] = 0;
				$log_per_date[$date]["view_count"] = 0;
			}
			// ファイルダウンロード数
			$log_per_date[$date]["download_count"] = isset($log_per_date_downloads[$ii]["cnt"]) ? $log_per_date_downloads[$ii]["cnt"] : 0;
		}
		unset($log_per_date_downloads);
	
		// アイテム閲覧回数
		$customReport->setCountType(Repository_Components_Business_Customreport::TYPE_LOG_VIEW);
		$customReport->execute();
		$log_per_date_views = $customReport->getCustomReportData();
		for($ii = 0; $ii < count($log_per_date_views); $ii++) {
			$date = $log_per_date_views[$ii]["day"];
			// 配列初期化
			if(!isset($log_per_date[$date])){
				// 年の何週目か
				$result = $this->Db->execute("SELECT YEARWEEK('".$date."') AS d2 ");
				$year_week = ($result !== false) ?  $result[0]['d2'] : "";
				
				$log_per_date[$date] = array();
				$log_per_date[$date]["record_date"] = $date;
				$log_per_date[$date]["year_week"] = $year_week;
				$log_per_date[$date]["item_count"] = 0;
				$log_per_date[$date]["download_count"] = 0;
				$log_per_date[$date]["view_count"] = 0;
			}
			// アイテム閲覧数
			$log_per_date[$date]["view_count"] = isset($log_per_date_views[$ii]["cnt"]) ? $log_per_date_views[$ii]["cnt"] : 0;
		}
		unset($log_per_date_views);
		
		// ログファイル作成
		$this->writeLogPerPerDate($log_per_date);
	}
	
	/**
	 * Aggregate log per host
	 * ホスト毎にログを集計する
	 *
	 * @param $customReport Repository_Components_Business_Customreport Custom report object カスタムレポート処理オブジェクト
	 */
	private function aggregateLogPerHost($customReport) {
		// TODO: ビジネスの集計処理ではログファイルに必要な情報が取得できないため独自実装となっている
		$log_per_host = array();
		// 集計クエリ
		$subQuery = Repository_Components_Business_Logmanager::getSubQueryForAnalyzeLog(Repository_Components_Business_Logmanager::SUB_QUERY_TYPE_DEFAULT);
		$query = "SELECT CAST(LOG.record_date AS DATE) AS d1, LOG.ip_address AS ip_address, LOG.host AS host, count(LOG.log_no) AS cnt ";
		$query .= $subQuery[Repository_Components_Business_Logmanager::SUB_QUERY_KEY_FROM];
		$query .= " WHERE ".$subQuery[Repository_Components_Business_Logmanager::SUB_QUERY_KEY_WHERE].
				  " AND LOG.record_date >= ? ".
				  " AND LOG.record_date <= ? ".
				  " AND LOG.operation_id = ? ".
				  " AND LOG.item_id IS NOT NULL ".
				  " GROUP BY d1, ip_address ".
				  " ORDER BY d1, ip_address; ";
		
		// アイテム登録数
		$params = array();
		$params[] = $this->start_year. "-". $this->start_month. "-". $this->start_day. " 00:00:00.000";
		$params[] = $this->end_year. "-". $this->end_month. "-". $this->end_day. " 23:59:59.999";
		$params[] = Repository_Components_Business_Logmanager::LOG_OPERATION_ENTRY_ITEM;
		$log_per_host_items = $this->executeSql($query, $params);
		for($ii = 0; $ii < count($log_per_host_items); $ii++) {
			$date = $log_per_host_items[$ii]["d1"];
			$ip = $log_per_host_items[$ii]["ip_address"];
			// 配列初期化
			if(!isset($log_per_host[$date])){ $log_per_host[$date] = array(); }
			if(!isset($log_per_host[$date][$ip])) {
				$log_per_host[$date][$ip] = array();
				$log_per_host[$date][$ip]['record_date'] = $date;
				$log_per_host[$date][$ip]['ip_address'] = $ip;
				$log_per_host[$date][$ip]['host'] = $log_per_host_items[$ii]['host'];
				$log_per_host[$date][$ip]['item_count'] = 0;
				$log_per_host[$date][$ip]['download_count'] = 0;
				$log_per_host[$date][$ip]['view_count'] = 0;
			}
			// アイテム登録数
			$log_per_host[$date][$ip]["item_count"] = isset($log_per_host_items[$ii]["cnt"]) ? $log_per_host_items[$ii]["cnt"] : 0;
		}
		unset($log_per_host_items);
		
		// ファイルダウンロード回数
		$params = array();
		$params[] = $this->start_year. "-". $this->start_month. "-". $this->start_day. " 00:00:00.000";
		$params[] = $this->end_year. "-". $this->end_month. "-". $this->end_day. " 23:59:59.999";
		$params[] = Repository_Components_Business_Logmanager::LOG_OPERATION_DOWNLOAD_FILE;
		$log_per_host_downloads = $this->executeSql($query, $params);
		for($ii = 0; $ii < count($log_per_host_downloads); $ii++) {
			$date = $log_per_host_downloads[$ii]["d1"];
			$ip = $log_per_host_downloads[$ii]["ip_address"];
			// 配列初期化
			if(!isset($log_per_host[$date])) { $log_per_host[$date] = array(); }
			if(!isset($log_per_host[$date][$ip])) {
				$log_per_host[$date][$ip] = array();
				$log_per_host[$date][$ip]['record_date'] = $date;
				$log_per_host[$date][$ip]['ip_address'] = $ip;
				$log_per_host[$date][$ip]['host'] = $log_per_host_downloads[$ii]['host'];
				$log_per_host[$date][$ip]['item_count'] = 0;
				$log_per_host[$date][$ip]['download_count'] = 0;
				$log_per_host[$date][$ip]['view_count'] = 0;
			}
			// ファイルダウンロード数
			$log_per_host[$date][$ip]["download_count"] = isset($log_per_host_downloads[$ii]["cnt"]) ? $log_per_host_downloads[$ii]["cnt"] : 0;
		}
		unset($log_per_host_downloads);
		
		// アイテム閲覧回数
		$params = array();
		$params[] = $this->start_year. "-". $this->start_month. "-". $this->start_day. " 00:00:00.000";
		$params[] = $this->end_year. "-". $this->end_month. "-". $this->end_day. " 23:59:59.999";
		$params[] = Repository_Components_Business_Logmanager::LOG_OPERATION_DETAIL_VIEW;
		$log_per_host_views = $this->executeSql($query, $params);
		for($ii = 0; $ii < count($log_per_host_views); $ii++) {
			$date = $log_per_host_views[$ii]["d1"];
			$ip = $log_per_host_views[$ii]["ip_address"];
			// 配列初期化
			if(!isset($log_per_host[$date])) { $log_per_host[$date] = array(); }
			if(!isset($log_per_host[$date][$ip])) {
				$log_per_host[$date][$ip] = array();
				$log_per_host[$date][$ip]['record_date'] = $date;
				$log_per_host[$date][$ip]['ip_address'] = $ip;
				$log_per_host[$date][$ip]['host'] = $log_per_host_views[$ii]['host'];
				$log_per_host[$date][$ip]['item_count'] = 0;
				$log_per_host[$date][$ip]['download_count'] = 0;
				$log_per_host[$date][$ip]['view_count'] = 0;
			}
			// アイテム閲覧数
			$log_per_host[$date][$ip]["view_count"] = isset($log_per_host_views[$ii]["cnt"]) ? $log_per_host_views[$ii]["cnt"] : 0;
		}
		unset($log_per_host_views);
		
		$this->writeLogPerHost($log_per_host);
	}
	
	/**
	 * Aggregate log per item
	 * アイテム毎にログを集計する
	 *
	 * @param $customReport Repository_Components_Business_Customreport Custom report object カスタムレポート処理オブジェクト
	 */
	private function aggregateLogPerItem($customReport) {
		// TODO: ビジネスの集計処理ではログファイルに必要な情報が取得できないため独自実装となっている
		$log_per_item = array();
		// 集計用クエリ
		$subQuery = Repository_Components_Business_Logmanager::getSubQueryForAnalyzeLog(Repository_Components_Business_Logmanager::SUB_QUERY_TYPE_DEFAULT);
		$query = " SELECT CAST(LOG.record_date AS DATE) AS d1, LOG.item_id, LOG.item_no, count(*) AS cnt ".
				 $subQuery[Repository_Components_Business_Logmanager::SUB_QUERY_KEY_FROM].
				 " WHERE ".$subQuery[Repository_Components_Business_Logmanager::SUB_QUERY_KEY_WHERE].
				 " AND LOG.record_date >= ? ".
				 " AND LOG.record_date <= ? ".
				 " AND LOG.operation_id = ? ".
				 " AND LOG.item_id IS NOT NULL ".
				 " GROUP BY item_id, item_no ";
		
		// ファイルダウンロード数
		$params = array();
		$params[] = $this->start_year. "-". $this->start_month. "-". $this->start_day. " 00:00:00.000";
		$params[] = $this->end_year. "-". $this->end_month. "-". $this->end_day. " 23:59:59.999";
		$params[] = Repository_Components_Business_Logmanager::LOG_OPERATION_DOWNLOAD_FILE;
		$log_per_item_downloads = $this->executeSql($query, $params);
		for($ii = 0; $ii < count($log_per_item_downloads); $ii++) {
			$date = $log_per_item_downloads[$ii]["d1"];
			$item_id = $log_per_item_downloads[$ii]["item_id"];
			$item_no = $log_per_item_downloads[$ii]["item_no"];
			// 配列初期化
			if(!isset($log_per_host[$date])) { $log_per_host[$date] = array(); }
			if(!isset($log_per_item[$date][$item_id])){
				$log_per_item[$date][$item_id] = array();
				$log_per_item[$date][$item_id][$item_no] = array();
				$log_per_item[$date][$item_id][$item_no]['record_date'] = $date;
				$log_per_item[$date][$item_id][$item_no]['item_id'] = $item_id;
				$log_per_item[$date][$item_id][$item_no]['item_no'] = $item_no;
				$log_per_item[$date][$item_id][$item_no]['download_count'] = 0;
				$log_per_item[$date][$item_id][$item_no]['view_count'] = 0;
			}
			$log_per_item[$date][$item_id][$item_no]['download_count'] = isset($log_per_item_downloads[$ii]['cnt']) ? $log_per_item_downloads[$ii]['cnt'] : 0;
		}
		unset($log_per_item_downloads);
		
		// アイテム閲覧数
		$params = array();
		$params[] = $this->start_year. "-". $this->start_month. "-". $this->start_day. " 00:00:00.000";
		$params[] = $this->end_year. "-". $this->end_month. "-". $this->end_day. " 23:59:59.999";
		$params[] = Repository_Components_Business_Logmanager::LOG_OPERATION_DETAIL_VIEW;
		$log_per_item_views = $this->executeSql($query, $params);
		for($ii = 0; $ii < count($log_per_item_views); $ii++) {
			$date = $log_per_item_views[$ii]["d1"];
			$item_id = $log_per_item_views[$ii]["item_id"];
			$item_no = $log_per_item_views[$ii]["item_no"];
			// 配列初期化
			if(!isset($log_per_host[$date])) { $log_per_host[$date] = array(); }
			if(!isset($log_per_item[$date][$item_id])){
				$log_per_item[$date][$item_id] = array();
				$log_per_item[$date][$item_id][$item_no] = array();
				$log_per_item[$date][$item_id][$item_no]['record_date'] = $date;
				$log_per_item[$date][$item_id][$item_no]['item_id'] = $item_id;
				$log_per_item[$date][$item_id][$item_no]['item_no'] = $item_no;
				$log_per_item[$date][$item_id][$item_no]['download_count'] = 0;
				$log_per_item[$date][$item_id][$item_no]['view_count'] = 0;
			}
			$log_per_item[$date][$item_id][$item_no]['view_count'] = isset($log_per_item_views[$ii]['cnt']) ? $log_per_item_views[$ii]['cnt'] : 0;
		}
		unset($log_per_item_views);
		
		// ログファイル書き込み
		$this->writeLogPerItem($log_per_item);
	}
}
?>
