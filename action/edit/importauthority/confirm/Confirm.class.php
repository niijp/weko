<?php

/**
 * Name authority import confirmation action class
 * 著者名典拠インポート確認アクションクラス
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Confirm.class.php 80692 2017-08-08 03:03:43Z keiya_sugimoto $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Action base class for the WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryAction.class.php';
/**
 * Name authority import action class
 * 著者名典拠インポートアクションクラス
 */
require_once WEBAPP_DIR. '/modules/repository/action/edit/importauthority/Importauthority.class.php';


/**
 * Name authority import confirmation action class
 * 著者名典拠インポート確認アクションクラス
 * 
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class Repository_Action_Edit_Importauthority_Confirm extends RepositoryAction
{
    /**
     * execute insert authority into name_authority table
     * 著者名典拠テーブルへのインサート
     */
    function executeApp()
    {
        // 表示タブ指定
        $this->Session->setParameter("nameAuthorityControlActiveTabId", 2);
        
        // upload tsv file to tmp folder
        $tmp_file = $this->getFilePathAuthorTsv();
        if($tmp_file == false){
            // アップロードされたファイルがTSVファイルではない。
            $this->addErrMsg("repository_name_authority_import_error_readFile");
            return 'error';
        }
        
        ////////////////////////////////////////
        // insert author
        ////////////////////////////////////////
        $errorList = array();
        $successNum = 0;
        $importAuthority = new Repository_Action_Edit_Importauthority($this->Session, $this->Db);
        $fileData = $importAuthority->readFile($tmp_file);
        if($fileData == false){
            // ファイルデータの読み込み失敗
            $this->addErrMsg("repository_name_authority_import_error_readFile");
            return 'error';
        }
        $authorNum = count($fileData) - 1;
        if($authorNum < 1){
            // インポートファイルが空
            $this->addErrMsg("repository_name_authority_import_error_authorData");
            return 'error';
        }
        
        // エラー出力のための言語リソース
        $container =& DIContainerFactory::getContainer();
        $filterChain =& $container->getComponent('FilterChain');
        $smartyAssign =& $filterChain->getFilterByName('SmartyAssign');
        
        // divide tsv data by tab
        $dividedDataArray = array();
        $importAuthority->divideTsvToArray($fileData, $dividedDataArray);
        // create metadata for name authority
        $metadataForNameAuthority = array();
        $importAuthority->createMetadataForNameAuthority($dividedDataArray, $metadataForNameAuthority);
        // insert Name Authority
        $nameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        for ($nCnt = 0; $nCnt < $authorNum; $nCnt++)
        {
            if(strlen($metadataForNameAuthority[$nCnt][Repository_Action_Edit_Importauthority::FAMILY]) < 1)
            {
                // 必須入力となる「姓」がない
                array_push($errorList, vsprintf($smartyAssign->getLang("repository_name_authority_import_error_requiredFamily"), array($nCnt+2)));
                continue;
            }
            // entry name authority
            $result = $nameAuthority->entryNameAuthority($metadataForNameAuthority[$nCnt]);
            if($result === false)
            {
                // 典拠のインポートに失敗
                array_push($errorList, vsprintf($smartyAssign->getLang("repository_name_authority_import_error_insertNameAuthority"), array($nCnt+2)));
                $this->errorLog("Failed entry NameAuthority Not Identify.", __FILE__, __CLASS__, __LINE__);
                continue;
            }
            $successNum++;
        }
        
        // エラー情報、成功した件数をViewに受け渡す
        $this->Session->setParameter("importNameAuthorityErrorList", $errorList);
        $this->Session->setParameter("importNameAuthoritySuccess", $successNum);
        
        // delete work file
        unlink($tmp_file);
        
        return 'success';
    }
    
    /**
     * get file path
     * ファイルパス取得
     */
    private function getFilePathAuthorTsv(){
    
        // get upload file
        $tmp_file = $this->Session->getParameter("filelist");
        
        $dir_path = WEBAPP_DIR. "/uploads/repository/";
        $file_path = $dir_path . $tmp_file[0]['physical_file_name'];
        
        if($tmp_file[0]['extension'] != "tsv"){
            unlink($file_path);
            return false;
        }
        
        return $file_path;
    }
}
?>
