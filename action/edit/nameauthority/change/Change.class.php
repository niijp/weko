<?php
/**
 * Replace author.
 * 著者を付替える。
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Change.class.php 76512 2017-02-22 11:50:18Z keiya_sugimoto $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
 * Action base class for WEKO
 * WEKO用アクション基底クラス
 * 
 */
require_once WEBAPP_DIR. '/modules/repository/components/common/WekoAction.class.php';

/**
 * Replace author.
 * 著者を付替える。
 * 
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Action_Edit_Nameauthority_Change extends WekoAction
{
    /**
     * 統合前の著者ID
     * 
     * @var int
     */
    public $before_author_id = null;
    
    /**
     * 統合後の著者ID
     * 
     * @var id
     */
    public $after_author_id = null;
    
    /**
     * 付け替え対象となるアイテムのアイテムID
     * 
     * @var array
     */
    public $item_id_list = null;
    
    /**
     * メタデータを上書きする
     * 
     * @var uint
     */
    public $overwrite_flg = null;
    
    /**
     * Whether to check error condition of input condition only
     * 入力条件のエラーチェックのみを行うかどうか
     *
     * @var int
     */
    public $isCheckOnly = null;
    
    // Bugfix WEKO_IVIS-92 Y.Nakao 2019.03.22 --start--
    /**
     * Update search table.
     * 検索テーブル更新フラグ
     *
     * @var bool
     */
    private $searchTableUpdateFlag = false;
    
    /**
     * call search table update.
     * 検索テーブルの更新処理呼び出し
     */
    protected function afterTrans() {
        if($this->searchTableUpdateFlag) {
            require_once WEBAPP_DIR. '/modules/repository/components/RepositorySearchTableProcessing.class.php';
            $searchTableProcessing = new RepositorySearchTableProcessing($this->Session, $this->Db);
            $searchTableProcessing->callAsyncProcess();
        }
        parent::afterTrans();
    }
    // Bugfix WEKO_IVIS-92 Y.Nakao 2019.03.22 --end--
    
    /**
     * Delete the specified author ID from the author name authority.
     * 指定した著者IDを著者名典拠から削除する。
     * 
     * @return string Result code. 実行結果
     */
    public function executeApp()
    {
        // JSON出力するAPIのためViewは呼ばない。
        // エラーメッセージは$this->addErrMsgで詰め、json出力される。
        $this->exitFlag = true;
        
        // 追加する著者情報の必須項目、入力値チェック
        if(!$this->validateRequestParameter())
        {
            // 必須不足
            return;
        }
        
        // 統合先著者のメールアドレスチェック
        if(!$this->checkExistEmailAddress())
        {
            return;
        }
        
        // 入力チェックのみを行う場合、以下の処理は行わない
        if($this->isCheckOnly == 0)
        {
            // 付替え＆著者名典拠情報のマージ
            try
            {
                $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
                $busNameAuthority->changeNameAuthority($this->item_id_list, $this->before_author_id, $this->after_author_id);
            }
            catch (AppException $e)
            {
                $this->addErrMsg("repository_name_authority_change_error_changeNameAuthority");
                return;
            }
            // 上書き
            try
            {
                if($this->overwrite_flg == 1)
                {
                    $busoverwriteMetadata = BusinessFactory::getFactory()->getBusiness('businessOverwriteMetadata');
                    $busoverwriteMetadata->setComponents($this->Session, $this->Db);
                    $this->searchTableUpdateFlag = $busoverwriteMetadata->overwriteMetadataWithNameAuthority($this->after_author_id);
                }
            }
            catch (AppException $e)
            {
                $this->addErrMsg("repository_name_authority_change_error_overwriteMetadata");
                return;
            }
        }
        
        // 成功時、統合前と統合先のWEKO著者IDを返却する。(結果表示に使用する)
        $changeResult = array('beforeAuthorId'=>$this->before_author_id, 'afterAuthorId'=>$this->after_author_id);
        $businessGeneratejsontypedata = BusinessFactory::getFactory()->getBusiness('businessGeneratejsontypedata');
        echo $businessGeneratejsontypedata->raw_json_encode($changeResult);
    }
    
    /**
     * Check the request parameters and if there is a problem process error.
     * リクエストパラメータをチェックし問題があればエラー処理。
     * 
     * @return bool true=>OK / false=>NG
     */
    private function validateRequestParameter()
    {
        // 指定された著者IDが不正な場合は終了
        if($this->before_author_id < 1 || $this->after_author_id < 1)
        {
            $this->addErrMsg("repository_name_authority_change_error_authorId");
            return false;
        }
        
        // 更新対象の著者ID存在チェック
        $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        if(!$busNameAuthority->existsAuthor($this->before_author_id) || !$busNameAuthority->existsAuthor($this->after_author_id))
        {
            // 存在しない＝既に削除されている。
            $this->addErrMsg("repository_name_authority_change_error_existsAuthorId");
            return false;
        }
        
        // アイテムIDは1以上の整数でなければならない
        $itemIdList = array();
        for($ii=0; $ii<count($this->item_id_list); $ii++)
        {
            
            if(!is_numeric($this->item_id_list[$ii])) continue;
            $this->item_id_list[$ii] = intval($this->item_id_list[$ii]);
            if($this->item_id_list[$ii] < 1) continue;
            array_push($itemIdList, $this->item_id_list[$ii]);
        }
        // 付け替え対象のアイテムについて存在チェック
        $busItemAuthority = BusinessFactory::getFactory()->getBusiness('businessItemAuthority');
        $itemIdList = $busItemAuthority->existsItems($itemIdList);
        if(count($itemIdList) != count($this->item_id_list))
        {
            // 不正なアイテムIDまたは削除済アイテムが指定されていた。
            $this->addErrMsg("repository_name_authority_change_error_existsItem");
            return false;
        }
        $this->item_id_list = $itemIdList;
        
        // 付け替え対象のアイテムが0件＝終了
        if(count($this->item_id_list) == 0)
        {
            $this->addErrMsg("repository_name_authority_change_error_targetItem");
            return false;
        }
        
        // 上書きフラグはラジオボタンのため、チェックONの場合のみ値が送付される。
        // このため、チェックOFFの場合に「0」を埋める。
        if(!isset($this->overwrite_flg)) $this->overwrite_flg = 0;
        
        return true;
    }
    
    /**
     * Check if mail address is set for integration destination
     * 統合先にメールアドレスが設定されているかチェックする
     * 
     * @return bool true=>OK / false=>NG
     */
    private function checkExistEmailAddress()
    {
        $busFeedbackMail = BusinessFactory::getFactory()->getBusiness('businessSendfeedbackmail');
        if($busFeedbackMail->checkSetFeedbackMailByAuthor($this->item_id_list, $this->before_author_id))
        {
            $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
            $result = $busNameAuthority->getExternalAuthorIdPrefixAndSuffix($this->after_author_id, true);
            for($ii = 0; $ii < count($result); $ii++)
            {
                if($result[$ii]["prefix_id"] == 0 && strlen($result[$ii]["suffix"]) > 0)
                {
                    // 統合先著者にメールアドレスが設定されていればOK
                    return true;
                }
            }
            
            // 統合先著者にメールアドレスが設定されていなければNG
            $this->addErrMsg("repository_name_authority_change_error_noEmailAddress");
            return false;
        }
        
        // 付替え対象にフィードバックメール送信先が設定されていないければOK
        return true;
    }
}
?>
