<?php
/**
 * Update author information authority information.
 * 著者名典拠の情報を更新する。
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Edit.class.php 76595 2017-02-24 08:28:35Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
 * Action base class for WEKO
 * WEKO用アクション基底クラス
 * 
 */
require_once WEBAPP_DIR. '/modules/repository/components/common/WekoAction.class.php';

/**
 * Search process class
 * 検索処理クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositorySearch.class.php';

/**
 * Update author information authority information.
 * 著者名典拠の情報を更新する。
 *   指定された言語の典拠データを指定されたアイテムのメタデータに上書きする。
 * 
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Action_Edit_Nameauthority_Edit extends WekoAction
{
    /**
     * Author ID to be deleted
     * 削除対象の著者ID
     *
     * @var int
     */
    public $author_id = null;
    
    /**
     * (japanese)Surname.
     * (日)姓
     *
     * @var String
     */
    public $family = null;
    
    /**
     * (japanese)Name
     * (日)名
     *
     * @var String
     */
    public $name = null;
    
    /**
     * (japanese)Surname(ruby).
     * (日)姓(ヨミ)
     *
     * @var String
     */
    public $family_ruby = null;
    
    /**
     * (japanese)Name(ruby).
     * (日)名(ヨミ)
     *
     * @var String
     */
    public $name_ruby = null;
    
    /**
     * (english)Surname.
     * (英)姓
     *
     * @var String
     */
    public $family_en = null;
    
    /**
     * (english)Name
     * (英)名
     *
     * @var String
     */
    public $name_en = null;
    
    /**
     * (other language)Surname.
     * (その他言語)姓
     *
     * @var String
     */
    public $family_other = null;
    
    /**
     * (other language)Name
     * (その他言語)名
     *
     * @var String
     */
    public $name_other = null;
    
    /**
     * (other language)Surname(ruby).
     * (その他言語)姓(ヨミ)
     *
     * @var String
     */
    public $family_other_ruby = null;
    
    /**
     * (other language)Name(ruby)
     * (その他言語)名(ヨミ)
     *
     * @var String
     */
    public $name_other_ruby = null;
    
    /**
     * author's mail address
     * 著者のメールアドレス
     *
     * @var String
     */
    public $mail = null;
    
    /**
     * 外部著者ID(Prefix)の選択値
     *   1 => CiNii ID
     *   2 => 研究者リゾルバーID
     *   3 => 科研費研究者番号
     *   4以上 => ユーザーが任意に登録した外部著者ID
     *
     * @var int[]
     */
    public $prefix_id_list = null;
    
    /**
     * 外部著者ID(Suffix)の入力値
     *
     * @var String[]
     */
    public $suffix_list = null;
    
    /**
     * (JPN) Meta data overwrite flag of author information
     * (日)著者情報のメタデータ上書きフラグ
     * 
     * @var int
     */
    public $overwrite_flg_ja = null;
    
    /**
     * (ENG) Meta data overwrite flag of author information
     * (英)著者情報のメタデータ上書きフラグ
     * 
     * @var int
     */
    public $overwrite_flg_en = null;
    
    /**
     * (OHTER) Meta data overwrite flag of author information
     * (その他)著者情報のメタデータ上書きフラグ
     *
     * @var int
     */
    public $overwrite_flg_other = null;
    
    /**
     * (MAIL) Meta data overwrite flag of author information
     * (メールアドレス)著者情報のメタデータ上書きフラグ
     *
     * @var int
     */
    public $overwrite_flg_mail = null;
    
    /**
     * Whether to check error condition of input condition only
     * 入力条件のエラーチェックのみを行うかどうか
     *
     * @var int
     */
    public $isCheckOnly = null;
    
    // Bugfix WEKO_IVIS-92 Y.Nakao 2019.03.22 --start--
    /**
     * Update search table.
     * 検索テーブル更新フラグ
     *
     * @var bool
     */
    private $searchTableUpdateFlag = false;
    
    /**
     * call search table update.
     * 検索テーブルの更新処理呼び出し
     */
    protected function afterTrans() {
        if($this->searchTableUpdateFlag) {
            require_once WEBAPP_DIR. '/modules/repository/components/RepositorySearchTableProcessing.class.php';
            $searchTableProcessing = new RepositorySearchTableProcessing($this->Session, $this->Db);
            $searchTableProcessing->callAsyncProcess();
        }
        parent::afterTrans();
    }
    // Bugfix WEKO_IVIS-92 Y.Nakao 2019.03.22 --end--
    
    /**
     * 著者名典拠を変更する。
     * 
     * @return string Result code. 実行結果
     */
    public function executeApp()
    {
        // JSON出力するAPIのためViewは呼ばない。
        // エラーメッセージは$this->addErrMsgで詰め、json出力される。
        $this->exitFlag = true;
        
        // 編集結果
        $editResult = array('authorId'=>$this->author_id, 'conflict'=>-1);
        
        // 追加する著者情報の必須項目、入力値チェック
        $this->validateRequestParameter();
        if(!$this->isRequired())
        {
            // 必須不足
            return;
        }
        
        // 著者、メールアドレス削除時、アイテムに紐付いているかチェック
        if(!$this->checkExistItemWhenDeleteAuthor())
        {
            // アイテムに紐付いている場合は削除できない
            return;
        }
        
        // 同定チェック。ユーザーが更新許可済の場合は実施しない。
        if($this->isCheckOnly == 1)
        {
            // 一致する著者があればユーザーに確認する。ただし、自分自身はのぞく。
            if($this->isIdentificationExternalAuthorId())
            {
                // 外部著者IDによる同定先が編集対象の著者ID以外に存在するため、ユーザーに変更確認を行う。
                $editResult['conflict'] = 1;
            }
        }
        
        // 入力チェックのみを行う場合、以下の処理は行わない
        if($this->isCheckOnly == 0)
        {
            // 著者名典拠を更新する。
            try
            {
                 $this->editNameAuthority();
            }
            catch (AppException $e)
            {
                // エラー発生時のユーザー通知はAPI呼び出し元で行うためログを出力して進む。
                $this->addErrMsg("repository_name_authority_edit_error_editNameAuthority");
                $this->errorLog("Failed entry NameAuthority Not Identify.", __FILE__, __CLASS__, __LINE__);
                return;
            }
            
            // メタデータ上書き
            try
            {
                $this->overwriteMetadata();
            }
            catch (AppException $e)
            {
                // エラー発生時のユーザー通知はAPI呼び出し元で行うためログを出力して進む。
                $this->addErrMsg("repository_name_authority_edit_error_overwriteMetadata");
                $this->errorLog("Failed entry NameAuthority Not Identify.", __FILE__, __CLASS__, __LINE__);
                return;
            }
        }
        
        // JSON出力
        $businessGeneratejsontypedata = BusinessFactory::getFactory()->getBusiness('businessGeneratejsontypedata');
        echo $businessGeneratejsontypedata->raw_json_encode($editResult);
        return;
        
    }
    
    /**
     * リクエストパラメータの空白文字除去
     */
    private function validateRequestParameter()
    {
        // 空文字置換しつつ入力値をセット
        $this->family = preg_replace("/^( |　)+$/", "", $this->family);
        $this->name = preg_replace("/^( |　)+$/", "", $this->name);
        $this->family_ruby = preg_replace("/^( |　)+$/", "", $this->family_ruby);
        $this->name_ruby = preg_replace("/^( |　)+$/", "", $this->name_ruby);
        $this->family_en = preg_replace("/^( |　)+$/", "", $this->family_en);
        $this->name_en = preg_replace("/^( |　)+$/", "", $this->name_en);
        $this->family_other = preg_replace("/^( |　)+$/", "", $this->family_other);
        $this->name_other = preg_replace("/^( |　)+$/", "", $this->name_other);
        $this->family_other_ruby = preg_replace("/^( |　)+$/", "", $this->family_other_ruby);
        $this->name_other_ruby = preg_replace("/^( |　)+$/", "", $this->name_other_ruby);
        $this->mail = preg_replace("/^( |　)+$/", "", $this->mail);
        
        // 上書きフラグはラジオボタンのため、チェックONの場合のみ値が送付される。
        // このため、チェックOFFの場合に「0」を埋める。
        if(!isset($this->overwrite_flg_ja)) $this->overwrite_flg_ja = 0;
        if(!isset($this->overwrite_flg_en)) $this->overwrite_flg_en = 0;
        if(!isset($this->overwrite_flg_other)) $this->overwrite_flg_other = 0;
        if(!isset($this->overwrite_flg_mail)) $this->overwrite_flg_mail = 0;
        
        // 外部著者ID
        $prefix = array();
        $suffix = array();
        for($ii=0; $ii<count($this->prefix_id_list); $ii++)
        {
            // prefixID==0は未選択状態である。
            // 対応するsuffixはdisabledのためデータが渡らないので、無視する。
            if($this->prefix_id_list[$ii] == 0)
            {
                array_splice($this->prefix_id_list, $ii, 1);
                $ii--;
                continue;
            }
            
            // SuffixIDが未入力は登録しない
            if(!isset($this->suffix_list[$ii])) continue;
            $suffixID = preg_replace("/^( |　)+$/", "", $this->suffix_list[$ii]);
            if(strlen($suffixID) == 0) continue;
            
            // 指定された外部著者IDのPrefixIDが存在しているか
            if($this->existsNameAuthorityPrefixID($this->prefix_id_list[$ii]))
            {
                array_push($prefix, $this->prefix_id_list[$ii]);
                array_push($suffix, $this->suffix_list[$ii]);
            }
        }
        $this->prefix_id_list = $prefix;
        $this->suffix_list = $suffix;
    }
    
    /**
     * Required input check.
     * 必須入力チェック
     *
     * @return boolean Whether the input check is satisfied 入力チェックを満たしているか
     */
    private function isRequired()
    {
        // 指定された著者IDが不正な場合は終了
        if($this->author_id < 1)
        {
            $this->addErrMsg("repository_name_authority_edit_error_numericAuthorId");
            return false;
        }
        
        // 更新対象の著者ID存在チェック
        $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        if(!$busNameAuthority->existsAuthor($this->author_id))
        {
            // 存在しない＝既に削除されている。
            $this->addErrMsg("repository_name_authority_edit_error_existsEditNameAuthority");
            return false;
        }
        
        // 「姓」の必須入力チェック
        // 言語の入力欄が空⇒典拠にあれば典拠データ削除(エラーではない)ただしメタデータは上書きしない。典拠に無ければ何もしない。
        // 「姓」の入力が空欄かつその他の入力がある⇒エラー
        $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        
        // 日本語
        if(strlen($this->family)==0 && (strlen($this->name)>0 || strlen($this->family_ruby)>0 || strlen($this->name_ruby)>0))
        {
            // 著者名典拠に存在しない言語について「姓」の入力が空欄かつその他の入力がある⇒エラー
            $this->addErrMsg("repository_name_authority_edit_error_requiredSurname");
            return false;
        }
        
        // 英語
        if(strlen($this->family_en)==0 && strlen($this->name_en)>0)
        {
            // 著者名典拠に存在しない言語について「姓」の入力が空欄かつその他の入力がある⇒エラー
            $this->addErrMsg("repository_name_authority_edit_error_requiredSurname");
            return false;
        }
        
        // その他
        if(strlen($this->family_other)==0 && (strlen($this->name_other)>0 || strlen($this->family_other_ruby)>0 || strlen($this->name_other_ruby)>0))
        {
            // 著者名典拠に存在しない言語について「姓」の入力が空欄かつその他の入力がある⇒エラー
            $this->addErrMsg("repository_name_authority_edit_error_requiredSurname");
            return false;
        }
        
        // 全言語の「姓」の入力が空欄⇒エラー
        if(strlen($this->family)==0 && strlen($this->family_en)==0 && strlen($this->family_other)==0)
        {
            $this->addErrMsg("repository_name_authority_edit_error_requiredSurname");
            return false;
        }
        
        return true;
    }
    
    /**
     * When deletion of the author or mail address, check whether it is tied to the item
     * 著者、メールアドレス削除時、アイテムに紐付いていないかチェック
     *
     * @return boolean Whether the author or mail address has been deleted and not linked to the item 著者またはメールアドレスが削除されるかつアイテムに紐付いていないか
     */
    private function checkExistItemWhenDeleteAuthor()
    {
        if($this->checkDeleteAuthorWhenExistAuthor($this->author_id, $this->family, "japanese")
           || $this->checkDeleteAuthorWhenExistAuthor($this->author_id, $this->family_en, "english")
           || $this->checkDeleteAuthorWhenExistAuthor($this->author_id, $this->family_other, "")
           || $this->checkDeleteMailWhenExistMail($this->author_id, $this->mail))
        {
            // 削除対象の著者IDがアイテムに紐付いている場合、削除不可
            $repositorySearch = new RepositorySearch();
            $repositorySearch->Db = $this->Db;
            $repositorySearch->Session = $this->Session;
            $repositorySearch->search_term[RepositorySearch::REQUEST_WEKO_AUTHOR_ID] = $this->author_id;
            $repositorySearch->listResords = "all";
            $result = $repositorySearch->search();
            $total_num = $repositorySearch->getTotal();
            if($total_num > 0)
            {
                $this->addErrMsg("repository_name_authority_edit_error_unlinkItemDeleteExistsAuthor");
                return false;
            }
            
            // 著者フィードバックメール送信先に登録されている場合、削除不可
            $busSendFeedbackMail = BusinessFactory::getFactory()->getBusiness('businessSendfeedbackmail');
            if($busSendFeedbackMail->feedbackmailItemExists($this->author_id))
            {
                $this->addErrMsg("repository_name_authority_edit_error_unlinkFeedbackMailDeleteExistsAuthor");
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Check if the author of the specified language already exists and is about to delete
     * 指定した言語の著者が既に存在し、削除しようとしているかチェック
     * 
     * @param int $author_id WEKO Author id. WEKO著者ID
     * @param string $family surname. 姓
     * @param string $language author lanugage. 著者名典拠の言語。
     * @return boolean Whether the author of the specified language already exists and is about to delete 指定した言語の著者が既に存在し、削除しようとしているか
     */
    private function checkDeleteAuthorWhenExistAuthor($author_id, $family, $language)
    {
        $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        
        if($busNameAuthority->existsAuthorLanguage($author_id, $language))
        {
            if(strlen($family) == 0)
            {
                // 著者名典拠に存在する言語について「姓」の入力が空欄⇒エラー
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Check if the mail address already exists and is about to delete
     * メールアドレスが既に存在し、削除しようとしているかチェック
     *
     * @param int $author_id WEKO Author id. WEKO著者ID
     * @param string mail address.メールアドレス。
     * @return boolean Whether the mail address already exists and is about to delete メールアドレスが既に存在し、削除しようとしているか
     */
    private function checkDeleteMailWhenExistMail($author_id, $mail)
    {
        $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        
        $result = $busNameAuthority->getExternalAuthorIdPrefixAndSuffix($author_id, true);
        for($ii = 0; $ii < count($result); $ii++)
        {
            if($result[$ii]["prefix_id"] == 0 && strlen($result[$ii]["suffix"]) > 0)
            {
                if(strlen($mail) == 0)
                {
                    // 著者名典拠に存在する言語について「姓」の入力が空欄⇒エラー
                    return true;
                }
            }
        }
        
        return false;
    }
    
    /**
     * Author Name Renewal Update
     * 著者名典拠更新
     * 
     */
    private function editNameAuthority()
    {
        // 更新データなし＝削除
        // 著者名典拠データ更新
        $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        $busNameAuthority->editNameAuthority($this->author_id, "japanese", $this->family, $this->name, $this->family_ruby, $this->name_ruby);
        $busNameAuthority->editNameAuthority($this->author_id, "english", $this->family_en, $this->name_en);
        $busNameAuthority->editNameAuthority($this->author_id, "", $this->family_other, $this->name_other, $this->family_other_ruby, $this->name_other_ruby);
        
        // 外部著者IDを一度全て削除
        $busNameAuthority->deleteAllExternalAuthorSuffix($this->author_id);
        
        // メールアドレス更新（空更新＝削除）
        $busNameAuthority->editExternalAuthorSuffix($this->author_id, 0, $this->mail);
        
        // 渡されたリクエストパラメータの外部著者IDに更新（空更新＝削除）
        for($ii=0; $ii<count($this->prefix_id_list); $ii++)
        {
            $busNameAuthority->editExternalAuthorSuffix($this->author_id, $this->prefix_id_list[$ii], $this->suffix_list[$ii]);
        }
    }
    
    /**
     * Overwrite metadata.
     * メタデータを上書きする。
     *   ※上書き指定がある かつ 「姓」の入力がある言語のみ。
     * 
     */
    private function overwriteMetadata()
    {
        $busoverwriteMetadata = BusinessFactory::getFactory()->getBusiness('businessOverwriteMetadata');
        $busoverwriteMetadata->setComponents($this->Session, $this->Db);
        
        // メタデータ上書き(日)
        if($this->overwrite_flg_ja == 1 && strlen($this->family) > 0)
        {
            $busoverwriteMetadata->overwriteMetadata($this->author_id, "japanese", $this->family, $this->name, $this->family_ruby, $this->name_ruby);
            $this->searchTableUpdateFlag = true;
        }
        
        // メタデータ上書き(英)
        if($this->overwrite_flg_en == 1 && strlen($this->family_en) > 0)
        {
            $busoverwriteMetadata->overwriteMetadata($this->author_id, "english", $this->family_en, $this->name_en);
            $this->searchTableUpdateFlag = true;
        }
        
        // メタデータ上書き(その他)
        if($this->overwrite_flg_other == 1 && strlen($this->family_other) > 0)
        {
            $busoverwriteMetadata->overwriteMetadata($this->author_id, "", $this->family_other, $this->name_other);
            $this->searchTableUpdateFlag = true;
        }
        
        // メール上書き（空更新を許可する）
        if($this->overwrite_flg_mail == 1)
        {
            $busoverwriteMetadata->overwriteMailAddress($this->author_id, $this->mail);
            $this->searchTableUpdateFlag = true;
        }
    }
    
    /**
     * Check whether the specified external author ID type is deleted.
     * 指定された外部著者ID種別が削除されていないかチェックする。
     *
     * @param int External author ID type (prefix).外部著者ID種別(prefix)
     * @return bool true=>Eexists. / false-> NoExists.
     */
    private function existsNameAuthorityPrefixID($authorPrefixId)
    {
        $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        $externalPrefixList = $busNameAuthority->getExternalAuthorIdPrefixList();
        
        for($ii=0; $ii<count($externalPrefixList); $ii++)
        {
            if($externalPrefixList[$ii]["prefix_id"] == $authorPrefixId)
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Confirm the existence of the author identified by the entered external author ID. However, excluding editing subjects (themselves).
     * 入力されたの外部著者IDにより同定される著者の存在を確認する。ただし編集対象(自分自身)は除く。
     * 
     * @return true=>There are authors of identifiers other than me. 自分以外に同定先の著者が存在する。 / false=>There are no authors of identifiers other than myself. 自分以外に同定先の著者が存在しない。
     */
    private function isIdentificationExternalAuthorId()
    {
        $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        $externalAuthorId = array();
        if(strlen($this->mail) > 0) array_push($externalAuthorId, array('prefix_id'=>'0', 'suffix'=>$this->mail));
        for($ii=0; $ii<count($this->prefix_id_list); $ii++)
        {
            array_push($externalAuthorId, array('prefix_id'=>$this->prefix_id_list[$ii], 'suffix'=>$this->suffix_list[$ii]));
        }
        $isConflict = $busNameAuthority->isIdentificationExternalAuthorId($externalAuthorId, array($this->author_id));
        if($isConflict)
        {
            return true;
        }
        return false;
    }
}
?>
