<?php
/**
 * 著者名典拠へデータを新規追加する。
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Add.class.php 76568 2017-02-23 12:10:52Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
 * Action base class for WEKO
 * WEKO用アクション基底クラス
 * 
 */
require_once WEBAPP_DIR. '/modules/repository/components/common/WekoAction.class.php';

/**
 * Add data to the author name authority.
 * 著者名典拠へデータを新規追加する。
 * 
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Action_Edit_Nameauthority_Add extends WekoAction
{
    /**
     * (japanese)Surname.
     * (日)姓
     * 
     * @var String
     */
    public $family = null;
    
    /**
     * (japanese)Name
     * (日)名
     * 
     * @var String
     */
    public $name = null;
    
    /**
     * (japanese)Surname(ruby).
     * (日)姓(ヨミ)
     * 
     * @var String
     */
    public $family_ruby = null;
    
    /**
     * (japanese)Name(ruby).
     * (日)名(ヨミ)
     * 
     * @var String
     */
    public $name_ruby = null;
    
    /**
     * (english)Surname.
     * (英)姓
     * 
     * @var String
     */
    public $family_en = null;
    
    /**
     * (english)Name
     * (英)名
     * 
     * @var String
     */
    public $name_en = null;
    
    /**
     * (other language)Surname.
     * (その他言語)姓
     * 
     * @var String
     */
    public $family_other = null;
    
    /**
     * (other language)Name
     * (その他言語)名
     * 
     * @var String
     */
    public $name_other = null;
    
    /**
     * (other language)Surname(ruby).
     * (その他言語)姓(ヨミ)
     *
     * @var String
     */
    public $family_other_ruby = null;
    
    /**
     * (other language)Name(ruby)
     * (その他言語)名(ヨミ)
     *
     * @var String
     */
    public $name_other_ruby = null;
    
    /**
     * author's mail address
     * 著者のメールアドレス
     * 
     * @var String
     */
    public $mail = null;
    
    /**
     * 外部著者ID(Prefix)の選択値
     *   1 => CiNii ID
     *   2 => 研究者リゾルバーID
     *   3 => 科研費研究者番号
     *   4以上 => ユーザーが任意に登録した外部著者ID
     * 
     * @var int[]
     */
    public $prefix_id_list = null;
    
    /**
     * 外部著者ID(Suffix)の入力値
     * 
     * @var String[]
     */
    public $suffix_list = null;
    
    /**
     * Whether the user agreed to forcibly add even if a matching author with the entered external author ID has already been registered.
     * 入力された外部著者IDで一致する著者がすでに登録済でも強制的に追加することにユーザーが同意したか。
     *   1=>同意済。1以外=>未同意のため同定チェックする。
     * 
     * @var string
     */
    public $conflict = null;
    
    /**
     * Add data to the author name authority.
     * 著者名典拠へデータを新規追加する。
     * 
     * 出力するJSON：
     *   ['authorId'] = 登録した著者名典拠のID。-1の場合は失敗。
     * 
     */
    public function executeApp()
    {
        // JSON出力するAPIのためViewは呼ばない。
        // エラーメッセージは$this->addErrMsgで詰め、json出力される。
        $this->exitFlag = true;
        
        $addResult = array('authorId'=>-1, 'conflict'=>-1);
        
        // 追加する著者情報の必須項目、入力値チェック
        $this->validateRequestParameter();
        if(!$this->isRequired())
        {
            return;
        }
        $nameAuthority = $this->setNameAuthorityData();
        
        // 著者名典拠にデータを登録する。同定はしない。
        try
        {
            $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
            
            // 同定チェック
            if($this->conflict != 1)
            {
                // 一致する著者があればユーザーに確認する。
                $isConflict = $busNameAuthority->isIdentificationExternalAuthorId($nameAuthority["external_author_id"]);
                if($isConflict)
                {
                    $addResult['conflict'] = 1;
                    $businessGeneratejsontypedata = BusinessFactory::getFactory()->getBusiness('businessGeneratejsontypedata');
                    echo $businessGeneratejsontypedata->raw_json_encode($addResult);
                    return;
                }
            }
            
            $addResult['authorId'] = $busNameAuthority->entryNameAuthorityNotIdentify($nameAuthority["author"], $nameAuthority["external_author_id"]);
        }
        catch (AppException $e)
        {
            // エラー発生時のユーザー通知はAPI呼び出し元で行うためログを出力して進む。
            $this->addErrMsg("repository_name_authority_edit_error_addNameAuthority");
            $this->errorLog("Failed entry NameAuthority Not Identify.", __FILE__, __CLASS__, __LINE__);
            return;
        }
        
        // JSON出力
        $businessGeneratejsontypedata = BusinessFactory::getFactory()->getBusiness('businessGeneratejsontypedata');
        echo $businessGeneratejsontypedata->raw_json_encode($addResult);
    }
    
    /**
     * リクエストパラメータの空白文字除去
     */
    private function validateRequestParameter()
    {
        // スペースのみは空文字入力とする
        $this->family = preg_replace("/^( |　)+$/", "", $this->family);
        $this->name = preg_replace("/^( |　)+$/", "", $this->name);
        $this->family_ruby = preg_replace("/^( |　)+$/", "", $this->family_ruby);
        $this->name_ruby = preg_replace("/^( |　)+$/", "", $this->name_ruby);
        $this->family_en = preg_replace("/^( |　)+$/", "", $this->family_en);
        $this->name_en = preg_replace("/^( |　)+$/", "", $this->name_en);
        $this->family_other = preg_replace("/^( |　)+$/", "", $this->family_other);
        $this->name_other = preg_replace("/^( |　)+$/", "", $this->name_other);
        $this->family_other_ruby = preg_replace("/^( |　)+$/", "", $this->family_other_ruby);
        $this->name_other_ruby = preg_replace("/^( |　)+$/", "", $this->name_other_ruby);
        $this->mail = preg_replace("/^( |　)+$/", "", $this->mail);
        
        // 同意状態はintに変換
        $this->conflict = intval($this->conflict);
    }
    
    /**
     * Create registration data from request parameters. We also carry out validation.
     * リクエストパラメータから登録データを作成する。
     * また、下記の整形も行う。
     *   必須チェック
     *   空文字置換
     * @return array NameAuthority data for add.登録する著者名典拠データ
     * 
     */
    private function setNameAuthorityData()
    {
        $nameAuthority = array();
        $nameAuthority["author"] = array();
        $nameAuthority["external_author_id"] = array();
        
        // 著者情報をセット（必須となる「姓」が入力されている著者名典拠のみ追加する）
        if(strlen($this->family) > 0)
        {
            array_push($nameAuthority["author"], array("language"=>"japanese", "family"=>$this->family, "name"=>$this->name, "family_ruby"=>$this->family_ruby, "name_ruby"=>$this->name_ruby));
        }
        if(strlen($this->family_en) > 0)
        {
            array_push($nameAuthority["author"], array("language"=>"english", "family"=>$this->family_en, "name"=>$this->name_en, "family_ruby"=>"", "name_ruby"=>""));
        }
        if(strlen($this->family_other) > 0)
        {
            array_push($nameAuthority["author"], array("language"=>"","family"=>$this->family_other, "name"=>$this->name_other, "family_ruby"=>$this->family_other_ruby, "name_ruby"=>$this->name_other_ruby));
        }
        
        // メール
        if(strlen($this->mail) > 0)
        {
            array_push($nameAuthority["external_author_id"], array("prefix_id"=>0, "suffix"=>$this->mail));
        }
        // 外部著者ID
        $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        for($ii=0; $ii<count($this->prefix_id_list); $ii++)
        {
            // prefixID==0は未選択状態である。
            // 対応するsuffixはdisabledのためデータが渡らないので、無視する。
            if($this->prefix_id_list[$ii] == 0)
            {
                array_splice($this->prefix_id_list, $ii, 1);
                $ii--;
                continue;
            }
            
            // SuffixIDが未入力は登録しない
            if(!isset($this->suffix_list[$ii])) continue;
            $suffixID = preg_replace("/^( |　)+$/", "", $this->suffix_list[$ii]);
            if(strlen($suffixID) == 0) continue;
            
            // 指定された外部著者IDのPrefixIDが存在しているか
            if($this->nameAuthorityPrefixIDExists($this->prefix_id_list[$ii]))
            {
                array_push($nameAuthority["external_author_id"], array("prefix_id"=>$this->prefix_id_list[$ii], "suffix"=>$this->suffix_list[$ii]));
            }
        }
        
        return $nameAuthority;
    }
    
    /**
     * Required input check.
     * 必須入力チェック
     */
    private function isRequired()
    {
        // 日、英、その他のどれか1つでも「姓」が入力されていること
        if(strlen($this->family) == 0 && strlen($this->family_en) == 0 && strlen($this->family_other) == 0)
        {
            $this->addErrMsg("repository_name_authority_edit_error_requiredSurname");
            return false;
        }
        
        // 更新対象となっている言語の典拠について「姓」の入力がなくそれ以外の入力が一つでもある⇒エラー
        if(strlen($this->family)==0 && (strlen($this->name)>0 || strlen($this->family_ruby)>0 || strlen($this->name_ruby)>0))
        {
            $this->addErrMsg("repository_name_authority_edit_error_requiredSurname");
            return false;
        }
        if(strlen($this->family_en)==0 && strlen($this->name_en)>0)
        {
            $this->addErrMsg("repository_name_authority_edit_error_requiredSurname");
            return false;
        }
        if(strlen($this->family_other)==0 && (strlen($this->name_other)>0 || strlen($this->family_other_ruby)>0 || strlen($this->name_other_ruby)>0))
        {
            $this->addErrMsg("repository_name_authority_edit_error_requiredSurname");
            return false;
        }
        return true;
    }
    
    /**
     * Check whether the specified external author ID type is deleted.
     * 指定された外部著者ID種別が削除されていないかチェックする。
     * 
     * @param int External author ID type (prefix).外部著者ID種別(prefix)
     * @return bool true=>Eexists. / false-> NoExists.
     */
    private function nameAuthorityPrefixIDExists($authorPrefixId)
    {
        $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        $externalPrefixList = $busNameAuthority->getExternalAuthorIdPrefixList();
        
        for($ii=0; $ii<count($externalPrefixList); $ii++)
        {
            if($externalPrefixList[$ii]["prefix_id"] == $authorPrefixId)
            {
                return true;
            }
        }
        
        return false;
    }
}
?>
