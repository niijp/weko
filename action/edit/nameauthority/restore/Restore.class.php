<?php
/**
 * Revive authorized delegated authority.
 * 削除済の著者名典拠を復活する。
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Restore.class.php 76074 2017-02-07 10:17:33Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
 * Action base class for WEKO
 * WEKO用アクション基底クラス
 * 
 */
require_once WEBAPP_DIR. '/modules/repository/components/common/WekoAction.class.php';

/**
 * Revive authorized delegated authority.
 * 削除済の著者名典拠を復活する。
 * 
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Action_Edit_Nameauthority_Restore extends WekoAction
{
    /**
     * Author ID to be deleted
     * 削除済の著者ID
     *
     * @var int
     */
    public $author_id = null;
    
    /**
     * Revive authorized delegated authority.
     * 削除済の著者名典拠を復活する。
     * 
     * @return string Result code. 実行結果
     */
    public function executeApp()
    {
        // JSON出力するAPIのためViewは呼ばない。
        // エラーメッセージは$this->addErrMsgで詰め、json出力される。
        $this->exitFlag = true;
        
        // 指定された著者IDが不正な場合は終了
        if($this->author_id < 1)
        {
            $this->addErrMsg("repository_name_authority_edit_error_numericAuthorId");
            return;
        }
        
        // 削除を取り消す
        try
        {
            $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
            $addResult['authorId'] = $busNameAuthority->restoreNameAuthority($this->author_id);
        }
        catch (AppException $e)
        {
            // エラー発生時のユーザー通知はAPI呼び出し元で行うためログを出力して進む。
            $this->addErrMsg("repository_name_authority_edit_error_restoreNameAuthority");
            $this->errorLog("Failed entry NameAuthority Not Identify.", __FILE__, __CLASS__, __LINE__);
            return;
        }
        // 成功時は何も出力しない。
    }
}
?>
