<?php

/**
 * Action class download search result of authors
 * 著者検索結果ダウンロードアクションクラス
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Download.class.php 76468 2017-02-21 11:05:23Z keiya_sugimoto $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
 * ZIP file manipulation library
 * ZIPファイル操作ライブラリ
 */
include_once MAPLE_DIR.'/includes/pear/File/Archive.php';
/**
 * Action base class for WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/common/WekoAction.class.php';
/**
 * Action base class for the WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryAction.class.php';
/**
 * Common class file download
 * ファイルダウンロード共通クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryDownload.class.php';
/**
 * Search process class
 * 検索処理クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositorySearch.class.php';

/**
 * Action class download search result of authors
 * 著者検索結果ダウンロードアクションクラス
 * 
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class Repository_Action_Edit_Nameauthority_Search_Download extends WekoAction
{
    /**
     * File name
     * ファイル名
     *
     * @var string
     */
    const FILE_NAME = "search_result.tsv";
    
    /**
     * WEKO author ID
     * WEKO著者ID
     *
     * @var int
     */
    public $author_id = null;
    /**
     * Family name
     * 姓
     *
     * @var string
     */
    public $family = null;
    /**
     * Given name
     * 名
     *
     * @var string
     */
    public $name = null;
    /**
     * Mail address
     * メールアドレス
     *
     * @var string
     */
    public $mail = null;
    /**
     * External author ID prefix ID
     * 外部著者ID prefix ID
     *
     * @var int
     */
    public $prefix_id = null;
    /**
     * External author ID
     * 外部著者ID
     *
     * @var string
     */
    public $suffix = null;
    /**
     * Delete flag
     * 削除フラグ
     *
     * @var int
     */
    public $is_delete = null;
    /**
     * Work directory path
     * 作業用ディレクトリ
     *
     * @var string
     */
    private $repositorySearch = null;
    /**
     * Instance of item search class
     * アイテム検索クラスのインスタンス
     *
     * @var Object
     */
    private $tmp_dir = null;
    
    /**
     * Download name authority template file
     * 著者名典拠テンプレートファイルダウンロード
     */
    function executeApp()
    {
        // 作業用ディレクトリ作成
        $this->infoLog("businessWorkdirectory", __FILE__, __CLASS__, __LINE__);
        $businessWorkdirectory = BusinessFactory::getFactory()->getBusiness('businessWorkdirectory');
        $this->tmp_dir = $businessWorkdirectory->create();
        $this->tmp_dir = substr($this->tmp_dir, 0, -1);
        
        
        $this->downloadNameAuthorityTSV();
        
        $exitFlag = true;;
    }
    
    /**
     * export NameAuthority Data
     * 著者名典拠データエクスポート
     * 
     * @return boolean Result 結果
     */
    private function downloadNameAuthorityTSV()
    {
        $filepath = $this->tmp_dir. "/". self::FILE_NAME;
        
        // TSVファイル作成
        $this->createNameAuthorityTSV($filepath);
        
        //ダウンロードアクション処理
        $repositoryDownload = new RepositoryDownload();
        $repositoryDownload->downloadFile($filepath, self::FILE_NAME);
        
        return true;
    }
    
    /**
     * Create name authority data for TSV
     * TSV著者名典拠データ作成
     * 
     * @param string $filepath TSV file path TSVファイルパス
     */
    private function createNameAuthorityTSV($filepath)
    {
        $fp = fopen($filepath, "w");
        
        $businessNameauthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        $prefixList = $businessNameauthority->getExternalAuthorIdPrefixList();
        
        // TSVヘッダ作成
        $this->outputHeaderSearchResultNameAuthorityTSV($fp);
        
        $businessSearchnameauthority = BusinessFactory::getFactory()->getBusiness('businessSearchnameauthority');
        $authorIdList = $businessSearchnameauthority->searchNameAuthorityFromAuthorInfo($this->author_id, $this->family, $this->name, $this->mail, $this->prefix_id, $this->suffix, $this->is_delete);
        
        $this->repositorySearch = new RepositorySearch();
        $this->repositorySearch->Db = $this->Db;
        $this->repositorySearch->Session = $this->Session;
        $this->repositorySearch->listResords = "all";
        
        // 著者ごとのTSVデータ作成
        for($ii = 0; $ii < count($authorIdList); $ii++)
        {
            $this->outputNameAuthorityInfoTSV($fp, $authorIdList[$ii], $this->is_delete);
        }
        
        fclose($fp);
    }
    
    /**
     * Output header of name authority search results to TSV
     * 著者名典拠検索結果のヘッダをTSVに出力する
     * 
     * @param resorce $fp Output destination file pointer 出力先のファイルポインタ
     */
    private function outputHeaderSearchResultNameAuthorityTSV($fp)
    {
        // get lang resource
        $container =& DIContainerFactory::getContainer();
        $filterChain =& $container->getComponent("FilterChain");
        $smartyAssign =& $filterChain->getFilterByName("SmartyAssign");
        
        $businessNameauthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        $prefixList = $businessNameauthority->getExternalAuthorIdPrefixList();
        
        $header = $smartyAssign->getLang("repository_name_authority_edit_search_download_weko_author_id")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_import_headerNameAuthorityImportFile_surname")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_import_headerNameAuthorityImportFile_name")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_import_headerNameAuthorityImportFile_sei")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_import_headerNameAuthorityImportFile_mei")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_surname_en")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_name_en")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_surname_other")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_name_other")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_surname_other_ruby")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_name_other_ruby")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_import_headerNameAuthorityImportFile_mail")."\t";
        for($ii = 0; $ii < count($prefixList); $ii++)
        {
            $header .= $prefixList[$ii]["prefix_name"]."\t";
        }
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_delete_flag")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_item_id")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_title")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_url")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_import_headerNameAuthorityImportFile_surname")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_import_headerNameAuthorityImportFile_name")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_import_headerNameAuthorityImportFile_sei")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_import_headerNameAuthorityImportFile_mei")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_surname_en")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_name_en")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_surname_other")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_name_other")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_import_headerNameAuthorityImportFile_mail")."\t";
        $header .= $smartyAssign->getLang("repository_name_authority_edit_search_download_feedback")."\n";
        fwrite($fp, $header);
    }
    
    /**
     * Output name authority information to TSV
     * 著者名典拠情報をTSVに出力する
     * 
     * @param resorce $fp Output destination file pointer 出力先のファイルポインタ
     * @param int $author_id WEKO author ID WEKO著者ID
     * @param int $is_delete Delete flag 削除フラグ
     */
    private function outputNameAuthorityInfoTSV($fp, $author_id, $is_delete)
    {
        $businessLoadauthorinformation = BusinessFactory::getFactory()->getBusiness('businessLoadauthorinformation');
        $authorInfo = $businessLoadauthorinformation->selectAuthorInfoForNameAuthorityDownload($author_id, $is_delete);
        $externalAuthorInfo = $businessLoadauthorinformation->selectExternalAuthorInfoForNameAuthorityDownload($author_id, $is_delete);
        
        if($is_delete == 0)
        {
            $feedbackInfo = $businessLoadauthorinformation->selectFeedbackInfo($author_id);
            // 検索実行
            $this->repositorySearch->search_term[RepositorySearch::REQUEST_WEKO_AUTHOR_ID] = $author_id;
            $searchResult = $this->repositorySearch->search();
            $itemIdList = $businessLoadauthorinformation->createItemIdListAssociatedWithAuthor($searchResult, $feedbackInfo);
            if(count($itemIdList) > 0)
            {
                for($jj = 0; $jj < count($itemIdList); $jj++)
                {
                    $output = $this->outputAuthorInfoTabSeparator($author_id, $is_delete);
                    $output .= $this->outputItemInfoTabSeparator($author_id, $itemIdList[$jj]["item_id"], $itemIdList[$jj]["item_no"], $itemIdList[$jj]["feedback_flag"]);
                    fwrite($fp, $output);
                }
            }
            else
            {
                $output = $this->outputAuthorInfoTabSeparator($author_id, $is_delete);
                $output .= "\t\t\t\t\t\t\t\t\t\t\t\t\n";
                fwrite($fp, $output);
            }
        }
        else
        {
            $output = $this->outputAuthorInfoTabSeparator($author_id, $is_delete);
            $output .= "\t\t\t\t\t\t\t\t\t\t\t\t\n";
            if(preg_match("/^[\\t]+\\n$/", $output) == 0)
            {
                fwrite($fp, $output);
            }
        }
    }
    
    /**
     * Output author information by tab separator
     * 著者情報をタブ区切りで出力する
     * 
     * @param int $author_id WEKO author ID WEKO著者ID
     * @param int $is_delete Delete flag 削除フラグ
     * 
     * @return string Author information by tab separator タブ区切りの著者情報
     */
    private function outputAuthorInfoTabSeparator($author_id, $is_delete)
    {
        $businessLoadauthorinformation = BusinessFactory::getFactory()->getBusiness('businessLoadauthorinformation');
        $authorInfo = $businessLoadauthorinformation->selectAuthorInfoForNameAuthorityDownload($author_id, $is_delete);
        $externalAuthorInfo = $businessLoadauthorinformation->selectExternalAuthorInfoForNameAuthorityDownload($author_id, $is_delete);
        
        $businessNameauthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        $prefixList = $businessNameauthority->getExternalAuthorIdPrefixList();
        
        $output = $authorInfo["author_id"]."\t";
        $output .= $authorInfo["family_ja"]."\t";
        $output .= $authorInfo["name_ja"]."\t";
        $output .= $authorInfo["family_ruby"]."\t";
        $output .= $authorInfo["name_ruby"]."\t";
        $output .= $authorInfo["family_en"]."\t";
        $output .= $authorInfo["name_en"]."\t";
        $output .= $authorInfo["family_other"]."\t";
        $output .= $authorInfo["name_other"]."\t";
        $output .= $authorInfo["family_other_ruby"]."\t";
        $output .= $authorInfo["name_other_ruby"]."\t";
        $output .= $externalAuthorInfo["mail"]."\t";
        for($kk = 0; $kk < count($prefixList); $kk++)
        {
            $output .= $externalAuthorInfo["external_author_id_".$prefixList[$kk]["prefix_id"]]."\t";
        }
        $output .= $authorInfo["is_delete"]."\t";
        
        return $output;
    }
    
    /**
     * Output item information by tab separator
     * アイテム情報をタブ区切りで出力する
     * 
     * @param int $author_id WEKO author ID WEKO著者ID
     * @param int $item_id Item ID アイテムID
     * @param int $item_no Item No アイテム通番
     * @param boolean $isFeedback Whether it is attached as a feedback mail transmission destination フィードバックメール送信先として紐付いているか
     * 
     * @return string Item information by tab separator タブ区切りのアイテム情報
     */
    private function outputItemInfoTabSeparator($author_id, $item_id, $item_no, $isFeedback)
    {
        $businessLoadauthorinformation = BusinessFactory::getFactory()->getBusiness('businessLoadauthorinformation');
        $itemInfo = $businessLoadauthorinformation->selectItemInfoForNameAuthorityDownload($author_id, $item_id, $item_no);
        $output = $itemInfo["item_id"]."\t";
        if(strlen($itemInfo["title"]) > 0)
        {
            $output .= $itemInfo["title"]."\t";
        }
        else
        {
            $output .= $itemInfo["title_english"]."\t";
        }
        $output .= $itemInfo["url"]."\t";
        $output .= $itemInfo["family_ja"]."\t";
        $output .= $itemInfo["name_ja"]."\t";
        $output .= $itemInfo["family_ruby"]."\t";
        $output .= $itemInfo["name_ruby"]."\t";
        $output .= $itemInfo["family_en"]."\t";
        $output .= $itemInfo["name_en"]."\t";
        $output .= $itemInfo["family_other"]."\t";
        $output .= $itemInfo["name_other"]."\t";
        $output .= $itemInfo["mail"]."\t";
        if($isFeedback)
        {
            $output .= "1\n";
        }
        else
        {
            $output .= "0\n";
        }
        
        return $output;
    }
}
?>
