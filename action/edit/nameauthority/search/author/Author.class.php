<?php

/**
 * Action class name authority search
 * 著者名典拠検索アクションクラス
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Author.class.php 76055 2017-02-07 00:32:50Z keiya_sugimoto $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
 * Action base class for WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/common/WekoAction.class.php';
/**

/**
 * Action class name authority search
 * 著者名典拠検索アクションクラス
 * 
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class Repository_Action_Edit_Nameauthority_Search_Author extends WekoAction
{
    /**
     * WEKO author ID
     * WEKO著者ID
     *
     * @var int
     */
    public $author_id = null;
    /**
     * Family name
     * 姓
     *
     * @var string
     */
    public $family = null;
    /**
     * Given name
     * 名
     *
     * @var string
     */
    public $name = null;
    /**
     * Mail address
     * メールアドレス
     *
     * @var string
     */
    public $mail = null;
    /**
     * External author ID prefix ID
     * 外部著者ID prefix ID
     *
     * @var int
     */
    public $prefix_id = null;
    /**
     * External author ID
     * 外部著者ID
     *
     * @var string
     */
    public $suffix = null;
    /**
     * Delete flag
     * 削除フラグ
     *
     * @var int
     */
    public $is_delete = null;
    /**
     * Start position got author information
     * 取得する著者情報の開始位置
     *
     * @var int
     */
    public $start_num = null;
    /**
     * Number of search result per request
     * リクエストあたりの検索結果件数
     *
     * @var int
     */
    public $per_num = null;
    
    /**
     * Search name authority
     * 著者名典拠検索
     */
    function executeApp()
    {
        $businessSearchnameauthority = BusinessFactory::getFactory()->getBusiness('businessSearchnameauthority');
        $authorIdList = $businessSearchnameauthority->searchNameAuthorityFromAuthorInfo($this->author_id, $this->family, $this->name, $this->mail, $this->prefix_id, $this->suffix, $this->is_delete);
        
        $businessLoadauthorinformation = BusinessFactory::getFactory()->getBusiness('businessLoadauthorinformation');
        $authorList = $businessLoadauthorinformation->selectAuthorInfoJsonArray($authorIdList, $this->is_delete, $this->start_num, $this->per_num);
        
        $businessGeneratejsontypedata = BusinessFactory::getFactory()->getBusiness('businessGeneratejsontypedata');
        $json = $businessGeneratejsontypedata->raw_json_encode($authorList);
        
        echo $json;
        
        $this->exitFlag = true;
    }
}
?>
