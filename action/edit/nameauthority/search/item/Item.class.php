<?php

/**
 * Action class author affiliation item search
 * 著者所属アイテム検索アクションクラス
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Item.class.php 76398 2017-02-17 09:15:23Z keiya_sugimoto $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
 * Action base class for WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/common/WekoAction.class.php';
/**
 * Action base class for the WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryAction.class.php';
/**
 * Search process class
 * 検索処理クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositorySearch.class.php';

/**
 * Action class author affiliation item search
 * 著者所属アイテム検索アクションクラス
 * 
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class Repository_Action_Edit_Nameauthority_Search_Item extends WekoAction
{
    /**
     * WEKO author ID
     * WEKO著者ID
     *
     * @var int
     */
    public $author_id = null;
    /**
     * Start position got item
     * 取得するアイテムの開始位置
     *
     * @var int
     */
    public $start_num = null;
    /**
     * Number of search result per request
     * リクエストあたりの検索結果件数
     *
     * @var int
     */
    public $per_num = null;
    
    /**
     * Search author affiliation item
     * 著者所属アイテム検索
     */
    function executeApp()
    {
        // 検索実行
        $repositorySearch = new RepositorySearch();
        $repositorySearch->Db = $this->Db;
        $repositorySearch->Session = $this->Session;
        $repositorySearch->search_term[RepositorySearch::REQUEST_WEKO_AUTHOR_ID] = $this->author_id;
        $repositorySearch->listResords = "all";
        $result = $repositorySearch->search();
        
        // フィードバックメール送信先として紐付いているアイテムを検索
        $businessLoadauthorinformation = BusinessFactory::getFactory()->getBusiness('businessLoadauthorinformation');
        $feedbackInfo = $businessLoadauthorinformation->selectFeedbackInfo($this->author_id);
        
        // 検索結果をマージ
        $itemIdList = $businessLoadauthorinformation->createItemIdListAssociatedWithAuthor($result, $feedbackInfo);
        
        // アイテムリスト作成
        $itemList = $this->createItemList($this->start_num, $this->per_num, count($itemIdList), $itemIdList);
        
        // JSON形式に変換
        $jsonArray = array();
        $jsonArray["item_list"] = $itemList;
        $jsonArray["start_num"] = $this->start_num;
        $jsonArray["per_num"] = $this->per_num;
        $jsonArray["total_num"] = count($itemIdList);
        
        $businessGeneratejsontypedata = BusinessFactory::getFactory()->getBusiness('businessGeneratejsontypedata');
        $itemListJson = $businessGeneratejsontypedata->raw_json_encode($jsonArray);
        
        echo $itemListJson;
        
        $this->exitFlag = true;
    }
    
    /**
     * Create list of search results of author affiliation items
     * 著者所属アイテム検索結果のリストを作成する
     *
     * @param int $start_num Start position got item 取得するアイテムの開始位置
     * @param int $per_num Number of search result per request リクエストあたりの検索結果件数
     * @param int $total_num Number of search result total 検索されたアイテムの総件数
     * @param array $searchResult Search result 検索結果
     *                            array[$ii]["item_id"|"item_no"|"metadata_flag"|"feedback_flag"]
     *
     * @return array Author information 著者情報
     *               array[$ii]["author_id"|"language"|"family"|...]
     */
    private function createItemList($start_num, $per_num, $total_num, $searchResult) 
    {
        $itemList = array();
        
        $repositoryAction = new RepositoryAction();
        $repositoryAction->Db = $this->Db;
        $repositoryAction->Session = $this->Session;
        $repositoryAction->TransStartDate = $this->accessDate;
        
        if($start_num + $per_num < $total_num)
        {
            $end_num = $start_num + $per_num;
        }
        else
        {
            $end_num = $total_num;
        }
        
        for($ii = $start_num; $ii < $end_num; $ii++)
        {
            $repositoryAction->getItemData($searchResult[$ii]["item_id"], $searchResult[$ii]["item_no"], $Result_List, $Error_Msg);
            $itemList[] = array("item_id" => $Result_List["item"][0]["item_id"], 
                                "title" => $Result_List["item"][0]["title"], 
                                "title_english" => $Result_List["item"][0]["title_english"]);
        }
        
        return $itemList;
    }
}
?>
