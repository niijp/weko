<?php
/**
 * Delete author information from author name authority.
 * 著者名典拠から著者情報を削除する。
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Delete.class.php 76371 2017-02-16 08:13:59Z keiya_sugimoto $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
 * Action base class for WEKO
 * WEKO用アクション基底クラス
 * 
 */
require_once WEBAPP_DIR. '/modules/repository/components/common/WekoAction.class.php';

/**
 * Search process class
 * 検索処理クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositorySearch.class.php';

/**
 * Delete author information from author name authority.
 * 著者名典拠から著者情報を削除する。
 * 
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Action_Edit_Nameauthority_Delete extends WekoAction
{
    /**
     * Author ID to be deleted
     * 削除対象の著者ID
     * 
     * @var int
     */
    public $author_id = null;
    
    /**
     * Whether to check error condition of input condition only
     * 入力条件のエラーチェックのみを行うかどうか
     *
     * @var int
     */
    public $isCheckOnly = null;
    
    /**
     * Delete the specified author ID from the author name authority.
     * 指定した著者IDを著者名典拠から削除する。
     * 
     * @return string Result code. 実行結果
     */
    public function executeApp()
    {
        // JSON出力するAPIのためViewは呼ばない。
        // エラーメッセージは$this->addErrMsgで詰め、json出力される。
        $this->exitFlag = true;
        
        // 指定された著者IDが不正な場合は終了
        if($this->author_id < 1)
        {
            $this->addErrMsg("repository_name_authority_edit_error_numericAuthorId");
            return;
        }
        
        // 削除対象の著者IDがアイテムに紐付いている場合、削除不可
        $repositorySearch = new RepositorySearch();
        $repositorySearch->Db = $this->Db;
        $repositorySearch->Session = $this->Session;
        $repositorySearch->search_term[RepositorySearch::REQUEST_WEKO_AUTHOR_ID] = $this->author_id;
        $repositorySearch->listResords = "all";
        $result = $repositorySearch->search();
        $total_num = $repositorySearch->getTotal();
        if($total_num > 0)
        {
            $this->addErrMsg("repository_name_authority_edit_error_unlinkItem");
            return;
        }
        
        // 著者フィードバックメール送信先に登録されている場合、削除不可
        $busSendFeedbackMail = BusinessFactory::getFactory()->getBusiness('businessSendfeedbackmail');
        if($busSendFeedbackMail->feedbackmailItemExists($this->author_id))
        {
            $this->addErrMsg("repository_name_authority_edit_error_unlinkFeedbackMail");
            return;
        }
        
        // 入力チェックのみを行う場合、以下の処理は行わない
        if($this->isCheckOnly == 0)
        {
            // 削除
            try
            {
                $busNameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
                $addResult['authorId'] = $busNameAuthority->deleteNameAuthority($this->author_id);
            }
            catch (AppException $e)
            {
                // エラー発生時のユーザー通知はAPI呼び出し元で行うためログを出力して進む。
                $this->addErrMsg("repository_name_authority_edit_error_deleteNameAuthority");
                $this->errorLog("Failed entry NameAuthority Not Identify.", __FILE__, __CLASS__, __LINE__);
                return;
            }
            
            // 問題なく削除できれば何も出力しないで終了する。
        }
    }
}
?>
