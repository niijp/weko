<?php

/**
 * FLASH Views logger API.
 * FLASH再生ログを出力するAPI。
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Flashviewslog.class.php 76318 2017-02-13 13:00:25Z tomohiro_ichikawa $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics,
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Action base class for the WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/common/WekoAction.class.php';

/**
 * Const class
 * 定数クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryConst.class.php';


/**
 * FLASH Views logger API.
 * FLASH再生ログを出力するAPI。
 *   本APIが呼び出されるタイミング：
 *      詳細画面にて動画、音楽ファイルのFLASHファイル再生ボタンが押下された時。
 *      SWF, PDFなど再生ボタンを伴わないFLASHファイルの再生では詳細画面表示時にFLASHログを出力する。
 *   
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Action_Common_Flashviewslog extends WekoAction
{
    /**
     * Views Flash file item id.
     * ＜必須＞再生したFLASHファイルのアイテムID
     * 
     * @var int
     */
    public $item_id = null;
    
    /**
     * Views Flash file item no.
     * ＜必須＞再生したFLASHファイルのアイテム通番
     * 
     * @var int
     */
    public $item_no = null;
    
    /**
     * Views Flash file attribute id.
     * ＜必須＞再生したFLASHファイルのメタデータ項目ID(属性ID)
     * 
     * @var int
     */
    public $attribute_id = null;
    
    /**
     * Views Flash file file no.
     * ＜必須＞再生したFLASHファイルのファイル通番
     * 
     * @var int
     */
    public $file_no = null;
    
    /**
     * The state of the file that was operated. For files other than file download and FLASH playback log, set 0 (default).
     * ＜必須＞操作したファイルの状態。ファイルダウンロード、FLASH再生ログ以外では0(デフォルト)を設定する。
     *   0：不明
     *   1：オープンアクセス
     *  -1：非オープンアクセス
     * 
     * @var int
     */
    public $fileStatus = null;
    
    /**
     * User's site licensing status
     * ＜必須＞操作ユーザーのサイトライセンス認可状態
     * 
     * @var int
     */
    public $siteLicense = null;
    
    /**
     * The ID of the site licensing authority. Set to 0 if you are not a site licensing authority.
     * サイトライセンス認可機関のID。サイトライセンス認可機関でない場合は0を設定する。
     * 
     * @var int
     */
    public $siteLicenseId = null;
    
    /**
     * Type of operation file. Set to NULL except for file download and FLASH playback log.
     * ＜必須＞操作ファイルの種別。
     *   0：非課金ファイル
     *   1：課金ファイル
     * 
     * @var int
     */
    public $inputType = null;
    
    /**
     * User login status, authority.
     * ＜必須＞操作ユーザーのログイン状態、権限。
     *   0：未ログイン
     *   1：一般
     *   2：会員グループ（課金ファイル）
     *   5：登録者
     *  10：管理者
     * 
     * @var int
     */
    public $loginStatus = null;
    
    /**
     * The group ID of the NC 2 group (= member) in the download of the accounting file.
     * 課金ファイルのダウンロードでのNC2グループ(＝会員)のグループID。
     * 
     * @var int
     */
    public $groupId = null;
    
    /**
     * Only output the FLASH playback log to the operation log table once when the following conditions are satisfied.
     * 下記条件を満たす場合のみ、FLASH再生ログを操作ログテーブルに1回出力する。
     *   (1) POSTリクエストである。＜maple.iniでチェック済＞
     *   (2) リクエストパラメータにitem_id, item_no, attribute_id, file_noがある。＜maple.iniでチェック済＞
     *   (3) リファラ＝BASE_URL/weko/multimedia/WekoViewerForMultimedia.swfである。＜LogManagerクラスでチェック＞
     *   (4) ファイルが削除されていない。＜LogManagerクラスでチェック＞
     * 
     * @return string 'success'=>Entry flash views log, 'error'=>Dose'nt entry flash views log.
     *                'success'⇒FLASH再生ログを出力した。'error'⇒FLASH再生ログを出力しなかった。
     * @throws AppException
     */
    public function executeApp()
    {
        // 本Actionはログを出力した時点で完了し、Viewは存在しない。
        $this->exitFlag = true;
        
        // maple.iniにてチェックしていない入力値について、バリデーションする。
        // どちらも値がセットされている場合、数値のみ受付とする。
        if(isset($this->siteLicenseId) && !is_numeric($this->siteLicenseId)) $this->siteLicenseId = null;
        if(isset($this->groupId) && !is_numeric($this->groupId)) $this->groupId = null;
        
        // FLASH再生ログを出力して終了。
        $this->infoLog("businessLogmanager", __FILE__, __CLASS__, __LINE__);
        $logmanager = BusinessFactory::getFactory()->getBusiness("businessLogmanager");
        $logmanager->entryLogForFlashViewsAPI($this->item_id, $this->item_no, $this->attribute_id, $this->file_no, $this->fileStatus, $this->inputType, $this->loginStatus, $this->groupId, $this->siteLicense, $this->siteLicenseId);
        
        return 'success';
    }
}
?>