<?php
/**
 * Action class for robotlist log delete
 * ロボットリストログ削除用アクションクラス
 * 
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Robotlist.class.php 85654 2019-03-14 08:05:50Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
 * Action base class for WEKO
 * WEKO用アクション基底クラス
 * 
 */
require_once WEBAPP_DIR. '/modules/repository/components/common/WekoAction.class.php';

/**
 * Action class for robotlist log delete
 * ロボットリストログ削除用アクションクラス
 * 
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Action_Common_Robotlist extends WekoAction
{
    /**
     * Background process name for lock table
     * ロックテーブル用非同期処理名
     *
     * @var string
     */
    const PARAM_NAME = "Repository_Action_Common_Robotlist";
    
    /**
      * Random key (negative integer, 9 digits) for determining the execution authority
      * 実行権限有無を判断するためのランダムキー(負の整数, 9桁)
      *
      * @var int
      */
     public $request_id = null;
     
     /**
      * Valid time of request_id (seconds). Default 30 seconds
      * request_idの有効時間(秒)。デフォルト30秒
      *
      * @var int
      */
     const TIME_OUT_SEC = 30;
    
    /**
     * Get robotlist and delete robotlist log
     * ロボットリストの取得及びロボットリストログの削除を実施する
     *
     * @return string Result code 
     *                実行結果
     */
    function executeApp()
    {
        // View off. 画面は表示しない(デフォルト)
        $this->exitFlag = true;
        
        // Authority check. 権限チェック
        $user_id = $this->Session->getParameter("_user_id");
        $user_auth_id = $this->Session->getParameter("_user_auth_id");
        $auth_id = $this->Session->getParameter("_auth_id");
        if($user_id != "0" && $user_auth_id >= $this->repository_admin_base && $auth_id >= $this->repository_admin_room) {
            // Session情報があり、管理者＝WEKO管理画面から実行された⇒実行権限あり＆画面を表示する
            $this->request_id = RepositoryProcessUtility::genelateBackgroundProcessRequestId();
            if($this->startBackgroundProcessLockTable(self::PARAM_NAME, $this->request_id) === false) {
                // error. エラー終了
                $this->isFinish = true;
                return;
            }
            $this->exitFlag = false;
        } else if($this->validateRequestId() === false){
            // request_id is wrong, then error termination.
            $this->errorLog("Miss BackgroundProcess request_id.", __FILE__, __CLASS__, __LINE__);
            $this->isFinish = true;
            return;
        }
        
        // check process
        $status = $this->lockProcess();
        if($status === false){
            $this->errorLog("Can't lock BackgroundProcess.", __FILE__, __CLASS__, __LINE__);
            $this->isFinish = true;
            return;
        }
        
        $this->infoLog("businessRobotlistbase", __FILE__, __CLASS__, __LINE__);
        $businessRobotlistbase = BusinessFactory::getFactory()->getBusiness("businessRobotlistbase");
        
        // get robotlist_master
        $result = $this->getRobotListMaster();
        
        for ($ii = 0; $ii < count($result); $ii++) 
        {
            $isRobotlistUse = $result[$ii]["is_robotlist_use"];
            $robotlistId = $result[$ii]["robotlist_id"];
            
            // get robotlist data
            $robotlist = $businessRobotlistbase->getRobotList($robotlistId);
            
            // update robotlist data
            $robotlist = $businessRobotlistbase->updateRobotList($robotlistId, $robotlist);
        }
        
        $this->infoLog("businessLogmanager", __FILE__, __CLASS__, __LINE__);
        $businessLogmanager = BusinessFactory::getFactory()->getBusiness("businessLogmanager");
        
        // delete exclusion logs
        $businessLogmanager->removeExclusionAddress();
        
        // Improve Log 2015/06/22 K.Sugimoto --start--
        $this->infoLog("Commit SQL.", __FILE__, __CLASS__, __LINE__);
        if($this->Db->CompleteTrans() === false)
        {
            $this->infoLog("Failed commit trance.", __FILE__, __CLASS__, __LINE__);
            throw new AppException("Failed commit trance.");
        }
        // Improve Log 2015/06/22 K.Sugimoto --end--
        
        // access to deleterobotlist
        $this->accessTodeleterobotlist();
        
        return "success";
    }
    
    /**
     * Access to delete robotlist log background process class
     * ロボットリストログ削除非同期処理クラスにアクセスする
     *
     */
    private function accessTodeleterobotlist()
    {
        $deleterobotlist_request_id = RepositoryProcessUtility::genelateBackgroundProcessRequestId();
        if($this->startBackgroundProcessLockTable("Repository_Action_Common_Background_Deleterobotlist", $deleterobotlist_request_id) === false) {
            // error. エラー終了
            $this->isFinish = true;
            return;
        }
        
        $url = BASE_URL."/?action=repository_action_common_background_deleterobotlist"."&request_id=".$deleterobotlist_request_id;
        
        $option = array("timeout" => 10, 
                        "allowRedirects" => "true", 
                        "maxRedirects" => 3);
        
        $request = new HTTP_Request($url, $option);
        $request->addHeader("User-Agent", $_SERVER['HTTP_USER_AGENT']);
        
        $response = $request->sendRequest(); 
    }
    
    /**
     * Get Robotlist master file information
     * ロボットリストマスタファイル情報を取得する
     *
     */
    private function getRobotListMaster()
    {
        $query = "SELECT * " . 
                 "FROM " . DATABASE_PREFIX . "repository_robotlist_master " . 
                 "WHERE is_robotlist_use = ? AND is_delete = ? ; ";
        
        $params = array();
        $params[] = 1;
        $params[] = 0;
        
        $result = $this->Db->execute($query, $params);
        if($result === false) {
            $this->errorLog($this->Db->ErrorMsg(), __FILE__, __CLASS__, __LINE__);
            throw new AppException($this->Db->ErrorMsg());
        }
        
        return $result;
    }
    
    /**
      * As the same asynchronous processing is not multiple execution, leaving the effect that running the database
      * 同じ非同期処理が多重実行されないよう、データベースに実行中である旨を残す
      *
      * @return bool 成否
      */
     private function lockProcess()
     {
         // update process status
         $query = "UPDATE ".DATABASE_PREFIX."repository_lock ".
                 "SET status = ? ".
                 "WHERE process_name = ? ".
                 "AND status = ? ".
                 "AND LENGTH(comment) > ? ".
                 "AND TIME_TO_SEC(TIMEDIFF(?, comment)) < ?;";
         $params = array();
         $params[] = 1;
         $params[] = self::PARAM_NAME;
         $params[] = $this->request_id;
         $params[] = 0;
         $params[] = $this->accessDate;
         $params[] = self::TIME_OUT_SEC;
         
         $result = $this->Db->execute($query, $params);
         if($result === false) {
            $this->errorLog($this->Db->ErrorMsg(), __FILE__, __CLASS__, __LINE__);
            throw new AppException($this->Db->ErrorMsg());
        }
         
         $count = $this->Db->affectedRows();
         if($count == 0) return false;
         
         return true;
     }
    
    /**
     * Check if request_id is a 9-digit negative integer
     * request_idが9桁の負の整数であるか確認
     *
     * @return boolean 成否
     */
    private function validateRequestId() {
        $this->request_id = intval($this->request_id);
        if(-1000000000 < $this->request_id && $this->request_id < -99999999) return true;
        return false;
    }
    
    /**
     * Set execution start of BackgroundProcess.
     * BackgroundProcessの実行開始設定を行う
     * 
     * @param string $process_name Name of background process to start execution. 実行開始するバックグラウンド処理の名称
     * @param int $request_id Key figure that determines the execution authority of background processing. バックグラウンド処理の実行権限を判定するキー数字
     * @return boolean Execution start status. 実行開始有無
     */
    private function startBackgroundProcessLockTable($process_name, $request_id) {
        $query = "UPDATE {repository_lock} ".
                " SET status = ?, comment = DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s.000') ".
                " WHERE process_name = ? ".
                " AND status != ?";
        $params = array();
        $params[] = $request_id;
        $params[] = $process_name;
        $params[] = 1;
        
        $result = $this->Db->execute($query, $params);
        if($result === false) {
            $this->errorLog($this->Db->ErrorMsg(), __FILE__, __CLASS__, __LINE__);
            throw new AppException($this->Db->ErrorMsg());
        }
        
        $count = $this->Db->affectedRows();
        if($count == 0) return false;
        
        return true;
    }
}
?>
