<?php

/**
 * Reconstruct index view rights table
 * 検索テーブル再構築実行クラス
 *
 * @package     WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Search.class.php 85773 2019-03-22 11:01:37Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics,
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Action base class for the WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryAction.class.php';

/**
 * Search table manager class
 * 検索テーブル管理クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositorySearchTableProcessing.class.php';

/**
 * Reconstruct index view rights table
 * 検索テーブル再構築実行クラス
 *
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_Action_Common_Reconstruction_Search extends RepositoryAction
{
    // request parameter
    /**
     * login ID
     * NC2ログインID
     *
     * @var int
     */
    public $login_id = null;
    /**
     * login password
     * NC2ログインパスワード
     *
     * @var int
     */
    public $password = null;
    
    // user's authority level
    /**
     * user authority ID
     * ユーザーベース権限
     *
     * @var int
     */
    public $user_authority_id = "";
    /**
     * authority ID
     * ユーザールーム権限
     *
     * @var int
     */
    public $authority_id = '';
    
    // Bugfix WEKO_IVIS-92 Y.Nakao 2019.03.22 --start--
    /**
     * searchTableProcessing class
     * 検索テーブル更新用クラス
     */
    private $searchTableProcessing = null;
    
    /**
     * call search table update.
     * 検索テーブルの更新処理呼び出し
     */
    protected function afterTrans() {
        $this->searchTableProcessing->callAsyncProcess();
        print("Successfully updated.\n");
    }
    // Bugfix WEKO_IVIS-92 Y.Nakao 2019.03.22 --end--
    
    /**
     * Execute
     * 実行
     *
     * @return string "success"/"error" success/failed 成功/失敗
     */
    function executeApp()
    {
        // check login
        $result = null;
        $error_msg = null;
        $return = $this->checkLogin($this->login_id, $this->password, $result, $error_msg);
        if($return == false){
            print("Incorrect Login!\n");
            return false;
        }
        
        // check user authority id
        if($this->user_authority_id < $this->repository_admin_base || $this->authority_id < $this->repository_admin_room){
            print("You do not have permission to update.\n");
            return false;
        }
        
        // Bugfix WEKO_IVIS-92 Y.Nakao 2019.03.22 --start--
        $this->searchTableProcessing = new RepositorySearchTableProcessing($this->Session, $this->Db);
        $this->searchTableProcessing->updateSearchTableForAllItem();
        // Bugfix WEKO_IVIS-92 Y.Nakao 2019.03.22 --end--
        
        return 'success';
    }
}
?>