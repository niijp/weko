<?php

/**
 * Name authority view class
 * 著者名典拠Viewクラス
 *
 * @package     WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Nameauthority.class.php 69040 2016-06-17 07:52:42Z tatsuya_koyasu $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
 * Action base class for WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/common/WekoAction.class.php';
/**
 * Action base class for the WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryAction.class.php';

/**
 * Name authority view class
 * 著者名典拠Viewクラス
 *
 * @package     WEKO
 * @copyright   (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license     http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access      public
 */
class Repository_View_Edit_NameAuthority extends WekoAction
{
    // --------------------------------------------
    // 著者名典拠画面全体の表示項目
    // --------------------------------------------
    /**
     * Active tab ID.
     * 表示するタブID。
     *   0：追加・変更・削除（デフォルト）
     *   1：付替え
     *   2：インポート
     * @var int
     */
    public $nameAuthorityControlActiveTabId = 0;
    
    /**
     * Author ID prefix array
     * 外部著者IDのPrefixキーリスト
     * @var array
     */
    public $author_id_prefix_list = array();
    
    /**
     * Help icon display flag
     * ヘルプアイコン表示フラグ
     *
     * @var int
     */
    public $help_icon_display =  null;
    
    // --------------------------------------------
    // 著者名典拠→インポート画面表示項目
    // --------------------------------------------
    /**
     * Error list of import author name authority imported.
     * インポートした著者名典拠のエラーリスト
     *   エラー情報はRepository_Action_Edit_Importauthority_Confirmにて設定する。
     *
     * @var array
     */
    public $importErrorList = array();
    /**
     * Number of Author Authorities successfully imported.
     * インポートに成功した著者名典拠の件数
     *
     * @var int
     */
    public $importSuccessCnt = 0;
    
    /**
     * Execute
     * 実行
     *
     * @return string "success"/"error" success/failed 成功/失敗
     * @throws AppException
     */
    protected function executeApp()
    {
        // 表示中タブの指定を取得
        $this->nameAuthorityControlActiveTabId = $this->Session->getParameter("nameAuthorityControlActiveTabId");
        if(!isset($this->nameAuthorityControlActiveTabId)) $this->nameAuthorityControlActiveTabId = 0;
        $this->Session->removeParameter("nameAuthorityControlActiveTabId");
        
        // 外部著者IDのPrefixキーリスト取得
        $this->author_id_prefix_list = $this->loadExternalAuthorIdPrefixList();
        
        // インポート実行結果（エラーリスト）取得
        $this->importErrorList = $this->Session->getParameter("importNameAuthorityErrorList");
        if(!isset($this->importErrorList)) $this->importErrorList = array();
        $this->Session->removeParameter("importNameAuthorityErrorList");
        
        // インポート実行結果（成功件数）取得
        $this->importSuccessCnt = $this->Session->getParameter("importNameAuthoritySuccess");
        if(!isset($this->importSuccessCnt)) $this->importSuccessCnt = 0;
        $this->Session->removeParameter("importNameAuthoritySuccess");
        
        // ヘルプアイコン表示
        $configParameter = BusinessFactory::getFactory()->getBusiness('businessConfigParameter');
        $this->help_icon_display = $configParameter->helpIconDisplay();
        
        return 'success';
    }
    
    /**
     * Load list of external author id prefix data
     * 外部著者IDのプレフィックス情報一覧を取得する
     * 
     * @return array List of external author id prefix data
     *               外部著者IDのプレフィックス情報一覧
     *               array[$ii]["prefix_id"|"prefix_name"|"url"|...]
     * @throws AppException
     */
    private function loadExternalAuthorIdPrefixList()
    {
        $NameAuthority = BusinessFactory::getFactory()->getBusiness('businessNameauthority');
        $result = $NameAuthority->getExternalAuthorIdPrefixList();
        if($result === false) {
            $tmpErrorMsg = "Cannot get external authorID prefix list.";
            $this->errorLog($tmpErrorMsg, __FILE__, __CLASS__, __LINE__);
            $exception = new AppException($tmpErrorMsg);
            $exception->addError($tmpErrorMsg);
            throw $exception;
        }
        
        return $result;
    }
}
?>
