<?php

/**
 * View class for private tree edit screen display
 * プライベートツリー編集画面表示用ビュークラス
 *
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Tree.class.php 15374 2012-02-10 12:41:17Z yuko_nakao $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
 * Action base class for the WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryAction.class.php';

/**
 * View class for index editing screen display
 * インデックス編集画面表示用ビュークラス
 */
require_once WEBAPP_DIR. '/modules/repository/view/edit/tree/Tree.class.php';

/**
 * View class for private tree edit screen display
 * プライベートツリー編集画面表示用ビュークラス
 *
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class Repository_View_Main_Privatetree extends RepositoryAction
{
    // member
    /**
     * Error flag
     * エラーフラグ
     *
     * @var boolean
     */
    public $error_flg = null;
    /**
     * 
     * 更新成功ポップアップ表示
     *
     * @var string
     */
    public $view_popup = null;
    
    // Set help icon setting 2010/02/10 K.Ando --start--
    
    /**
     * Help icon display flag
     * ヘルプアイコン表示フラグ
     *
     * @var int
     */
    public $help_icon_display =  null;
    // Set help icon setting 2010/02/10 K.Ando --end--
    
    /**
     * Tree error message
     * ツリーエラーメッセージ
     *
     * @var string
     */
    public $tree_error_msg = '';
    
    /**
     * Edit index ID
     * 編集中のインデックスID
     *
     * @var int
     */
    public $edit_id = -1;
    
    /**
     * Display the private tree screen
     * プライベートツリー画面表示
     *
     * @access  public
     * @return string Result 結果
     */
    function executeApp()
    {
        // プライベートツリーのインデックスID取得
        $privateTreeIndexId = $this->getPrivateTreeIndexId();
        
        // 初期化処理
        $editTree = new Repository_View_Edit_Tree();
        $editTree->Session = $this->Session;
        $editTree->Db = $this->Db;
        $editTree->error_flg = $this->error_flg;
        
        // 初期選択状態となるインデックスを設定する
        $editIndex = $privateTreeIndexId;
        if($this->Session->getParameter("update_private_index") != null) {
            // インデックス更新から戻った場合はその値を使用する
            $editIndex = $this->Session->getParameter("update_private_index");
        }
        $this->Session->removeParameter("update_private_index");
        $this->Session->removeParameter("update_index");
        // 選択中のインデックスID
        $this->edit_id = $editIndex;
        $editTree->edit_id = $editIndex;
        
        // 実行して結果を取得する
        $result = $editTree->execute();
        $this->help_icon_display = $editTree->help_icon_display;
        $this->tree_error_msg = $editTree->tree_error_msg;
        $this->view_popup = $editTree->view_popup;
        $this->Session->setParameter("edit_index", $editIndex);
        
        // プライベートツリーの親インデックスIDの取得
        $makePrivateTree = null;
        $error_msg = null;
        $return = $this->getAdminParam('is_make_privatetree', $makePrivateTree, $error_msg);
        if($return == false){
            return false;
        }
        if($makePrivateTree == 0){
            return "error";
        }
        $parentIndexId = null;
        $error_msg = null;
        $return = $this->getAdminParam('privatetree_parent_indexid', $parentIndexId, $error_msg);
        if($return == false){
            return false;
        }
        
        // Add tree access control list 2012/02/29 T.Koyasu -start-
        // get auth level of user
        $user_auth_id = $this->Session->getParameter("_user_auth_id");
        
        // get user's room authority
        $auth_id = $this->getRoomAuthorityID();
        // when user is not admin
        if($user_auth_id < $this->repository_admin_base || $auth_id < $this->repository_admin_room) {
            if(!$this->checkParentPublicState($privateTreeIndexId)){
                return "error";
            }
        }
        $this->Session->setParameter("MyPrivateTreeRootId", $privateTreeIndexId);
        
        return $result;
    }
}
?>
