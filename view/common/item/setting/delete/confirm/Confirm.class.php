<?php

/**
 * View class for search delete confirm
 * 一括削除確認ポップアップ表示用ビュークラス
 *
 * @package WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Confirm.class.php 76253 2017-02-10 06:25:32Z tomohiro_ichikawa $
//
// Copyright (c) 2007 - 2008, National Institute of Informatics, 
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
 * Action base class for WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/common/WekoAction.class.php';

/**
 * View class for search delete confirm
 * 一括削除確認ポップアップ表示用ビュークラス
 *
 * @package WEKO
 * @copyright (c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class Repository_View_Common_Item_Setting_Delete_Confirm extends WekoAction
{
    /**
     * base block
     * 呼び出し元画面のblockID
     *
     * @var string
     */
    public $baseBlock = null;
    /**
     * delete type(0: index/1: keyword/2: item type)
     * 削除タイプ(0: index/1: keyword/2: item type)
     *
     * @var int
     */
    public $deleteType = null;
    
    /**
     * Select index id(for index)
     * 選択インデックスID(インデックス削除用)
     *
     * @var int
     */
    public $targetIndexId = null;
    /**
     * Sub-index following the deletion flag(for index)
     * サブインデックス以下削除フラグ(インデックス削除用)
     *
     * @var boolean
     */
    public $isDeleteSubIndexItem = null;
    
    /**
     * search keyword for delete(for keyword)
     * 検索キーワード(キーワード検索削除用)
     *
     * @var string
     */
    public $searchKeyword = null;
    /**
     * search item type for delete(for item type)
     * 検索アイテムタイプ(アイテムタイプ検索削除用)
     *
     * @var string
     */
    public $searchItemType = null;
    /**
     * search type(for keyword)
     * 検索タイプ(キーワード検索削除用)
     *
     * @var int
     */
    public $searchType = null;
    
    /**
     * execute
     * 実行
     *
     * @access  public
     * @return string Result 結果
     */
    function executeApp()
    {
        return "success";
    }
}
?>
