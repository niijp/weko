<?php

/**
 * View class for DOI granting condition state check
 * DOI付与条件状態チェック用ビュークラス
 *
 * @package     WEKO
 */

// --------------------------------------------------------------------
//
// $Id: Status.class.php 75663 2017-01-25 06:17:45Z keiya_sugimoto $
//
// Copyright(c) 2007 - 2008, National Institute of Informatics,
// Research and Development Center for Scientific Information Resources
//
// This program is licensed under a Creative Commons BSD Licence
// http://creativecommons.org/licenses/BSD/
//
// --------------------------------------------------------------------
/**
 * Action base class for the WEKO
 * WEKO用アクション基底クラス
 */
require_once WEBAPP_DIR . '/modules/repository/components/common/WekoAction.class.php';

/**
 * Check grant doi business class
 * DOI付与チェックビジネスクラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/business/doi/Checkdoi.class.php';
/**
 * Result of check grant DOI class
 * DOI付与チェック結果クラス
 */
require_once WEBAPP_DIR . '/modules/repository/components/business/doi/Resultcheckdoi.class.php';
/**
 * Handle management common classes
 * ハンドル管理共通クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryHandleManager.class.php';
/**
 * Const for WEKO class
 * WEKO用定数クラス
 */
require_once WEBAPP_DIR. '/modules/repository/components/RepositoryConst.class.php';
/**
 * Mapping List Reqquired DOI class
 * DOIに必要なメタデータリストクラス
 *
 */
require_once WEBAPP_DIR. '/modules/repository/components/business/doi/Mappinglistrequireddoi.class.php';

/**
 * View class for DOI granting condition state check
 * DOI付与条件状態チェック用ビュークラス
 *
 * @package WEKO
 * @copyright(c) 2007, National Institute of Informatics, Research and Development Center for Scientific Information Resources
 * @license http://creativecommons.org/licenses/BSD/ This program is licensed under the BSD Licence
 * @access public
 */
class Repository_View_Common_Doi_Assign_Status extends WekoAction
{
    /**
     * DOI type: JaLC
     * DOI種別: JaLC
     *
     * @var int
     */
    const JALC_DOI = "JaLC";
    
    /**
     * DOI type: CrossRef
     * DOI種別: CrossRef
     *
     * @var int
     */
    const CROSSREF_DOI = "CrossRef";
    
    /**
     * DOI type: DataCite
     * DOI種別: DataCite
     *
     * @var int
     */
    const DATACITE_DOI = "DataCite";
    
    /**
     * Item ID of item for checking DOI granting condition
     * DOI付与条件をチェックするアイテムのアイテムID
     *
     * @var int
     */
    public $item_id = null;
    
    /**
     * Item No of item for checking DOI granting condition
     * DOI付与条件をチェックするアイテムのアイテム通番
     *
     * @var int
     */
    public $item_no = null;
    
    /**
     * DOI granted state
     *    0 => not granted
     *    1 => granted
     *    2 => droped
     * DOI付与状態
     *    0 => 未付与
     *    1 => 付与済
     *    2 => 取下げ
     * 
     * @var int
     */
    public $doiAssigned = 0;
    
    /**
     * DOI granting condition check state of JaLC
     * JaLCのDOI付与条件チェック状態
     *
     * @var Object
     */
    public $resultCheckDoiJaLC = null;
    
    /**
     * DOI granting condition check state of CrossRef
     * CrossRefのDOI付与条件チェック状態
     *
     * @var Object
     */
    public $resultCheckDoiCrossRef = null;
    
    /**
     * DOI granting condition check state of DataCite
     * DataCiteのDOI付与条件チェック状態
     *
     * @var Object
     */
    public $resultCheckDoiDataCite = null;
    
    /**
     * Granted DOI type
     * 付与済のDOI種別
     * 
     * @var string
     */
    public $doiAssignedType = "";
    
    /**
     * Item type name being edited
     * 編集中のアイテムタイプ名
     * 
     * @var string
     */
    public $itemtypeName = "";
    
    /**
     * Mapping list required for DOI
     * DOIに必要なマッピングリスト
     * 
     * @var array array[$ii]
     */
    public $mappingList = array();
    
    /**
     * Metadata list required for DOI
     * DOIに必要なメタデータリスト
     * 
     * @var array array["JaLC"|"CrossRef"|"DataCite"]["fullTextURL"|"title"|"publisher"|...]["required"|"multiple"|"english"]
     */
    public $requiredMetadata = array();
    
    /**
     * Display DOI granting condition check result screen
     * DOI付与条件チェック結果画面を表示する
     *
     * @return string Result code 実行結果
     */
    protected function executeApp()
    {
        // DOI付与条件チェック結果画面が表示できるか確認し、閲覧不可であればエラーコード(表示するHTMLを明示する文字列)を返す
        $errorCode = $this->isViewDoiGrantingConditionCheckResultPage();
        if(strlen($errorCode) > 0)
        {
            return $errorCode;
        }
        
        // アイテムタイプ名取得
        $this->itemtypeName = $this->selectItemtypeName($this->item_id, $this->item_no);
        
        // DOIに必要なメタデータリスト取得
        $this->createMetadataListRequiredForDoiForItem($this->item_id, $this->item_no, $this->mappingList, $this->requiredMetadata);
        
        // Checkdoi クラスを使用
        $CheckDoi = BusinessFactory::getFactory()->getBusiness("businessCheckdoi");
        
        // DOI付与状態取得
        $this->doiAssigned = $CheckDoi->getDoiStatus($this->item_id, $this->item_no);
        
        // DOI種別取得
        if($this->doiAssigned == Repository_Components_Business_Doi_Checkdoi::DOI_STATUS_GRANTED)
        {
            $this->doiAssignedType = $this->checkGrantedDoiType($this->item_id, $this->item_no);
        }
        
        // DOI付与条件チェック
        if($this->doiAssigned != Repository_Components_Business_Doi_Checkdoi::DOI_STATUS_DROPED)
        {
            $this->resultCheckDoiJaLC = $CheckDoi->checkDoiGrant($this->item_id, $this->item_no, Repository_Components_Business_Doi_Checkdoi::TYPE_JALC_DOI, null);
            $this->resultCheckDoiCrossRef = $CheckDoi->checkDoiGrant($this->item_id, $this->item_no, Repository_Components_Business_Doi_Checkdoi::TYPE_CROSS_REF, null);
            $this->resultCheckDoiDataCite = $CheckDoi->checkDoiGrant($this->item_id, $this->item_no, Repository_Components_Business_Doi_Checkdoi::TYPE_DATACITE, null);
            
            // smartyにデータを持たせる際に、falseとnullの区別ができないため、下記のように変換する
            // true → 1, false → -1, null → 0
            $this->resultCheckDoiJaLC = $this->convertDoiCheckResult($this->resultCheckDoiJaLC);
            $this->resultCheckDoiCrossRef = $this->convertDoiCheckResult($this->resultCheckDoiCrossRef);
            $this->resultCheckDoiDataCite = $this->convertDoiCheckResult($this->resultCheckDoiDataCite);
        }
        
        return 'success';
    }
    
    /**
     * Judged whether reading DOI granting condition check result screen is possible.
     * DOI付与条件チェック結果画面表示が可能か判断する
     * 
     * @return string The indicated HTML file designation character. 空文字=>閲覧可能。空文字以外=>閲覧不可、表示するHTMLファイル指定文字。
     */
    private function isViewDoiGrantingConditionCheckResultPage()
    {
        // リクエストパラメータのアイテム指定が無い場合はアイテムに依存しない項目のみでチェックする
        if( preg_match("/^[1-9][0-9]*$/", $this->item_id) !== 1 || preg_match("/^[1-9][0-9]*$/", $this->item_no) !== 1)
        {
            $this->checkDoiForExcludeItem();
            return "success";
        }
        
        $bizItemAuthority = BusinessFactory::getFactory()->getBusiness("businessItemAuthority");
        $bizItemtypeAuthority = BusinessFactory::getFactory()->getBusiness("businessItemtypeAuthority");
        
        // アイテムが削除されている場合はアイテム存在エラー
        if($bizItemAuthority->isItemDeleted($this->item_id, $this->item_no))
        {
            return "invalid";
        }
        
        // アイテム投稿者であるか確認する
        if($bizItemAuthority->isItemContributor($this->Session->getParameter("_user_id"), $this->item_id, $this->item_no))
        {
            return "";
        }
        
        // 管理者ユーザーであるか確認する
        $dbAccess = new RepositoryDbAccess($this->Db);
        $userAuthorityManager = new RepositoryUserAuthorityManager($this->Session, $dbAccess, $this->accessDate);
        $user_auth_id = $this->Session->getParameter("_user_auth_id");
        $auth_id = $userAuthorityManager->getRoomAuthorityID();
        if($user_auth_id >= $this->repository_admin_base && $auth_id >= $this->repository_admin_room)
        {
            return "";
        }
        
        return "invalid";
    }
    
    /**
     * Check DOI turning on / off status with item-independent items
     * アイテムに依存しない項目でDOIの発番可否をチェックする
     */
    private function checkDoiForExcludeItem()
    {
        $item_type_info = $this->Session->getParameter("item_type_all");
        
        // Checkdoi クラスを使用
        $CheckDoi = BusinessFactory::getFactory()->getBusiness("businessCheckdoi");
        
        $this->resultCheckDoiJaLC = $CheckDoi->checkDoiForExcludeItemByDoiType($item_type_info["item_type_id"], Repository_Components_Business_Doi_Checkdoi::TYPE_JALC_DOI);
        $this->resultCheckDoiCrossRef = $CheckDoi->checkDoiForExcludeItemByDoiType($item_type_info["item_type_id"], Repository_Components_Business_Doi_Checkdoi::TYPE_CROSS_REF);
        $this->resultCheckDoiDataCite = $CheckDoi->checkDoiForExcludeItemByDoiType($item_type_info["item_type_id"], Repository_Components_Business_Doi_Checkdoi::TYPE_DATACITE);
        
        // smartyにデータを持たせる際に、falseとnullの区別ができないため、下記のように変換する
        // true → 1, false → -1, null → 0
        $this->resultCheckDoiJaLC = $this->convertDoiCheckResult($this->resultCheckDoiJaLC);
        $this->resultCheckDoiCrossRef = $this->convertDoiCheckResult($this->resultCheckDoiCrossRef);
        $this->resultCheckDoiDataCite = $this->convertDoiCheckResult($this->resultCheckDoiDataCite);
    }
    
    /**
     * Check type of granted DOI
     * 付与済みDOIの種別をチェックする
     * 
     * @param int $item_id Item ID アイテムID
     * @param int $item_no Item No アイテム通番
     *
     * @return string Granted DOI type 付与済のDOI種別
     */
    private function checkGrantedDoiType($item_id, $item_no)
    {
        $doi_type = "";
        
        $repositoryHandleManager = new RepositoryHandleManager($this->Session, $this->Db, $this->accessDate);
        $libraryJalcdoi_suffix = $repositoryHandleManager->getLibraryJalcdoiSuffix($item_id, $item_no);
        $jalcdoi_suffix = $repositoryHandleManager->getJalcdoiSuffix($item_id, $item_no);
        $crossref_suffix = $repositoryHandleManager->getCrossrefSuffix($item_id, $item_no);
        $datacite_suffix = $repositoryHandleManager->getDataciteSuffix($item_id, $item_no);
        if(strlen($libraryJalcdoi_suffix) > 0 || strlen($jalcdoi_suffix) > 0)
        {
            $doi_type = self::JALC_DOI;
        }
        else if(strlen($crossref_suffix) > 0)
        {
            $doi_type = self::CROSSREF_DOI;
        }
        else if(strlen($datacite_suffix) > 0)
        {
            $doi_type = self::DATACITE_DOI;
        }
        
        return $doi_type;
    }
    
    /**
     * Convert DOI grant check results
     * DOI付与チェック結果を変換する
     * 
     * @param Object $resultCheckDoi Result Check DOI DOIチェック結果
     * @param int $item_no Item No アイテム通番
     *
     * @return string Granted DOI type 付与済のDOI種別
     */
    private function convertDoiCheckResult($resultCheckDoi)
    {
        $convertedResultDoi = new ResultCheckDoi();
        
        $convertedResultDoi->isGrantDoi = $this->convertCheckItem($resultCheckDoi->isGrantDoi);
        $convertedResultDoi->isNotReviewItem = $this->convertCheckItem($resultCheckDoi->isNotReviewItem);
        $convertedResultDoi->isPublicItem = $this->convertCheckItem($resultCheckDoi->isPublicItem);
        $convertedResultDoi->isSetNiiType = $this->convertCheckItem($resultCheckDoi->isSetNiiType);
        $convertedResultDoi->isSetItemTypeMapping = $this->convertCheckItem($resultCheckDoi->isSetItemTypeMapping);
        $convertedResultDoi->LackItemTypeMapping = $resultCheckDoi->LackItemTypeMapping;
        $convertedResultDoi->isPublicIndex = $this->convertCheckItem($resultCheckDoi->isPublicIndex);
        $convertedResultDoi->isOpenAccessIndex = $this->convertCheckItem($resultCheckDoi->isOpenAccessIndex);
        $convertedResultDoi->isPublicHarvestIndex = $this->convertCheckItem($resultCheckDoi->isPublicHarvestIndex);
        $convertedResultDoi->isSetMetadata = $this->convertCheckItem($resultCheckDoi->isSetMetadata);
        $convertedResultDoi->LackMetadata = $resultCheckDoi->LackMetadata;
        
        return $convertedResultDoi;
    }
    
    /**
     * Convert the results of DOI grant check items
     * DOI付与チェック項目の結果を変換する
     * 
     * @param boolean $checkItem Check item チェック項目
     *
     * @return int Converted check item 変換後のチェック項目
     */
    private function convertCheckItem($checkItem)
    {
        if(!isset($checkItem))
        {
            $converted = 0;
        }
        else if($checkItem)
        {
            $converted = 1;
        }
        else
        {
            $converted = -1;
        }
        
        return $converted;
    }
    
    /**
     * Select item type name
     * アイテムタイプ名取得
     * 
     * @param int $item_id Item ID アイテムID
     * @param int $item_no Item No アイテムNo
     * @return string Item type name アイテムタイプ名
     */
    private function selectItemtypeName($item_id, $item_no)
    {
        $query = "SELECT item_type_name ".
                  " FROM {repository_item_type}".
                  " WHERE item_type_id = (".
                  "    SELECT item_type_id ".
                  "    FROM {repository_item}".
                  "    WHERE item_id = ? ".
                  "    AND item_no = ? ".
                  "    AND is_delete = ? ".
                  ")";
        $params = array();
        $params[] = $item_id;
        $params[] = $item_no;
        $params[] = 0;
        
        $result = $this->Db->execute($query, $params);
        if(count($result) == 0)
        {
            return "";
        }
        
        return $result[0]['item_type_name'];
    }
    
    /**
     * Create metadata list required for DOI for item
     * アイテムに関してDOIに必要なメタデータリストを作成する
     * 
     * @param int $item_id Item ID アイテムID
     * @param int $item_no Item No アイテムNo
     * @param array $mappingList Mapping list required for DOI DOIに必要なマッピングリスト
     *                           array[$ii]
     * @param array $requiredMetadata Metadata list required for DOI DOIに必要なメタデータリスト
     *                                array["JaLC"|"CrossRef"|"DataCite"]["fullTextURL"|"title"|"publisher"|...]["required"|"not_multiple"|"english"]
     */
    private function createMetadataListRequiredForDoiForItem($item_id, $item_no, &$mappingList, &$requiredMetadata)
    {
        $niiType = $this->selectItemtypeNiiType($item_id, $item_no);
        
        if(strlen($niiType) > 0)
        {
            $this->createMetadataListRequiredForDoi($niiType, $mappingList, $requiredMetadata);
        }
    }
    
    /**
     * Select NII type of the item type
     * アイテムタイプの資源タイプ取得
     * 
     * @param int $item_id Item ID アイテムID
     * @param int $item_no Item No アイテムNo
     * @return string NII type of the item type アイテムタイプの資源タイプ
     */
    private function selectItemtypeNiiType($item_id, $item_no)
    {
        $query = "SELECT mapping_info ".
                  " FROM {repository_item_type}".
                  " WHERE item_type_id = (".
                  "    SELECT item_type_id ".
                  "    FROM {repository_item}".
                  "    WHERE item_id = ? ".
                  "    AND item_no = ? ".
                  "    AND is_delete = ? ".
                  ")";
        $params = array();
        $params[] = $item_id;
        $params[] = $item_no;
        $params[] = 0;
        
        $result = $this->Db->execute($query, $params);
        if($result === false || count($result) == 0)
        {
            return "";
        }
        
        return $result[0]['mapping_info'];
    }
    
    /**
     * Create metadata list required for DOI
     * DOIに必要なメタデータリストを作成する
     * 
     * @param string $item_id NII type of the item type アイテムタイプの資源タイプ
     * @param array $mappingList Mapping list required for DOI DOIに必要なマッピングリスト
     *                           array[$ii]
     * @param array $requiredMetadata Metadata list required for DOI DOIに必要なメタデータリスト
     *                                array["JaLC"|"CrossRef"|"DataCite"]["fullTextURL"|"title"|"publisher"|...]["required"|"not_multiple"|"english"]
     */
    private function createMetadataListRequiredForDoi($niiType, &$mappingList, &$requiredMetadata)
    {
        $mappingListRequiredDoi = new MappingListRequiredDoi();
        switch($niiType)
        {
            case RepositoryConst::NIITYPE_JOURNAL_ARTICLE:
            case RepositoryConst::NIITYPE_ARTICLE:
            case RepositoryConst::NIITYPE_PREPRINT:
            case RepositoryConst::NIITYPE_DEPARTMENTAL_BULLETIN_PAPER:
                $mappingList = $mappingListRequiredDoi->mappingListJournalArticle;
                $requiredMetadata["JaLC"] = $mappingListRequiredDoi->mappingListJournalArticleForJalc;
                $requiredMetadata["CrossRef"] = $mappingListRequiredDoi->mappingListJournalArticleForCrossref;
                $requiredMetadata["DataCite"] = array();
                break;
                
            case RepositoryConst::NIITYPE_THESIS_OR_DISSERTATION:
            case RepositoryConst::NIITYPE_LEARNING_MATERIAL:
            case RepositoryConst::NIITYPE_PRESENTATION:
                $mappingList = $mappingListRequiredDoi->mappingListThesisOrDissertation;
                $requiredMetadata["JaLC"] = $mappingListRequiredDoi->mappingListThesisOrDissertationForJalc;
                $requiredMetadata["CrossRef"] = array();
                $requiredMetadata["DataCite"] = array();
                break;
                
            case RepositoryConst::NIITYPE_CONFERENCE_PAPER:
            case RepositoryConst::NIITYPE_BOOK:
            case RepositoryConst::NIITYPE_TECHNICAL_REPORT:
            case RepositoryConst::NIITYPE_RESEARCH_PAPER:
                $mappingList = $mappingListRequiredDoi->mappingListBook;
                $requiredMetadata["JaLC"] = $mappingListRequiredDoi->mappingListBookForJalc;
                $requiredMetadata["CrossRef"] = $mappingListRequiredDoi->mappingListBookForCrossref;
                $requiredMetadata["DataCite"] = array();
                break;
                
            case RepositoryConst::NIITYPE_DATA_OR_DATASET:
            case RepositoryConst::NIITYPE_SOFTWARE:
                $mappingList = $mappingListRequiredDoi->mappingListResearchData;
                $requiredMetadata["JaLC"] = $mappingListRequiredDoi->mappingListResearchDataForJalc;
                $requiredMetadata["CrossRef"] = array();
                $requiredMetadata["DataCite"] = $mappingListRequiredDoi->mappingListResearchDataForDataCite;
                break;
                
            case RepositoryConst::NIITYPE_OTHERS:
            default:
                $mappingList = array();
                $requiredMetadata["JaLC"] = array();
                $requiredMetadata["CrossRef"] = array();
                $requiredMetadata["DataCite"] = array();
                break;
                
        }
    }
}
?>
